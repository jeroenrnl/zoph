pipeline {
  agent {
    kubernetes {
      defaultContainer 'zoph'
      inheritFrom 'zoph'
    }
  }
  stages {
    stage('create empty db') {
      parallel {
        stage('create empty db') {
          environment {
            DB = credentials('dbpass')
          }
          steps {
            sh 'mysqladmin -f -h sql -u"${DB_USR}" -p"${DB_PSW}" drop zophtest'
            sh ' mysql -h sql -u"${DB_USR}" -p"${DB_PSW}" -e "CREATE DATABASE zophtest CHARACTER SET utf8 COLLATE utf8_general_ci"'
            //sh 'mysql -h sql -u "${DB_USR}" -p"${DB_PSW}" -e "grant all on zophtest.* to zoph@\'%\' identified by \'${DB_PSW}\'"'
          }
        }

        stage('Install phpDocumentor') {
          steps {
            sh '''
              if [ ! -x phpdoc ]
              then
                wget https://phpdoc.org/phpDocumentor.phar
                mv phpDocumentor.phar phpdoc
                chmod +x phpdoc
              fi
            '''
          }
        }

        stage('Delete old images') {
          steps {
            sh '''
              if [ -f ".images/TEST_0001.JPG" ]
              then
                  rm -r ./.images/*;
              fi
            '''
          }
        }
      }
    }

    stage('Prepare') {
      steps {
        sh 'mkdir -p build/api build/coverage build/logs  build/pdepend  build/phpdox'
        sh 'ln -s /etc/zoph/zoph.ini /etc/zoph.ini'
        pwd()
      }
    }

    stage('Create Testdata') {
      steps {
        sh 'php tests/createTestData/runTest.php'
      }
    }

    stage('composer') {
      steps {
        sh 'composer install'
      }
    }

    stage('Syntax check') {
      parallel {
        stage('pdepend') {
          steps {
            sh 'vendor/bin/pdepend --jdepend-xml=build/logs/jdepend.xml --jdepend-chart=build/pdepend/dependencies.svg --overview-pyramid=build/pdepend/overview-pyramid.svg --ignore=php/templates php'
            archiveArtifacts 'build/pdepend/*'
          }
        }

        stage('lint php') {
          steps {
            sh 'find php -name "*.php" -print0 | xargs -0 -n1 php -l'
          }
        }

        stage('lint Unittests') {
          steps {
            sh 'find tests -name "*.php" -print0 | xargs -0 -n1 php -l'
          }
        }

        stage('PHPloc') {
          steps {
            sh 'vendor/bin/phploc --count-tests --log-csv build/logs/phploc.csv --log-xml build/logs/phploc.xml php tests'
            archiveArtifacts 'build/logs/phploc.*'
          }
        }

        stage('Duplication detection') {
          steps {
            sh 'vendor/bin/phpcpd --log-pmd build/logs/pmd-cpd.xml php || true'
            archiveArtifacts 'build/logs/pmd-cpd.xml'
          }
        }

      }
    }

    stage('Static Analysis') {
      parallel {
        stage('phpmd') {
          steps {
            sh 'vendor/bin/phpmd php xml build/phpmd.xml --ignore-violations-on-exit --reportfile build/logs/phpmd.xml --exclude php/templates/default/blocks/header.tpl.php'
            archiveArtifacts 'build/logs/phpmd.xml'
          }
        }

        stage('PHPStan') {
          steps {
            sh 'vendor/bin/phpstan --no-interaction --no-progress --error-format=checkstyle --configuration=build/phpstan.neon analyse > build/logs/phpstan.xml || true'
            archiveArtifacts 'build/logs/phpstan.xml'
          }
        }

        stage('PHPCS') {
          steps {
            sh 'vendor/bin/phpcs --config-set ignore_errors_on_exit 1; vendor/bin/phpcs --config-set ignore_warnings_on_exit 1; vendor/bin/phpcs --report=checkstyle --report-file=build/logs/checkstyle.xml --standard=build/phpcs.xml --extensions=php --ignore=autoload.php php tests'
            archiveArtifacts 'build/logs/checkstyle.xml'
          }
        }

      }
    }

    stage('PHP unit') {
      steps {
        sh 'vendor/bin/phpunit --configuration build/phpunit.xml'
        archiveArtifacts 'build/logs/coverage/**'
      }
      post {
        always {
          junit 'build/logs/junit.xml'
        }
      }
    }

    stage('Sonar and documentation') {
      parallel {
        stage('PHP Documentor') {
          steps {
            sh './phpdoc -c build/phpdoc.xml'
            archiveArtifacts 'build/api/*'
            publishHTML target: [
              allowMissing: false,
              alwaysLinkToLastBuild: false,
              keepAll: true,
              reportDir: 'build/api',
              reportFiles: 'index.html',
              reportName: 'PHP Documentor'
            ]
          }
        }

        stage('Sonar') {
          steps {
            container('sonar') {
              withSonarQubeEnv(installationName: 'Sonar', credentialsId: 'Sonar') {
                sh 'sonar-scanner -D sonar.projectBaseDir=${WORKSPACE}'
              }
            }
          }
        }
      }
    }
    stage('Wait for Sonar') {
      steps {
        waitForQualityGate(credentialsId: 'Sonar', abortPipeline: true)
      }
    }

  }
}
