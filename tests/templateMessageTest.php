<?php
/**
 * Test message - a template to show messages
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use template\message;
use PHPUnit\Framework\TestCase;

/**
 * Test message template
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class templateMessageTest extends TestCase {
    /**
     * Test message template class
     * @dataProvider getMessages
     */
    public function testMessage(int $status, string $msg, string $title, string $exp_class, string $icon = null) {

        $message = new message($status, $msg, $title);

        $this->assertInstanceOf('template\message', $message);

        $output = (string) $message;

        $this->assertStringContainsString("icons/" . $icon ?: $exp_class . ".png", $output);
        $this->assertStringContainsString("class=\"message " . $exp_class . "\"", $output);
        $this->assertStringContainsString($msg, $output);
        if (!empty($title)) {
            $this->assertStringContainsString($title, $output);

        }
    }

    public function getMessages() {
        return array(
            array(message::SUCCESS, "It all went well", "", "success"),
            array(message::INFO, "I have something to say", "", "info"),
            array(message::WARNING, "There were some issues", "Warning!", "warning"),
            array(message::ERROR, "It all went haywire", "Run!", "error"),
            array(99, "I don't know what happened", "", "unknown")
        );
    }

}
