<?php
/**
 * Install controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use auth\validator;
use db\db;
use conf\conf;
use install\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use web\session;
use zoph\app;

/**
 * Test the install controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class installControllerTest extends TestCase {

    private static $token;

    public static function setUpBeforeClass() : void {
        app::setMode(app::INSTALL);
    }

    public static function tearDownAfterClass() : void {
        app::unsetMode(app::INSTALL);
        conf::loadFromDB();
    }

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $token = array();
        if ($action != "display") {
            $token = array(
                "token" => self::$token
            );
            $session = session::getCurrent();
            $session["token"] = self::$token;
        }
            
        $app = new app(mockupRequest::createRequest("install/" . $action, get: $token));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf($expView, $view);

        if ($action == "display") {
            self::$token = app::getToken();
        } 
    }

    public function testDisplay() {
        $app = new app(mockupRequest::createRequest("install/display"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<section class=\"install\">", (string) $output);

        $this->assertStringContainsString("<a href=\"/install/requirements?token=" . app::getToken() . "\" class=\"button install\" target=\"\">", (string) $output);
    }

    public function testRequirements() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );

        $app = new app(mockupRequest::createRequest("install/requirements", get: $token));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\requirements::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        $this->assertStringContainsString("<table class=\"requirements\">", (string) $output);
        $this->assertStringContainsString("class=\"reqpass\"", (string) $output);
        $this->assertStringContainsString("Check for PHP FileInfo Extension", (string) $output);
        $this->assertStringContainsString("PHP version supported", (string) $output);
    }

    public function testIniform() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );
        
        $app = new app(mockupRequest::createRequest("install/iniform", get: $token));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\iniform::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"name\"", (string) $output);
    }

    public function testInifile() {
        $session = session::getCurrent();
        $session["token"] = self::$token;

        $app = new app(mockupRequest::createRequest("install/inifile", post: array(
            "name"      => "zoph-install",
            "db_host"   => "db.zoph.org",
            "db_name"   => "install",
            "db_user"   => "test",
            "db_pass"   => "secret",
            "db_prefix" => "zoph_",
            "token"     => self::$token
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\inifile::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        // Check if the generated INI file is consisent with the input:
        $this->assertStringContainsString("&#091;zoph-install&#093", (string) $output);
        $this->assertStringContainsString("db_host = &quot;db.zoph.org&quot;", (string) $output);
        $this->assertStringContainsString("db_name = &quot;install&quot;", (string) $output);
        $this->assertStringContainsString("db_user = &quot;test&quot;", (string) $output);
        $this->assertStringContainsString("db_pass = &quot;secret&quot;", (string) $output);
        $this->assertStringContainsString("db_prefix = &quot;zoph_&quot;", (string) $output);
    }

    public function testDbPass() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );
        
        $app = new app(mockupRequest::createRequest("install/dbpass", get: $token));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\dbpass::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"admin_pass\" type=\"password\" name=\"admin_pass\" size=\"32\">", (string) $output);
    }

    public function testDbCheck() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );
        
        $app = new app(mockupRequest::createRequest("install/dbcheck", get: $token));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\dbcheck::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);

        // The check fails during unittest run, because the configuration for is not
        // stored in the INI file, like during normal run.
        $this->assertStringContainsString("Problem with INI file", (string) $output);
    }

    public function testDatabase() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        
        $db = db::getLoginDetails();

        $app = new app(mockupRequest::createRequest("install/database", post: array(
            "user"          => "admin",
            "admin_pass"    => "VerySecret",
            "token"         => self::$token
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\database::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<div class=\"result ok\">", (string) $output);
        $this->assertStringContainsString("create", (string) $output);
        $this->assertStringContainsString("grant", (string) $output);

        db::setLoginDetails($db["host"], $db["dbname"], $db["user"], $db["pass"], $db["prefix"]);
    }

    public function testTables() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );
        
        $app = new app(mockupRequest::createRequest("install/tables", get: $token));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(install\view\tables::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<div class=\"result ok\">", (string) $output);
        $this->assertStringContainsString("photos", (string) $output);
        $this->assertStringContainsString("albums", (string) $output);
        $this->assertStringContainsString("places", (string) $output);
    }

    public function testAdminuser() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        $token = array(
            "token" => self::$token
        );
        
        $app = new app(mockupRequest::createRequest("install/adminuser", get: $token));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(install\view\adminuser::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input id=\"admin_pass\" type=\"password\" name=\"admin_pass\" size=\"32\">", (string) $output);
    }

    public function testConfig() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        
        $app = new app(mockupRequest::createRequest("install/config", post: array(
            "admin_pass"    => "S3cr3t",
            "token"         => self::$token
        )));

        $app->run();
        $view = $app->view;

        // Restore configuration
        conf::set("path.images", getcwd() . "/.images")->update();

        $this->assertInstanceOf(install\view\config::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("<div class=\"install\">", (string) $output);
        $this->assertStringContainsString("<input type=\"text\" pattern=\"^.*$\" name=\"interface.title\" value=\"Zoph\" size=\"30\">", (string) $output);
        $this->assertStringContainsString("<select name=\"maps.provider\">", (string) $output);

        // Check if the password has changed
        $user = (new validator("admin", "S3cr3t"))->validate();
        $this->assertInstanceOf('user', $user);

        // Restore password
        $user->set("password", validator::hashPassword("admin"));
        $user->update();

    }

    public function testFinish() {
        $session = session::getCurrent();
        $session["token"] = self::$token;
        
        $app = new app(mockupRequest::createRequest("install/finish", post: array(
            "token"             => self::$token,
            "interface.title"   => "Zoph Test"
        )));
        $app->run();
        $view = $app->view;

        // Restore configuration
        conf::set("path.images", getcwd() . "/.images")->update();

        $this->assertInstanceOf(install\view\finish::class, $view);

        app::unsetMode(app::INSTALL);
        conf::loadFromDb();
        $title = conf::get("interface.title");
        $this->assertEquals("Zoph Test", $title);
    }

    public function getActions() {
        return array(
            array("display", install\view\display::class),
            array("requirements", install\view\requirements::class),
            array("iniform", install\view\iniform::class),
            array("inifile", install\view\inifile::class),
            array("dbpass", install\view\dbpass::class),
            array("nonexistent", install\view\display::class)
        );
    }
}
