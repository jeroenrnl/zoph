<?php
/**
 * Image controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use image\controller;
use import\web as webimport;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the image controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class imageControllerTest extends TestCase {

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("image/" . $action, get: array(
            "photo_id"  => "5"
        )));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test display action
     */
    public function testActionDisplay() {
        $app = new app(mockupRequest::createRequest("image/display", get: array(
            "photo_id"  => "5"
        )));
        $app->run();

        $this->assertInstanceOf(image\view\display::class, $app->view);

        $image = imagecreatefromstring($app->view->view());

        $photo=new photo(5);
        $photo->lookup();

        // Compare dimensions to stored size
        $this->assertEquals($photo->get("width"), imagesx($image));

        $modified=gmdate("D, d M Y H:i:s", filemtime($photo->getFilePath())) . " GMT";
        $exp_header=array(
            "Content-Length: " . strlen($app->view->view()),
            "Content-Disposition: inline; filename=" . $photo->get("name"),
            "Last-Modified: " . $modified,
            "Content-type: image/jpeg"
        );

        $this->assertEquals($exp_header, $app->view->getheaders());
    }

    /**
     * Test display action - not found
     */
    public function testActionDisplayNotFound() {
        $app = new app(mockupRequest::createRequest("image/display", get: array(
            "photo_id"  => "9999"
        )));
        $app->run();

        $this->assertInstanceOf(web\view\notfound::class, $app->view);
    }

    /**
     * Test display action - with share and hash
     */
    public function testActionDisplayShareHash() {
        conf::set("share.enable", true);
        conf::set("share.salt.full", "TestSaltFull");
        conf::set("share.salt.mid", "TestSaltMid");

        $photo=new photo(3);
        $photo->lookup();
        $storedHash=$photo->getHash();

        foreach (array("full", "mid") as $type) {
            if ($type=="full") {
                $hash=sha1("TestSaltFull" . $storedHash);
                $expPhoto=new file($photo->getFilePath());

            } else if ($type=="mid") {
                $hash=sha1("TestSaltMid" . $storedHash);
                $expPhoto=new file($photo->getFilePath(MID_PREFIX));
            }

            $app = new app(mockupRequest::createRequest("image/display", get: array(
                "hash"  => $hash
            )));
            $app->run();

            $this->assertInstanceOf(image\view\display::class, $app->view);

            $image = imagecreatefromstring($app->view->view());
            $expImage = imagecreatefromjpeg($expPhoto);
            $this->assertEquals($expImage, $image);
        }
    }

    /**
     * Test display action - with share and unknown hash
     */
    public function testActionDisplayShareHashNotFound() {
        conf::set("share.enable", true);
        conf::set("share.salt.full", "TestSaltFull");
        conf::set("share.salt.mid", "TestSaltMid");

        $app = new app(mockupRequest::createRequest("image/display", get: array(
            "hash"  => "nophotowiththishash"
        )));
        $app->run();

        $this->assertInstanceOf(web\view\notfound::class, $app->view);
    }

    /**
     * Test display action - watermarked photo
     */
    public function testActionDisplayWatermarked() {
        conf::set("watermark.enable", true)->update();
        conf::set("watermark.file", "copyright.gif")->update();

        // User jimi is a member of the group guitarists
        // which has access to album 1, which contains photo 7
        // The group guitarists has a an access level of 5 and a
        // watermark level of 3 in this photo, so if we set the
        // level of this photo between those numbers, they should get
        // a watermarked photo.
        user::setCurrent(new user(3));


        $photo=new photo(7);
        $photo->lookup();
        $photo->set("level", 4);
        $photo->update();

        $app = new app(mockupRequest::createRequest("image/display", get: array(
            "photo_id"  => 7
        )));
        $app->run();

        $this->assertInstanceOf(image\view\display::class, $app->view);

        $wmphoto = $app->controller->getObject();
        $this->assertInstanceOf(photo\watermarked::class, $wmphoto);

        user::setCurrent(new user(1));
    }

    /**
     * Test mid action
     * also test if modified since caching functionality
     */
    public function testActionMid() {
        $photo=new photo(5);
        $photo->lookup();

        $modified=gmdate("D, d M Y H:i:s", filemtime($photo->getFilePath())) . " GMT";

        $app = new app(mockupRequest::createRequest("image/mid",
            get: array(
                "photo_id"  => "5"
            ),
            server: array(
                "HTTP_IF_MODIFIED_SINCE" => $modified
            )
        ));
        $app->run();

        $this->assertInstanceOf(image\view\display::class, $app->view);

        $image = imagecreatefromstring($app->view->view());

        // Compare dimensions to stored size
        $this->assertEquals(MID_SIZE, imagesx($image));


        $_SERVER['HTTP_IF_MODIFIED_SINCE'] = $modified;


        $exp_header=array(
            "HTTP/1.1 304 Not Modified"
        );

        $this->assertEquals($exp_header, $app->view->getheaders());
    }

    /**
     * Test thumb action
     */
    public function testActionThumb() {
        $photo=new photo(5);
        $photo->lookup();

        $app = new app(mockupRequest::createRequest("image/thumb", get: array(
            "photo_id"  => "5"
        )));
        $app->run();

        $this->assertInstanceOf(image\view\display::class, $app->view);

        $image = imagecreatefromstring($app->view->view());

        // Compare dimensions to stored size
        $this->assertEquals(THUMB_SIZE, imagesx($image));
    }

    /**
     * Test background action - default photos
     */
    public function testActionBackground() {
        conf::set("share.enable", true)->update();
        conf::set("interface.logon.background.album", 7)->update();

        $photo = new photo(6);
        $photo->lookup();

        $hash = $photo->get("hash");;
        $salt = conf::get("share.salt.full");
        $exp_hash = sha1($salt . $hash);

        $app = new app(mockupRequest::createRequest("image/background"));
        $app->run();

        $this->assertInstanceOf(web\view\redirect::class, $app->view);
        $this->assertEquals(array("Location: /image?hash=" . $exp_hash), $app->view->getHeaders());
    }

    /**
     * Test background action - default backgrounds
     */
    public function testActionBackgroundDefault() {
        conf::set("share.enable", true)->update();
        conf::set("interface.logon.background.album", null)->update();

        $app = new app(mockupRequest::createRequest("image/background"));
        $app->run();

        $this->assertInstanceOf(web\view\redirect::class, $app->view);

        // The image is chosen randomly, so we just check for the path portion and not the image name
        $headers = $app->view->getHeaders();
        $this->assertStringContainsString("Location: /templates/default/images/backgrounds/", array_pop($headers));
    }

    /**
     * Test importThumb and importMid action
     */
    public function testActionImportThumbMid() {
        $dir=conf::get("path.images") . "/" . conf::get("path.upload") . "/";

        for ($i = 1; $i <= 3; $i++) {
            $file = realpath($dir . "IMPORT_000" . $i . ".JPG");
            $md5 = md5($file);

            webimport::processFile($md5);

            #
            # Test action importThumb
            #

            $thumbFile = realpath($dir . "thumb/thumb_IMPORT_000" . $i . ".JPG");
            $thumbData = file_get_contents($thumbFile);

            $app = new app(mockupRequest::createRequest("image/importThumb", get: array(
                "file"  => $md5
            )));
            $app->run();

            $this->assertInstanceOf(image\view\display::class, $app->view);

            $this->assertEquals($thumbData, $app->view->view());

            #
            # Test action importMid
            #

            $midFile = realpath($dir . "mid/mid_IMPORT_000" . $i . ".JPG");
            $midData = file_get_contents($midFile);

            $app = new app(mockupRequest::createRequest("image/importMid", get: array(
                "file"  => $md5
            )));
            $app->run();

            $this->assertInstanceOf(image\view\display::class, $app->view);

            $this->assertEquals($midData, $app->view->view());
        }
    }

    public function getActions() {
        return array(
            array("display",        image\view\display::class),
            array("background",     web\view\redirect::class),
            array("mid",            image\view\display::class),
            array("thumb",          image\view\display::class),
            array("nonexistent",    image\view\display::class)
        );
    }

}
