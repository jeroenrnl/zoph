<?php
/**
 * Web Service controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;
use conf\conf;
use import\web as webImport;
use web\request;
use zoph\app;

/**
 * Test the web service controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class webServiceControllerTest extends TestCase {

    /**
     * Test Action Details
     */
    public function testActionDetails() {
        $app = new app(mockupRequest::createRequest("service/details",
            get: array(
                "search"    => "place",
                "id"        => 4
            )
        ));

        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);

        // not testing the full XML here, that is done in the place object
        $this->assertStringContainsString("<id>4</id>", $view->view());
    }

    /**
     * Test Action Details with not allowed object
     */
    public function testActionDetailsNotAllowed() {
        $app = new app(mockupRequest::createRequest("service/details",
            get: array(
                "search"    => "user",
                "id"        => 1
            )
        ));

        $app->run();
        $view = $app->view;

        $this->assertNull($view);
    }

    /**
     * Test location lookup action
     */
    public function testActionLocationLookup() {
        $app = new app(mockupRequest::createRequest("service/locationLookup",
            get: array(
                "search"    => "7GXHX4HM MM"
            ),
            server: array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(29.9791875, $json["lat"]);
        $this->assertEquals(31.1341875, $json["lon"]);
        $this->assertEquals(17, $json["zoom"]);
    }

    /**
     * Test photoData action
     */
    public function testActionPhotoData() {
        $app = new app(mockupRequest::createRequest("service/photoData",
            get: array(
                "photoId"  => 3
            ),
            server: array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals("TEST_0003.JPG", $json["name"]);
        $this->assertEquals("Rotterdam", $json["location"]);
        $this->assertCount(2, $json["albums"]);
        $this->assertCount(2, $json["categories"]);
        $this->assertCount(2, $json["people"]);
    }

    /**
     * Test search action
     */
    public function testActionSearch() {
        $app = new app(mockupRequest::createRequest("service/search",
            get: array(
                "album_id"  => 3
            ),
            server: array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals([1,2,8], $json);
    }

    /**
     * Test photopeople with a photo that is inaccessible
     */
    public function testActionPhotoPeopleNoAccess() {
        user::setCurrent(new user(6));
        $app = new app(mockupRequest::createRequest("service/photoPeople",
            get: array(
                "action"    => "left",
                "photoId"  => "2",
                "personId"  => "7"
            ),
            server: array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals("insufficient rights", $json["error"]);
        user::setCurrent(new user(1));
    }

    /**
     * Test photopeople with no action
     * (returns the current people on the photo)
     */
    public function testActionPhotoPeopleActionEmpty() {
        $app = new app(mockupRequest::createRequest("service/photoPeople",
            get: array(
                "photoId"  => "2",
            ),
            server: array(
                "SERVER_NAME"   => "test.zoph.org"
            )
        ));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "left" action
     * (moves a person to the left)
     */
    public function testActionPhotoPeopleActionLeft() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "left",
            "photoId"  => "2",
            "personId"  => "7"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][0]["name"]); // swapped
        $this->assertEquals(5, $json["people"][1][1]["id"]);                // swapped
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "right" action
     * (moves a person to the right)
     */
    public function testActionPhotoPeopleActionRight() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "right",
            "photoId"  => "2",
            "personId"  => "7"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);                // swapped back
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]); // swapped back
        $this->assertEquals(3, $json["people"][1][2]["position"]);
    }

    /**
     * Test photopeople with "up" action
     * (moves a person a row up)
     */
    public function testActionPhotoPeopleActionUp() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "up",
            "photoId"   => "2",
            "personId"  => "9"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(2, $json["people"][0][1]["position"]);         // moved up
        $this->assertEquals("John Deacon", $json["people"][0][1]["name"]); // moved up
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
    }

    /**
     * Test photopeople with "down" action
     * (moves a person a row down)
     */
    public function testActionPhotoPeopleActionDown() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "down",
            "photoId"   => "2",
            "personId"  => "9"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(2, $json["people"][0][0]["id"]);
        $this->assertEquals(5, $json["people"][1][0]["id"]);
        $this->assertEquals("Roger Taylor", $json["people"][1][1]["name"]);
        $this->assertEquals(3, $json["people"][1][2]["position"]);         // moved back down
        $this->assertEquals("John Deacon", $json["people"][1][2]["name"]); // moved back down
    }

    /**
     * Test photopeople with "add" action
     * (adds a person)
     */
    public function testActionPhotoPeopleActionAdd() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "add",
            "photoId"  => "2",
            "personId"  => "3",
            "row"       => "3"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertEquals(3, $json["people"][2][0]["id"]);
    }

    /**
     * Test photopeople with "remove" action
     * (removes a person)
     */
    public function testActionPhotoPeopleActionRemove() {
        $app = new app(mockupRequest::createRequest("service/photoPeople", get: array(
            "action"    => "remove",
            "photoId"  => "2",
            "personId"  => "3"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals(2, $json["photoId"]);
        $this->assertArrayNotHasKey(2, $json["people"]);
    }

    /**
     * Test translate action
     */
    public function testActionTranslate() {
        $lang = language::getCurrent();
        $curLang = clone $lang;
        $lang = new language("nl");
        $lang->read();
        $app = new app(mockupRequest::createRequest("service/translation"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $this->assertEquals("Foto", $json["Photo"]);

        $lang = language::getCurrent($curLang);
    }

    public function testActionFeature() {
        $app = new app(mockupRequest::createRequest("service/feature", get: array(
            "type"  => "albums"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\json::class, $view);
        $json = json_decode($view->view(), true);

        $album = array_pop($json["objects"]);

        $expAlbum = array(
            "id"    => 6,
            "title" => "Album 20",
            "cover" => 5
        );

        $this->assertEquals("albums", $json["type"]);
        $this->assertEquals($expAlbum, $album);
    }

    public function testActionImportThumbs() {
        $dir=conf::get("path.images") . "/" . conf::get("path.upload") . "/";
        copy(getcwd() . "/tests/createTestData/xmptest4.jpg", $dir . "xmptest4.jpg");

        webImport::processFile(md5(realpath($dir . "xmptest4.jpg")));

        $app = new app(mockupRequest::createRequest("service/importThumbs"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<files>", $view->view());

        // Check if the file has been found and correctly identied the person in the XMP
        $this->assertStringContainsString("<file name=\"xmptest4.jpg\" type=\"image\">", $view->view());
        $this->assertStringContainsString("<person>Brian May</person>", $view->view());
    }

    public function testActionAlbum() {
        $app = new app(mockupRequest::createRequest("service/album"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<albums>", $view->view());
    }

    public function testActionCategory() {
        $app = new app(mockupRequest::createRequest("service/category"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<categories>", $view->view());
    }

    public function testActionPlace() {
        $app = new app(mockupRequest::createRequest("service/place"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<places>", $view->view());
    }

    public function testActionLocation() {
        $app = new app(mockupRequest::createRequest("service/location"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<places>", $view->view());
    }

    public function testActionPerson() {
        $app = new app(mockupRequest::createRequest("service/person"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<people>", $view->view());
    }

    public function testActionPhotographer() {
        $app = new app(mockupRequest::createRequest("service/photographer"));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(web\service\view\xml::class, $view);
        $this->assertStringContainsString("<people>", $view->view());
    }

}
