<?php
/**
 * Colour schemes controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use template\colorScheme\controller;
use template\colorScheme;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the colour schemes controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class colorSchemesControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("colourscheme/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("colourscheme/display", get: array(
            "color_scheme_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(template\colorScheme\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("colourscheme/edit?color_scheme_id=1", (string) $template);
        $this->assertStringContainsString("default", (string) $template);
        $this->assertStringContainsString("Selected tab bg color", (string) $template);
        $this->assertStringContainsString("<div class=\"color\" style=\"background: #111111;\">&nbsp;</div>", (string) $template);

    }

    /**
     * Test the "displayAll" action
     */
    public function testDisplayAllAction() {
        $app = new app(mockupRequest::createRequest("colourscheme/displayAll"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(template\colorScheme\view\displayAll::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("colourscheme/new", (string) $template);
        $this->assertStringContainsString("<div class=\"cs_example\" style=\"background: #111111\"></div>", (string) $template);
        $this->assertStringContainsString("<th scope=\"col\">preview</th>", (string) $template);
        $this->assertStringContainsString("Color Schemes", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("colourscheme/delete", get: array(
            "color_scheme_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(template\colorScheme\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm delete default", (string) $template);
        $this->assertStringContainsString("colourscheme/confirm?color_scheme_id=1", (string) $template);
    }

    /**
     * Create colour scheme in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("colourscheme/insert", post: array(
            "name"                      => "Green",
            "page_bg_color"             => "#001100",
            "text_color"                => "#002200",
            "link_color"                => "#003300",
            "vlink_color"               => "#004400",
            "table_bg_color"            => "#005500",
            "table_border_color"        => "#006600",
            "breadcrumb_bg_color"       => "#007700",
            "title_bg_color"            => "#008800",
            "title_font_color"          => "#009900",
            "tab_bg_color"              => "#00aa00",
            "tab_font_color"            => "#00bb00",
            "selected_tab_bg_color"     => "#00dd00",
            "selected_tab_font_color"   => "#00ee00"
        ))); // I haven't checked this, but it's probably *very* green :-)
        $app->run();

        $view = $app->view;

        $cs=$app->controller->getObject();

        $this->assertInstanceOf(template\colorScheme\view\display::class, $view);
        $this->assertEquals("Green", $cs->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);
        $output = helpers::whitespaceClean((string) $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Green", (string) $output);
        $this->assertStringContainsString("<dt>Tab bg color</dt><dd><div class=\"colordef\">00aa00</div><div class=\"color\" style=\"background: #00aa00;\">&nbsp;</div></dd>", (string) $output);
        $this->assertStringContainsString("<dl class=\"display colorScheme\">", (string) $output);

        return $cs;
    }

    /**
     * Update colour scheme in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(colorScheme $cs) {
        $app = new app(mockupRequest::createRequest("colourscheme/update", post: array(
            "name"          => "Very green",
            "color_scheme_id"      => $cs->getId(),
            "tab_bg_color"    => "00ff00"
        )));
        $app->run();

        $view = $app->view;

        $cs=$app->controller->getObject();

        $this->assertInstanceOf(template\colorScheme\view\update::class, $view);

        $this->assertEquals("Very green", $cs->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);
        $output = helpers::whitespaceClean((string) $template);

        // Check a few random strings that should appear in the output
        $this->assertStringNotContainsString("Green", (string) $output);
        $this->assertStringContainsString("Very green", (string) $output);
        $this->assertStringContainsString("<input type=\"color\" name=\"tab_bg_color\" value=\"#00ff00\">", (string) $output);

        return $cs;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("colourscheme/new"));
        $app->run();

        $view = $app->view;
        $cs=$app->controller->getObject();

        $this->assertInstanceOf(template\colorScheme::class, $cs);
        $this->assertEquals(0, $cs->getId());

        $this->assertInstanceOf(template\colorScheme\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("/colourscheme/displayAll", (string) $template);
        $this->assertStringContainsString("New Color Scheme", (string) $template);
        $this->assertStringContainsString("<input type=\"color\" name=\"tab_font_color\" value=\"", (string) $template);
        $this->assertStringContainsString("New Color Scheme", $view->getTitle());
    }

    /**
     * Test confirm (delete) acrion
     * @depends testUpdateAction
     */
    public function testConfirmAction(colorScheme $cs) {
        $app = new app(mockupRequest::createRequest("colourscheme/confirm", get: array(
            "color_scheme_id"  => $cs->getId()
        )));
        $app->run();

        $view = $app->view;

        $ids=array_map(function($c) { return $c->getId(); }, colorScheme::getAll());
        $this->assertNotContains($cs->getId(), $ids);

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /colourscheme/displayAll"), $view->getHeaders());
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());
    }

    public function getActions() {
        return array(
            array("new", template\colorScheme\view\update::class),
            array("edit", template\colorScheme\view\update::class),
            array("delete", template\colorScheme\view\confirm::class),
            array("displayAll", template\colorScheme\view\displayAll::class),
            array("nonexistant", template\colorScheme\view\display::class)
        );
    }
}
