<?php
/**
 * Pageset controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use page\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the page controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class pageControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("page/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }


    /**
     * Create page
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("page/insert",
            post: array(
                "page_id"       => "",
                "title"         => "Test Page",
                "text"          => "[h1]test page[/h1][mid=1]",
            ),
            server: array(
                "REMOTE_ADDR"   => "1.2.3.4"
            )
        ));
        $app->run();

        $view = $app->view;

        $page=$app->controller->getObject();

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /page?page_id=" . $page->getId()), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $page;
    }

    /**
     * Test the "display" action
     * @depends testInsertAction
     */
    public function testDisplayAction(page $page) {
        $app = new app(mockupRequest::createRequest("page", get: array(
            "page_id"  => $page->getId()
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(page\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Test Page", (string) $template);
        $this->assertStringContainsString("<h1>test page</h1>", (string) $template);
        $this->assertStringContainsString("/page/edit?page_id=" . $page->getId(), (string) $template);
    }

    /**
     * Test the "pages" action
     * @depends testInsertAction
     */
    public function testPagesAction(page $page) {
        $app = new app(mockupRequest::createRequest("page/pages"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(page\view\pages::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<table class=\"pages\">", (string) $template);
        $this->assertStringContainsString("Test Page", (string) $template);
        $this->assertStringContainsString("/page?page_id=" . $page->getId(), (string) $template);
    }

    /**
     * Test the "delete" action
     * @depends testInsertAction
     */
    public function testDeleteAction(page $page) {
        $app = new app(mockupRequest::createRequest("page/delete", get: array(
            "page_id"  => $page->getId()
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(page\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm deletion of 'Test Page'", (string) $template);
        $this->assertStringContainsString("/page/confirm?page_id=" . $page->getId(), (string) $template);
    }

    /**
     * Test the "edit", "delete" and "confirm" action - by non-admin user
     * @depends testInsertAction
     * @dataProvider getEditDeleteConfirmAction
     */
    public function testEditDeleteConfirmUnauthorisedAction(string $action, page $page) {
        user::setCurrent(new user(5));

        $app = new app(mockupRequest::createRequest("page/" . $action, get: array(
            "page_id"  => $page->getId()
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(page\view\display::class, $view);

        $pageIds=array_map(function($p) { return $p->getId(); }, page::getAll());
        $this->assertContains($page->getId(), $pageIds);

        user::setCurrent(new user(1));
    }

    /**
     * Update page
     * @depends testInsertAction
     */
    public function testUpdateAction(page $page) {
        $app = new app(mockupRequest::createRequest("page/update", post: array(
            "page_id"  => $page->getId(),
            "title"       => "A page for testing",
        )));
        $app->run();

        $view = $app->view;

        $page=$app->controller->getObject();

        $this->assertInstanceOf(page\view\display::class, $view);

        $this->assertEquals("A page for testing", $page->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("A page for testing", (string) $template);
        $this->assertStringNotContainsString("Test Page", (string) $template);
        $this->assertStringContainsString("/page/edit?page_id=" . $page->getId(), (string) $template);

        return $page;
    }

    /**
     * Test update by unauthorised user
     * @depends testUpdateAction
     */
    public function testUpdateNotAuthorised(page $page) {
        user::setCurrent(new user(5));

        $app = new app(mockupRequest::createRequest("page/update", post: array(
            "page_id"  => $page->getId(),
            "title"       => "hacked"
        )));
        $app->run();

        $view = $app->view;

        $page=$app->controller->getObject();

        $this->assertInstanceOf(page\view\display::class, $view);

        $this->assertEquals("A page for testing", $page->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("A page for testing", (string) $template);
        $this->assertStringNotContainsString("hacked", (string) $template);

        user::setCurrent(new user(1));
        return $page;
    }

    /**
     * Delete page
     * @depends testUpdateNotAuthorised
     */
    public function testConfirmAction(page $page) {
        $app = new app(mockupRequest::createRequest("page/confirm", get: array(
            "page_id"  => $page->getId()
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /page/pages"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        $pageIds=array_map(function($p) { return $p->getId(); }, page::getAll());
        $this->assertNotContains($page->getId(), $pageIds);
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("page/new"));
        $app->run();

        $view = $app->view;

        $page=$app->controller->getObject();
        $this->assertInstanceOf(page::class, $page);
        $this->assertEquals(0, $page->getId());

        $this->assertInstanceOf(page\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"page_id\" value=\"\">", (string) $template);
    }

    public function getActions() {
        return array(
            array("new", page\view\update::class),
            array("edit",page\view\update::class),
            array("delete", page\view\confirm::class),
            array("display", page\view\display::class),
            array("nonexistant", page\view\display::class)
        );
    }

    public function getEditDeleteConfirmAction() {
        return [["edit"], ["delete"], ["confirm"]];
    }
}
