<?php
/**
 * Test set model
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use set\model as set;
use PHPUnit\Framework\TestCase;

/**
 * Test the model class for sets
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class setModelTest extends TestCase {
    /**
     * Create sets in the database
     * @dataProvider getSets();
     */
    public function testCreateAndDeleteSets($name, $photos) {
        $set=new set();
        $set->set("name",$name);
        $set->insert();
        $this->assertInstanceOf(set::class, $set);

        foreach ($photos as $photoId) {
            $photo = new photo($photoId);
            $set->addPhoto($photo);
        }

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals($photos, $photoIds);

        $this->assertEquals(count($photos), $set->getPhotocount());

        $id = $set->getId();
        $set->delete();

        $retry=new set($id);
        $this->assertEquals(0, $retry->lookup());
    }

    /**
     * Test add / remove photo
     */
    public function testAddRemovePhoto() {
        $set=new set();
        $set->set("name", "Test add remove");
        $set->insert();

        $photo = new photo(1);
        $set->addPhoto($photo);

        $photoIds = array();
        foreach ($set->getPhotos() as $p) {
            $photoIds[] = $p->getId();
        }
        $this->assertEquals([1], $photoIds);

        $this->assertEquals(1, $set->getPhotocount());

        $set->removePhoto($photo);

        $photoIds = array();
        foreach ($set->getPhotos() as $p) {
            $photoIds[] = $p->getId();
        }
        $this->assertEquals([], $photoIds);

        $this->assertEquals(0, $set->getPhotocount());

        $set->delete();
    }

    /**
     * Test getCover function
     */
    public function testGetCoverphoto() {
        $set=new set();
        $set->set("name", "coverphototestset");
        $set->set("coverphoto", 1);
        $set->insert();

        $cover=$set->getAutoCover();
        $this->assertInstanceOf(photo::class, $cover);

        $this->assertEquals(1, $cover->getId());
        $set->set("coverphoto", null);
        $set->update();
    }

    /**
     * Test movePhoto function
     */
    public function testMovePhoto() {
        $set=new set();
        $set->set("name", "set for testMovePhoto");
        $set->insert();

        foreach (array(1,2,3) as $i) {
            $set->addPhoto(new photo($i));
        }

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([1,2,3], $photoIds);


        $set->movePhoto(new photo(1), new photo(3));

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([2,1,3], $photoIds);

        $set->movePhoto(new photo(3), new photo(2));

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([3,2,1], $photoIds);

        // move to end
        $set->movePhoto(new photo(2), new photo(0));

        $photoIds = array();
        foreach ($set->getPhotos() as $photo) {
            $photoIds[] = $photo->getId();
        }
        $this->assertEquals([3,1,2], $photoIds);
    }

    /**
     * dataProvider function
     * @return array name, photos
     */
    public function getSets() {

        return array(
            array("TestSet1", [ 3, 1, 2]),
            array("TestSet2", [ 4, 6, 7, 9, 5, 8 ] )
        );
    }

}
