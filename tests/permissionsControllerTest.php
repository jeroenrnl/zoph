<?php
/**
 * Permission controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use permissions\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the group controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class permissionControllerTest extends TestCase {

    /**
     * Update all albums
     */
    public function testUpdateAllAlbumsAction() {
        // Remove current permissions first
        $albums=album::getAll();
        foreach ($albums as $album) {
            $perm=new permissions(3, $album->getId());
            $perm->delete();
        }

        $app = new app(mockupRequest::createRequest("permissions/updatealbums", get: array(
            "_access_level_all_checkbox"    => 1,
            "group_id"                      => 3,
            "access_level_all"              => 2,
            "writable_all"                  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /group/edit?group_id=3"), $view->getHeaders());


        $albums=album::getAll();
        foreach ($albums as $album) {

            $perm=new permissions(3, $album->getId());
            $perm->lookup();

            $this->assertEquals(2, $perm->get("access_level"));
            $this->assertEquals(1, $perm->get("writable"));
            $this->assertEquals(0, $perm->get("watermark_level"));
        }
    }

    /**
     * Remove album permissions
     */
    public function testUpdateRemoveAlbumsAction() {
        $app = new app(mockupRequest::createRequest("permissions/updatealbums", get: array(
            "_access_level_all_checkbox"    => 1,
            "_remove_permission_album__2"   => 1,
            "group_id"                      => 3
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /group/edit?group_id=3"), $view->getHeaders());


        $perm=new permissions(3, 2);
        $perm->lookup();

        $this->assertEquals("", $perm->get("access_level"));
    }

    /**
     * Remove album permissions
     */
    public function testUpdateAddAlbumsAction() {
        $perm=new permissions(3, 1);
        $perm->delete();
        conf::set("watermark.enable", true);
        $app = new app(mockupRequest::createRequest("permissions/updatealbums", get: array(
            "album_id_new"          => 1,
            "group_id"              => 3,
            "group_id_new"          => 3,
            "access_level_new"      => 4,
            "watermark_level_new"   => 6,
            "writable_new"          => 0
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /group/edit?group_id=3"), $view->getHeaders());


        $perm=new permissions(3, 1);
        $perm->lookup();

        $this->assertEquals(4, $perm->get("access_level"));
        $this->assertEquals(0, $perm->get("writable"));
        $this->assertEquals(6, $perm->get("watermark_level"));
        conf::set("watermark.enable", false);
    }

    /**
     * Update all groups
     */
    public function testUpdateAllGroupsAction() {
        $app = new app(mockupRequest::createRequest("permissions/updategroups", get: array(
            "_access_level_all_checkbox"    => 1,
            "album_id"                      => 4,
            "access_level_all"              => 2,
            "writable_all"                  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /album/edit?album_id=4"), $view->getHeaders());

        $groups=group::getAll();
        foreach ($groups as $group) {

            $perm=new permissions($group->getId(), 4);
            $perm->lookup();

            $this->assertEquals(2, $perm->get("access_level"));
            $this->assertEquals(1, $perm->get("writable"));
            $this->assertEquals(0, $perm->get("watermark_level"));

            // Delete newly created permissions
            $perm->delete();
            $perm=new permissions($group->getId(), 3);
            $perm->lookup();
            $perm->delete();
        }
    }

    /**
     * Remove album permissions
     */
    public function testUpdateRemoveGroupAction() {
        $app = new app(mockupRequest::createRequest("permissions/updategroups", get: array(
            "_remove_permission_group__2"   => 1,
            "album_id"                      => 3
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /album/edit?album_id=3"), $view->getHeaders());


        $perm=new permissions(2, 3);
        $perm->lookup();

        $this->assertEquals("", $perm->get("access_level"));

        // Re-add permissions to the db
        $perm=new permissions(2, 3);
        $perm->set("access_level", 5);
        $perm->set("watermark_level", 3);
        $perm->set("writable", 0);
        $perm->insert();

    }

    /**
     * Add group permissions
     */
    public function testUpdateAddGroupsAction() {
        $perm=new permissions(3, 1);
        $perm->delete();
        conf::set("watermark.enable", true);
        $app = new app(mockupRequest::createRequest("permissions/updategroups", get: array(
            "album_id_new"                  => 1,
            "album_id"                      => 1,
            "group_id_new"                  => 3,
            "access_level_new"              => 4,
            "watermark_level_new"           => 6,
            "writable_new"                  => 0),
        ));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /album/edit?album_id=1"), $view->getHeaders());


        $perm=new permissions(3, 1);
        $perm->lookup();

        $this->assertEquals(4, $perm->get("access_level"));
        $this->assertEquals(0, $perm->get("writable"));
        $this->assertEquals(6, $perm->get("watermark_level"));

        // Restore settings
        conf::set("watermark.enable", false);
        $perm->delete();
    }

}

