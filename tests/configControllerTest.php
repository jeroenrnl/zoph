<?php
/**
 * Config controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use conf\view\display;
use conf\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the config controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class confControllerTest extends TestCase {

    protected function setUp() : void {
        user::setCurrent(new user(1));
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("config"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Interface settings", (string) $template);
        $this->assertStringContainsString("Import through webinterface", (string) $template);
        $this->assertStringContainsString("<input type=\"text\" pattern=\"^&#091;aABgGhHisueIOPTZcrU\/ \-&#040;&#041;:,.&#093;+$\" name=\"date.timeformat\"", (string) $template);

    }

    /**
     * Update config in the db
     */
    public function testUpdateAction() {
        $title = conf::get("interface.title");
        $app = new app(mockupRequest::createRequest("config/update", post: array(
            "interface.title"       => "Changing the config"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(display::class, $view);

        $this->assertEquals("Changing the config", conf::get("interface.title"));
        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Changing the config", (string) $template);
        conf::set("interface.title", $title)->update();
    }
}
