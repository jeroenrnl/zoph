<?php
/**
 * Unittests for photo/people class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use db\db;
use conf\conf;

use photo\collection as photoCollection;
use PHPUnit\Framework\TestCase;

/**
 * Test photo class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoPeopleTest extends TestCase {

    /**
     * Setup testcase
     * Make sure admin user is logged in
     */
    public function setUp() : void {
        parent::setUp();
        user::setCurrent(new user(1));
        conf::set("path.trash", "");
    }

    /**
     * Test getData function
     */
    public function testGetData() {
        $photoPeople = new photo\people(new photo(7));

        $data = $photoPeople->getData();

        $exp = array(
            "photoId"   => 7,
            "people"    => array(
                array(  // row 0
                    array(
                        "id"    => 2,
                        "name"  => "Brian May",
                        "url"   => "/person?person_id=2",
                        "row"   => 0,
                        "position"   => 1
                    ), array(
                        "id"    => 3,
                        "name"  => "Jimi Hendrix",
                        "url"   => "/person?person_id=3",
                        "row"   => 0,
                        "position"   => 2
                    )
                ), array(   // row 1
                    array(
                        "id"    => 5,
                        "name"  => "Freddie Mercury",
                        "url"   => "/person?person_id=5",
                        "row"   => 1,
                        "position"   => 1
                    )
                )
            )
        );

        $this->assertEquals($exp, $data);

    }
    /**
     * Test adding people
     * @dataProvider getPeople
     */
    public function testAddPerson($photo, array $newpers) {
        $ids=array();
        $photo=new photo($photo);
        foreach ($newpers as $pers) {
            $photo->addTo(new person($pers));
        }
        $peo=(new photo\people($photo))->getAll();
        foreach ($peo as $per) {
            $ids[]=$per->getId();
            $this->assertInstanceOf(person::class, $per);
        }
        foreach ($newpers as $per_id) {
            $this->assertContains($per_id, $ids);
        }
        foreach ($newpers as $pers) {
            $photo->removeFrom(new person($pers));
        }
    }

    /**
     * Test get position for a person
     * @dataProvider getPeoplePos
     */
    public function testGetPosition($photoId, $personId, $expRow, $expPos) {
        $photoPeople = new photo\people(new photo($photoId));
        list($row, $pos) = $photoPeople->getPosition(new person($personId));
        $this->assertEquals($expRow, $row);
        $this->assertEquals($expPos, $pos);
    }

    /**
     * Test getting people in rows
     * @dataProvider getPeopleRows
     */
    public function testGetPeopleInRows($photoId, array $people) {
        $this->assertEquals($people, $this->peopleToKeyArray($photoId));
    }

    /**
     * Test setting positon of a person (swapping two persons)
     */
    public function testSetPosition() {
        $photoPeople = new photo\people(new photo(1));
        $photoPeople->setPosition(new person(7),2);

        $act=$this->peopleToKeyArray(1);
        $exp = array([2,5], [9,7]);
        $this->assertEquals($exp, $act);

        // Move a person to a position that is not already
        // occupied - this would cause a gap in the positions
        // and is therefore not executed. The positions
        // should be unchanged
        $photoPeople = new photo\people(new photo(2));
        $photoPeople->setPosition(new person(5),4);

        $act=$this->peopleToKeyArray(2);
        $exp = array([2], [5,7,9]);
        $this->assertEquals($exp, $act);
    }

    /**
     * Test setting row of a person
     */
    public function testSetRow() {
        $photoPeople = new photo\people(new photo(2));
        $photoPeople->setRow(new person(5),0);

        $act=$this->peopleToKeyArray(2);
        $exp = array([2,5], [7,9]);
        $this->assertEquals($exp, $act);

        $photoPeople->setRow(new person(5), -1);

        $act=$this->peopleToKeyArray(2);
        $exp = array([5], [2], [7,9]);
        $this->assertEquals($exp, $act);

        // When we set the row to -1, all persons should automatically be shifted
        // so that the first row remains 0, despite setting the row to -1 above,
        // the person should be on row 0 now.
        list($row, $pos) = $photoPeople->getPosition(new person(5));
        $this->assertEquals($row, 0);

        // Returning everyone to their 'starting positions'
        $photoPeople->setRow(new person(2), 0);
        $photoPeople->setRow(new person(5), 1);
        $photoPeople->setRow(new person(7), 1);
        $photoPeople->setRow(new person(9), 1);
    }

    /**
     * Test photo\people::getForPosition()
     */
    public function testGetForPosition() {
        $photoPeople = new photo\people(new photo(2));
        $person=$photoPeople->getForPosition(1,2);
        $this->assertEquals(7, $person->getId());
    }



    private function peopleToKeyArray($photoId) {
        $rows=(new photo\people(new photo($photoId)))->getInRows();
        $ids=array();
        foreach ($rows as $row) {
            $rowids=array();
            foreach ($row as $person) {
                $rowids[]=$person->getId();
            }
            $ids[]=$rowids;
        }
        return $ids;
    }


    //================== DATA PROVIDERS =======================

    public function getPeople() {
        return array(
            array(1, array(3,4,6)),
            array(2, array(1,6,8)),
            array(3, array(7)),
            array(4, array(6,9))
        );
    }

    public function getPeoplePos() {
        return array(
            array(1, 5, 0, 2),
            array(1, 7, 1, 1),
            array(7, 2, 0, 1),
            array(7, 5, 1, 1)
        );
    }

    public function getPeopleRows() {
        return array(
            array(1, array(array(2,5), array(7,9))),
            array(2, array(array(2), array(5,7,9))),
            array(6, array(array(2,3))),
        );
    }
}
