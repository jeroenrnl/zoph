<?php
/**
 * Photo controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use photo\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use web\session;
use zoph\app;

/**
 * Test the photo controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class photoControllerTest extends TestCase {

    /**
     * Test actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("photo/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test display action
     */
    public function testActionDisplay() {
        $app = new app(mockupRequest::createRequest("photo/display", get: array(
            "photo_id"  => "1"

        )));
        $app->run();

        $this->assertInstanceOf(photo\view\display::class, $app->view);
    }

    /**
     * Test display action
     */
    public function testActionDisplayOffset() {
        $app = new app(mockupRequest::createRequest("photo/display", get: array(
            "album_id"  => "2",
            "_off"      => "1"
        )));

        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(photo\view\display::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("/image?photo_id=7", (string) $output);
        $this->assertStringContainsString("/photo?album_id=2&amp;_off=0", (string) $output); // Prev link
        $this->assertStringContainsString("/photos?album_id=2&_off=0", (string) $output); // Up link
        $this->assertStringNotContainsString("/photo?album_id=2&amp;_off=2", (string) $output); // No Next link
    }

    /**
     * Test update action
     */
    public function testActionUpdate() {
        $app = new app(mockupRequest::createRequest("photo/update", get: array(
            "photo_id"  => "1",
            "title"     => "Updated by testActionUpdate"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(photo\view\display::class, $view);

        $photo=new photo(1);
        $photo->lookup();

        $this->assertEquals("Updated by testActionUpdate", $photo->get("title"));
    }

    /**
     * Test delete action
     * delete does not delete the photo, it shows the 'confirm' view.
     */
    public function testActionDelete() {
        $app = new app(mockupRequest::createRequest("photo/delete", get: array(
            "photo_id"  => "1",
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(photo\view\confirm::class, $view);

        $output = helpers::whitespaceClean((string) $view->view());
        $this->assertStringContainsString("photo/confirm?photo_id=1", (string) $output);
        $this->assertStringContainsString("photo?photo_id=1", (string) $output); // Prev link

    }

    /**
     * Test confirm action
     */
    public function testActionConfirm() {
        $photo = new photo();
        $photo->insert();
        $photoId = $photo->getId();

        $app = new app(mockupRequest::createRequest("photo/confirm", get: array(
            "photo_id"  => $photoId,
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $photoIds=array_map(function($p) { return $p->getId(); }, photo::getAll());

        $this->assertNotContains($photoId, $photoIds);

    }

    /**
     * Test rate action
     */
    public function testActionRate() {
        $app = new app(mockupRequest::createRequest("photo/rate", post: array(
            "_qs"       => "photo_id=7",
            "photo_id"  => "7",
            "rating"    => "10"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /photo?photo_id=7"), $view->getHeaders());

        $photo=new photo(7);
        $photo->lookup();

        $this->assertEquals(10, $photo->getRating());

        $ratings=rating::getRatings($photo, user::getCurrent());
        return array_shift($ratings);
    }

    /**
     * Test delrate action
     * @depends testActionRate
     */
    public function testActionDelrate(rating $rating) {
        $rating->lookup();

        $app = new app(mockupRequest::createRequest("photo/delrate", get: array(
            "_rating_id" => $rating->getId(),
            "_return"   => "/zoph/welcome"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /zoph/welcome"), $view->getHeaders());

        $photo=new photo(7);
        $photo->lookup();

        $this->assertEquals(null, $photo->getRating());
    }

    /**
     * Test select action
     */
    public function testActionSelect() {
        $session  = new session(array());
        $app = new app(mockupRequest::createRequest("photo/select", get: array(
            "photo_id"  => "7",
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(photo\view\display::class, $view);

        $session = $session->getCurrent();
        $this->assertEquals(7, $session["selected_photo"][0]);
    }

    /**
     * Test deselect action
     */
    public function testActionDeselect() {
        $session  = new session(array(
            "selected_photo"    => array(7)
        ));
        $app = new app(mockupRequest::createRequest("photo/deselect", get: array(
            "photo_id"  => "7",
            "_return"   => "photo",
            "_qs"       => "photo_id=1"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /photo?photo_id=1"), $view->getHeaders());

        $session = $session->getCurrent();
        $this->assertEmpty($session["selected_photo"]);
    }

    /**
     * Test lightbox action
     */
    public function testActionLightbox() {
        $user = user::getCurrent();
        $user->set("lightbox_id", 2);
        $user->update();

        $app = new app(mockupRequest::createRequest("photo/lightbox", get: array(
            "photo_id"  => "8",
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $photo = $app->controller->getObject();
        $this->assertInstanceOf(photo::class, $photo);
        $this->assertEquals(8, $photo->getId());

        $albumIds=array_map(function($a) { return $a->getId(); }, $photo->getAlbums());
        $this->assertContains(2, $albumIds);
    }

    /**
     * Test unlightbox action
     * @depends testActionLightbox
     */
    public function testActionUnlightbox() {
        $user = user::getCurrent();
        $app = new app(mockupRequest::createRequest("photo/unlightbox", get: array(
            "photo_id"  => "8",
            "_qs"       => "album_id=8&_off=1",
            "_return"   => "photo"
        )));
        $app->run();

        $view = $app->view;

        $photo = $app->controller->getObject();

        $this->assertInstanceOf(web\view\redirect::class, $view);
        $this->assertEquals(array("Location: /photo?album_id=8&_off=1"), $view->getHeaders());

        $albumIds=array_map(function($a) { return $a->getId(); }, $photo->getAlbums());
        $this->assertNotContains(2, $albumIds);

        $user->set("lightbox_id", null);
        $user->update();
    }

    public function getActions() {
        return array(
            array("display", photo\view\display::class),
            array("insert", photo\view\display::class),
            array("new", photo\view\display::class),
            array("edit", photo\view\update::class),
            array("nonexistent", photo\view\display::class)
        );
    }

}
