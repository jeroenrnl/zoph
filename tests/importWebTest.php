<?php
/**
 * This tests import/web class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;
use conf\conf;
use import\web as webImport;

/**
 * Test the web import
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class importWebTest extends TestCase {

    public function testImportArchive() {
        conf::set("path.unzip", "unzip");
        $dir = conf::get("path.images") . "/" . conf::get("path.upload") . "/";
        $zip = realpath($dir . "/TEST.ZIP");
        $md5 = md5($zip);

        webImport::processFile($md5);
        for ($i = 1; $i <= 3; $i++) {
            $file = $dir . "/TEST_ZIP_" . $i . ".JPG";
            $this->assertFileExists($file);
            unlink($file);
        }

    }

    public function testImportXMPSideCar() {
        $path = conf::get("path.images") . "/" . conf::get("path.upload") . "/";
        copy(getcwd() . "/tests/createTestData/xmptest3.jpg", $path . "xmpsidecartest.jpg");
        copy(getcwd() . "/tests/createTestData/xmptest3.xmp", $path . "xmpsidecartest.xmp");

        $filename = $path . "xmpsidecartest.jpg";
        $file = new file($filename);
        $md5 = md5($filename);
        
        $photos = webImport::photos(array(
            $md5                => $file
        ), array(
            "_sidecar_" . $md5  => "xmpsidecartest.xmp",
            "_category_" . $md5  => array(2),   
            "_rating_" . $md5  => 7
        ));
        

        $photo = array_pop($photos);
        $path = conf::get("path.images") . "/" . $photo->get("path");

        $this->assertFileExists($path . "/xmpsidecartest.jpg");
        $this->assertFileExists($path . "/xmpsidecartest.xmp");



        $categories = $photo->getCategories();
        $category = array_pop($categories)->getId();

        $this->assertEquals(2, $category);

        $rating = $photo->getRating();
        $this->assertEquals(7, $rating);

        $photo->delete();

        unlink($path . "/xmpsidecartest.jpg");
        unlink($path . "/xmpsidecartest.xmp");
    }
}

