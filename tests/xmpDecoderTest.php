<?php
/**
 * Unittests for XMP Decoder class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use PHPUnit\Framework\TestCase;

use xmp\decoder as xmpDecoder;
use xmp\data as xmpData;

require_once "testSetup.php";

/**
 * Test XMP Decoder class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class xmpDecoderTest extends TestCase {


    public function testGetSubjects() {
        $data = array(
            "x:xmpmeta" => array(
                "rdf:RDF" => array(
                    "rdf:Description" => array(
                        "exif:CameraMake"   => "Zoph",
                        "exif:CameraModel"  => "SuperZoom 2000",
                        "dc:subject" =>  array(
                            "XMP Test",
                            "XMP Data"
                        ),
                        "xmp:Rating"    => 10
                    )
                )
            )
        );

        $xmp = new xmpDecoder(array(new xmpData($data)));

        $subjects = $xmp->getSubjects();
        $this->assertInstanceOf("xmp\\data", $subjects);
        $this->assertCount(2, $subjects);

        $expectedSubjects = array("XMP Test", "XMP Data");

        $actualSubjects = array();
        foreach ($subjects as $subject) {
            $actualSubjects[] = $subject;
        }

        $this->assertEquals($expectedSubjects, $actualSubjects);
    }

    public function testRating() {
        $data = array(
            "x:xmpmeta" => array(
                "rdf:RDF" => array(
                    "rdf:Description" => array(
                        "exif:CameraMake"   => "Zoph",
                        "exif:CameraModel"  => "SuperZoom 2000",
                        "dc:subject" =>  array(
                            "XMP Test",
                            "XMP Data"
                        ),
                        "xmp:Rating"    => 10
                    )
                )
            )
        );

        $xmp = new xmpDecoder(array(new xmpData($data)));

        $rating = $xmp->getRating();

        $this->assertEquals(10, $rating);
    }
}
