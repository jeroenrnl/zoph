<?php
/**
 * Unittests for XMP Reader
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

use PHPUnit\Framework\TestCase;

use conf\conf;
use xmp\reader as xmpReader;

require_once "testSetup.php";

/**
 * Test XMP Reader
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class xmpReaderTest extends TestCase {


    public function testConstruct() {
        $xmpreader = new xmpReader(new file("/fake/file"));

        $this->assertInstanceOf("xmp\\reader", $xmpreader);
    }

    public function testReadFromSidecar() {

        // sidecar.jpg does not actually exist, but the getXMPfromSidecar() function
        // doesn't care about that, there is, however, a file called sidecar.xmp and
        // this function should find that file...
        $xmpreader = new xmpReader(new file(conf::get("path.images") . "/sidecar.jpg"));

        $xmp = $xmpreader->getXMPfromSidecar();

        $this->assertIsArray($xmp);
        $this->assertCount(1, $xmp);
        $xmpdata = array_pop($xmp);

        $this->assertInstanceOf("xmp\\data", $xmpdata);

        // Check one random entry to verify it has been loaded and decoded correctly
        $this->assertEquals("Canon Digital Camera", $xmpdata["x:xmpmeta"]["rdf:RDF"]["rdf:Description"]["tiff:Model"]);
    }

    public function testReadFromJPEG() {

        $xmpreader = new xmpReader(new file(conf::get("path.images") . "/xmptest.jpg"));

        $xmp = $xmpreader->getXMP();

        $this->assertIsArray($xmp);
        $this->assertCount(1, $xmp);
        $xmpdata = array_pop($xmp);

        $this->assertInstanceOf("xmp\\data", $xmpdata);

        // Check one random entry to verify it has been loaded and decoded correctly
        $this->assertEquals(
            "converted from application/vnd.adobe.photoshop to image/jpeg",
            $xmpdata["x:xmpmeta"]["rdf:RDF"]["rdf:Description"]["xmpMM:History"][3]["stEvt:parameters"]);
    }
}


