<?php
/**
 * Search controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the search controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class searchControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("search/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test 'search' action
     */
    public function testSearchAction() {
        $app = new app(mockupRequest::createRequest("search/search", get: array(
            "album_id"      => 5,
            "_search"       => "search"
        )));
        $app->run();
        $view = $app->view;
        $search=$app->controller->getObject();

        $this->assertInstanceOf(photos\view\display::class, $view);
    }

    /**
     * Create search in the db
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("search/new", get: array(
            "album_id"      => array("0" => "5")
        )));
        $app->run();
        $view = $app->view;
        $search=$app->controller->getObject();

        $this->assertInstanceOf(search\view\update::class, $view);
        $this->assertEquals("album_id%5B0%5D=5", $search->get("search"));

        return $search;
    }

    /**
     * Create search in the db
     * @depends testNewAction
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("search/insert", post: array(
            "search"        => "album_id#0=5",
            "name"          => "Search for Album 2",
        )));
        $app->run();
        $view = $app->view;
        $search=$app->controller->getObject();

        $this->assertInstanceOf(search\view\display::class, $view);
        $this->assertEquals("Search for Album 2", $search->getName());

        return $search;
    }

    /**
     * Update search in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(search $search) {
        $app = new app(mockupRequest::createRequest("search/update", post: array(
            "search_id"     => $search->getId(),
            "name"          => "Search for the second album",
        )));
        $app->run();
        $view = $app->view;

        $search=$app->controller->getObject();

        $this->assertInstanceOf(search\view\update::class, $view);
        $this->assertEquals("Search for the second album", $search->getName());

        return $search;
    }

    /**
     * Test confirm (delete) acrion
     * @depends testUpdateAction
     */
    public function testConfirmAction(search $search) {
        $id=$search->getId();
        $app = new app(mockupRequest::createRequest("search/confirm", get: array(
            "search_id"      => $id,
        )));
        $app->run();
        $view = $app->view;

        $searches=search::getAll();
        $ids=array();
        foreach ($searches as $search) {
            $ids[]=$search->getId();
        }
        $this->assertNotContains($id, $ids);
    }

    public function getActions() {
        return array(
            array("display", search\view\display::class),
            array("new", search\view\update::class),
            array("edit", search\view\update::class),
            array("delete", search\view\confirm::class),
            array("nonexistant", search\view\display::class)
        );
    }
}
