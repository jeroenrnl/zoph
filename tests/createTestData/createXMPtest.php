<?php

set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php");
set_include_path(get_include_path() . PATH_SEPARATOR . getcwd() . "/php/classes");

require_once "testData.php";
require_once "testImage.php";

$name="xmptest4.jpg";

$image=new testImage();
$image->setName($name);

$image->setLocation(4);     // Rotterdam
$image->setPhotographer(5); // Freddie Mercury
$image->addPerson(2);       // Brian May
$image->addToCategory(2);   // Red
$image->addToCategory(5);   // Blue

$image->writeImage();
copy("/tmp/" . $name, getcwd() . "/" . $name);
exec("exiftool -Xmp:PersonInImage+='Brian May' ./" . $name);

