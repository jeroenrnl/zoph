<?php
/**
 * User controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use auth\validator;
use user\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the user controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class userControllerTest extends TestCase {

    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("user/" . $action));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf($expView, $view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("user/display", get: array(
            "user_id"  => 5
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(user\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user/edit?user_id=5", (string) $template);
        $this->assertStringContainsString("freddie", (string) $template);
        $this->assertStringContainsString("Freddie Mercury", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("user/delete", get: array(
            "user_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(user\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("delete user", (string) $template);
        $this->assertStringContainsString("user/confirm?user_id=1", (string) $template);
    }

    /**
     * Create user in the db
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("user/insert", post: array(
            "user_name"    => "eric"
        )));
        $app->run();

        $view = $app->view;
        $user = $app->controller->getObject();

        $this->assertInstanceOf(user\view\display::class, $view);
        $this->assertEquals("eric", $user->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user/password?user_id=" . $user->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"" . $user->getId() . "\">", (string) $template);
        $this->assertStringContainsString("eric", (string) $template);

        return $user;
    }

    /**
     * Update user in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(user $user) {
        $app = new app(mockupRequest::createRequest("user/update", post: array(
            "user_id"      => $user->getId(),
            "user_name"    => "ericburton",
        )));
        $app->run();

        $view = $app->view;

        $user=$app->controller->getObject();

        $this->assertInstanceOf(user\view\update::class, $view);

        $this->assertEquals("ericburton", $user->getName());

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user/password?user_id=" . $user->getId(), (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"" . $user->getId() . "\">", (string) $template);
        $this->assertStringContainsString("ericburton", (string) $template);

        return $user;
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("user/new"));
        $app->run();

        $view = $app->view;

        $user=$app->controller->getObject();
        $this->assertInstanceOf("user", $user);
        $this->assertEquals(0, $user->getId());

        $this->assertInstanceOf(user\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("user/users", (string) $template);
        $this->assertStringContainsString("New user", (string) $template);
        $this->assertStringContainsString("New user", $view->getTitle());
    }

    /**
     * Test show users
     */
    public function testUsersAction() {
        $app = new app(mockupRequest::createRequest("user/users"));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(user\view\users::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<table class=\"users\">", (string) $template);
        $this->assertStringContainsString("Users", $view->getTitle());
        $this->assertStringContainsString("freddie", (string) $template);
        $this->assertStringContainsString("Freddie Mercury", (string) $template);
        $this->assertStringContainsString("user?user_id=5", (string) $template);
        $this->assertStringContainsString("person?person_id=5", (string) $template);
    }

    /**
     * Test update by non-admin user
     */
    public function testUpdateNotAuthorized() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $testUser = new user(3);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $app = new app(mockupRequest::createRequest("user/update", post: array(
            "user_id"      => 3,
            "user_name"    => "hacked",
            "_password"    => "hacked"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(user\view\password::class, $view);

        // Verify username and password have NOT been changed.
        $user = new user(3);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertEquals($testPwd, $user->get("password"));

        user::setCurrent(new user(1));
    }
    /**
     * Test display by non-admin user
     */
    public function testDisplayNotAuthorized() {
        $this->expectException(securityException::class);
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("user/display", get: array(
            "user_id"  => 3
        )));
        $app->run();
    }

    /**
     * Test change someone else's password by admin user
     */
    public function testUserPasswordAdmin() {
        user::setCurrent(new user(1));

        $app = new app(mockupRequest::createRequest("user/password", get: array(
            "user_id"    => 3
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for jimi", $view->getTitle());
        $this->assertStringContainsString("<div class=\"messageText\">Make sure password and confirm match</div>", (string) $template);
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"3\">", (string) $template);
    }

    /**
     * Test change someone else's password by non-admin user
     * should ignore the user_id and just present the own password
     */
    public function testUserOtherPassword() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("user/password", get: array(
            "user_id"    => 3
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for freddie", $view->getTitle());
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"5\">", (string) $template);

        user::setCurrent(new user(1));
    }

    /**
     * Test change own password by non-admin user
     */
    public function testUserPassword() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("user/password"));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(user\view\password::class, $view);

        $template = $view->view();

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Change password for freddie", $view->getTitle());
        $this->assertStringContainsString("<input type=\"hidden\" name=\"user_id\" value=\"5\">", (string) $template);

        user::setCurrent(new user(1));
    }

    /**
     * Test change password by non-admin user - Update action
     */
    public function testPasswordNotAdminUpdate() {
        $unauthUser = new user(5);
        user::setCurrent($unauthUser);

        $testUser = new user(5);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $app = new app(mockupRequest::createRequest("user/update", post: array(
            "_password"      => "changed"
        )));
        $app->run();

        $view = $app->view;
        $this->assertInstanceOf(user\view\password::class, $view);

        // Verify password has been changed.
        $user = new user(5);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

        user::setCurrent(new user(1));
    }

    /**
     * Test change own password by admin user - Update action
     */
    public function testPasswordAdminUpdate() {
        $testUser = new user(1);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $app = new app(mockupRequest::createRequest("user/update", post: array(
            "_password"      => "changed"
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(user\view\update::class, $view);

        // Verify password has been changed.
        $user = new user(1);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

        $user->set("password", validator::hashPassword("admin"));
        $user->update();
    }

    /**
     * Test change someone else's password by admin user - Update action
     */
    public function testUserPasswordAdminUpdate() {
        $testUser = new user(3);
        $testUser->lookup();
        $testName = $testUser->getName();
        $testPwd = $testUser->get("password");

        $app = new app(mockupRequest::createRequest("user/update", post: array(
            "_password" => "changed",
            "user_id" => 3,
        )));
        $app->run();
        $view = $app->view;

        $this->assertInstanceOf(user\view\update::class, $view);

        // Verify password has been changed.
        $user = new user(3);
        $user->lookup();
        $this->assertEquals($testName, $user->getName());
        $this->assertNotEquals($testPwd, $user->get("password"));

    }

    public function getActions() {
        return array(
            array("new", user\view\update::class),
            array("edit", user\view\update::class),
            array("delete", user\view\confirm::class),
            array("users", user\view\users::class),
            array("nonexistant", user\view\display::class)
        );
    }
}
