<?php
/**
 * Comment controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use comment\controller;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the comment controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class commentControllerTest extends TestCase {

    public static function setUpBeforeClass() : void {
        conf::set("feature.comments", true)->update();
    }

    public static function tearDownAfterClass() : void {
        conf::set("feature.comments", false)->update();
        user::setCurrent(new user(1));
        $user=new user(5);
        $user->lookup();
        $user->set("leave_comments", 1);
        $user->update();
    }


    /**
     * Test the "new", "edit" and "delete" actions
     * also tests handing an illegal action, this should result in
     * "display".
     * @dataProvider getActions
     */
    public function testBasicActions($action, $expView) {
        $app = new app(mockupRequest::createRequest("comment/" . $action));
        $app->run();

        $this->assertInstanceOf($expView, $app->view);
    }

    /**
     * Test the "display" action
     */
    public function testDisplayAction() {
        $app = new app(mockupRequest::createRequest("comment/display", get: array(
            "comment_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(comment\view\display::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Comment by brian", (string) $template);
        $this->assertStringContainsString("/user?user_id=2", (string) $template);
        $this->assertStringContainsString("/comment/edit?comment_id=1", (string) $template);
        $this->assertStringContainsString("/photo?photo_id=1", (string) $template);

    }

    /**
     * Test the "delete" action
     */
    public function testDeleteAction() {
        $app = new app(mockupRequest::createRequest("comment/delete", get: array(
            "comment_id"  => 1
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(comment\view\confirm::class, $view);

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Confirm deletion of comment '<b>Comment by brian</b>' by '<b>brian</b>'", (string) $template);
        $this->assertStringContainsString("/comment/confirm?comment_id=1", (string) $template);
    }

    /**
     * Create comment
     */
    public function testInsertAction() {
        $app = new app(mockupRequest::createRequest("comment/insert",
            post: array(
                "comment_id"    => "",
                "photo_id"      => 2,
                "subject"       => "Test comment",
                "comment"       => "Hello world"
            ),
            server: array(
                "REMOTE_ADDR"   => "1.2.3.4"
            )
        ));
        $app->run();

        $view = $app->view;

        $comment=$app->controller->getObject();

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /photo?photo_id=2"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        return $comment;
    }

    /**
     * Update comment in the db
     * @depends testInsertAction
     */
    public function testUpdateAction(comment $comment) {
        $app = new app(mockupRequest::createRequest("comment/update", post: array(
            "comment_id"    => $comment->getId(),
            "photo_id"      => 2,
            "subject"       => "Test comment",
            "comment"       => "Hello moon"
        )));
        $app->run();

        $view = $app->view;

        $comment=$app->controller->getObject();

        $this->assertInstanceOf(comment\view\display::class, $view);

        $this->assertEquals("Hello moon", $comment->get("comment"));

        $template = $view->view();

        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("Hello moon", (string) $template);
        $this->assertStringNotContainsString("Hello world", (string) $template);
        $this->assertStringContainsString("/user?user_id=1", (string) $template);
        $this->assertStringContainsString("/comment/edit?comment_id=" . $comment->getId(), (string) $template);
        $this->assertStringContainsString("/image/mid?photo_id=2", (string) $template);

        return $comment;
    }

    /**
     * Delete comment
     * @depends testUpdateAction
     */
    public function testConfirmAction(comment $comment) {
        $app = new app(mockupRequest::createRequest("comment/confirm", get: array(
            "comment_id"    => $comment->getId(),
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(web\view\redirect::class, $view);

        $this->assertEquals(array("Location: /photo?photo_id=2"), $view->getHeaders());

        $template = $view->view();
        $this->assertNull($view->view());
        $this->assertCount(0, $view->getActionLinks());

        $comments = (new photo(2))->getComments();
        $commentIds=array_map(function($c) { return $c->getId(); }, $comments);
        $this->assertEquals([4,5], $commentIds);
    }

    /**
     * Test create new form
     */
    public function testNewAction() {
        $app = new app(mockupRequest::createRequest("comment/new", get: array(
            "photo_id"    => 3
        )));
        $app->run();

        $view = $app->view;

        $comment=$app->controller->getObject();
        $this->assertInstanceOf(comment::class, $comment);
        $this->assertEquals(0, $comment->getId());

        $this->assertInstanceOf(comment\view\update::class, $view);

        $template = $view->view();
        $this->assertInstanceOf(template\block::class, $template);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString("<input type=\"hidden\" name=\"photo_id\" value=\"3\">", (string) $template);
    }

    /**
     * Test comment by unauthorised user
     */
    public function testCommentNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();

        $restore = clone $unauthUser;;
        user::setCurrent($unauthUser);

        $unauthUser->set("leave_comments", false);

        $app = new app(mockupRequest::createRequest("comment/insert",
            post: array(
                "comment_id"    => 1,
                "photo_id"      => 3,
                "subject"       => "hacked",
                "comment"       => "I am not allowed to, but I am commenting anyway"
            ),
            server: array(
                "REMOTE_ADDR"   => "1.2.3.4"
            )
        ));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    /**
     * Test changing comment by unauthorised user
     */
    public function testUpdateNotAuthorised() {
        $unauthUser = new user(5);
        $unauthUser->lookup();

        $restore = clone $unauthUser;;
        user::setCurrent($unauthUser);

        // The user can leave comments, but is still not allowed to change other people's comments!
        $unauthUser->set("leave_comments", true);

        $app = new app(mockupRequest::createRequest("comment/update", post: array(
            "comment_id"    => 1,
            "subject"       => "hacked",
            "comment"       => "I am not allowed to, but changed your comment anyway"
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    /**
     * Test display by non-admin user
     * Non admin users can only see comments on photos they can see
     */
    public function testDisplayNotAuthorized() {
        $unauthUser = new user(5);
        // The user can leave comments, but is still not allowed to see comments on photos they cannot see
        $unauthUser->set("leave_comments", 1);
        $unauthUser->update();

        user::setCurrent($unauthUser);

        $app = new app(mockupRequest::createRequest("comment/displau", get: array(
            "comment_id"    => 10,
        )));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf(generic\view\forbidden::class, $view);
    }

    public function getActions() {
        return array(
            array("new", comment\view\update::class),
            array("edit", comment\view\update::class),
            array("delete", comment\view\confirm::class),
            array("display", comment\view\display::class),
            array("nonexistant", comment\view\display::class)
        );
    }
}
