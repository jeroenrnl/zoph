<?php
/**
 * Actionlink - a template to create actionlinks
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use template\actionlink;
use web\request;
use zoph\app;
use PHPUnit\Framework\TestCase;

/**
 * Test result template
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class templateActionlinkTest extends TestCase {


    /**
     * Test creating an actionlink
     * @dataProvider getActionlinks
     */
    public function testActionlink($title, $link, $parameters, $exp_url) {
        $app = new app(mockupRequest::createRequest("zoph/test"));
        $app->run();

        $al = new actionlink($title, $link, $parameters);

        $output = helpers::whitespaceClean((string) $al);

        $this->assertStringContainsString($exp_url, $output);
        $this->assertStringContainsString($title, $output);
    }

    public function testActionlinkWithTranslation() {
        $lang = language::getCurrent();

        $app = new app(mockupRequest::createRequest("zoph/test"));
        $app->run();

        $curLang = clone $lang;
        $lang = new language("nl");
        $lang->read();

        $al = new actionlink("edit", "photo/edit", array("photo_id" => "1"));

        $output = helpers::whitespaceClean((string) $al);

        $this->assertStringContainsString("bewerken", $output);
        $this->assertStringContainsString("/photo/edit?photo_id=1", $output);

        language::setCurrent($curLang);
    }

    public function testActionlinkWithURL() {
        $app = new app(mockupRequest::createRequest("zoph/test"));
        $app->run();

        $al = new actionlink("return", url: "photo?photo_id=1");

        $output = helpers::whitespaceClean((string) $al);

        $this->assertStringContainsString("/photo?photo_id=1", $output);

    }

    public function testActionlinkWithSubdir() {
        $app = new app(mockupRequest::createRequest("zoph/test", server: array(
                "PHP_SELF"  => "/zophtest/index.php/zoph/test",
                "PATH_INFO" => "/zoph/test",
                "SCRIPT_NAME"   => "/zophtest/index.php"
        )));
        $app->run();

        $al = new actionlink("delete photo", "photo/confirm", array("photo_id" => "1"));

        $output = helpers::whitespaceClean((string) $al);

        $this->assertStringContainsString("/zophtest/photo/confirm?photo_id=1", $output);
    }

    public function testActionlinkWithSubdirAndURL() {
        $app = new app(mockupRequest::createRequest("zoph/test", server: array(
                "PHP_SELF"  => "/zophtest/index.php/zoph/test",
                "PATH_INFO" => "/zoph/test",
                "SCRIPT_NAME"   => "/zophtest/index.php"
        )));
        $app->run();

        $al = new actionlink("return", url: "photo?photo_id=1");

        $output = helpers::whitespaceClean((string) $al);

        $this->assertStringContainsString("/zophtest/photo?photo_id=1", $output);

        $app=new app(mockupRequest::createRequest("zoph/test"));
    }

    public function getActionlinks() {
        return array(
            array("show albums", "album/albums", array(), "/album/albums"),
            array("display album", "album", array("album_id" => 1), "/album?album_id=1"),
            array("edit album", "album/edit", array("album_id" => 1), "album/edit?album_id=1"),
            array("set coverphoto", "album/setcoverphoto", array(
                "album_id" => 1,
                "photo_id" => 5), "album/setcoverphoto?album_id=1&photo_id=5"),
            array("return", "photo/photos", array("return" => "photos?album_id[1]=1&category_id[1]=2"),
                "/photo/photos?return=photos%3Falbum_id%5B1%5D%3D1%26category_id%5B1%5D%3D2")
        );
    }

}
