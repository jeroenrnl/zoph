<?php
/**
 * Test CLI Authentication
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use cli\cli;
use conf\conf;
use PHPUnit\Framework\TestCase;

/**
 * Test CLI interface
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class authCliTest extends TestCase {

    /**
     * Make sure we're in CLI mode
     */
    protected function setUp() : void {
        if (!defined("CLI")) {
            define("CLI", true);
        }
    }

    /**
     * Test CLI authentication with pre-defined user
     */
    public function testAuthCliConfUser() {
        conf::set("interface.user.cli", 2);

        $auth = new auth\cli();
        $user = $auth->getUser();

        $this->assertEquals(2, $user->getId());
        $this->assertEquals("brian", $user->getName());
        $this->assertInstanceOf("language", $auth->getLang());
        $this->assertNull($auth->getRedirect());
    }

    /**
     * Test CLI authentication with OS user
     */
    public function testAuthCliOSUser() {
        conf::set("interface.user.cli", "0");
        $_SERVER["USER"]="freddie";

        $auth = new auth\cli();
        $user = $auth->getUser();

        $this->assertEquals(5, $user->getId());
        $this->assertEquals("freddie", $user->getName());
    }

    /**
     * Test CLI authentication with nonexistant OS user
     */
    public function testAuthCliNonExistantOSUser() {
        $this->expectException("\cliUserNotValidException");
        conf::set("interface.user.cli", "0");
        $_SERVER["USER"]="justin";

        $auth = new auth\cli();
        $user = $auth->getUser();

    }
}
