<?php
/**
 * template\controller test
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */

require_once "testSetup.php";

use conf\conf;
use PHPUnit\Framework\TestCase;
use web\request;
use zoph\app;

/**
 * Test the photo\relation\controller class
 *
 * @package ZophUnitTest
 * @author Jeroen Roos
 */
class templateControllerTest extends TestCase {

    /**
     * Test the various CSS actions
     * @dataProvider getActions
     */
    public function testCSSAction($url, $viewclass, $comment) {
        $app = new app(mockupRequest::createRequest($url));
        $app->run();

        $view = $app->view;

        $this->assertInstanceOf($viewclass, $view);

        // Check a few random strings that should appear in the output
        $this->assertStringContainsString($comment, (string) $view->view());

        $this->assertContains("Content-Type: text/css", $view->getHeaders());

    }

    /**
     * Test the a non-existant CSS file
     */
    public function testCSSNotFoundAction() {
        $this->expectException(fileNotFoundException::class);
        $app = new app(mockupRequest::createRequest("css/nonexistent.css"));
        $app->run();

        $view = $app->view;
    }

    public function getActions() {
        return array(
            array("css", template\view\view::class, "/* This is the base CSS */"),
            array("css/reset.css", template\view\css::class, "Reset Stylesheet"),
            array("css/leaflet.css", template\view\css::class, "/* required styles */"),
            array("css/colours.css", template\view\colours::class, "/* This is the colours CSS */"),
            array("css/variables.css", template\view\variables::class, "/* This is the variables CSS */"),
        );
    }
}
