This directory contains examples of EXIF data from real camera's to use while testing.
The actual image information is removed to keep the size small and avoid copyright issues.

Make a copy of the photo, so you do not destroy your actual photos. Make sure the naming refects the make and model of the camera:
```
cp IMG_0001.JPG Canon_EOS_600D.JPG
```
Remove personal information from photo:
```
exiftool -Artist:"Zoph" Canon_EOS_600D.JPG
exiftool -internalserialnumber="ZOPH0000" Canon_EOS_600D.JPG
exiftool -serialnumber="ZOPH0000" Canon_EOS_600D.JPG
exiftool -lensserialnumber="ZOPH0000" Canon_EOS_600D.JPG
```
check if there are other fields you need to wipe:
```
exiftool Canon_EOS_600D.JPG
```

Remove thumbnail:
```
jhead -dt Canon_EOS_600D.JPG
```

Create a blank image with the make and model:
```
magick -size 300x200 -background white -fill blue -gravity center label:"Canon\nEOS 600D" white.jpg
```
Now use jhead to copy the EXIF from your photo to the blank photo:
```
jhead -te Canon_EOS_600D.JPG white.jpg
```
and rename the white photo:
```
mv white.jpg Canon_EOS_600D.JPG
```

Either create a fork and a merge request to add your photo or create a new issue and attach it. If there is a specific reason to do so (e.g. problems in Zoph with photos from your camera) please state that in the image.
