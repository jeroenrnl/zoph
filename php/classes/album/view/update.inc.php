<?php
/**
 * View for edit album page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace album\view;

use album;
use conf\conf;
use pageset;
use permissions\view\edit as editPermissions;
use template\actionlink;
use template\block;
use template\template;
use user;
use web\request;


/**
 * This view displays the album page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : ?array {
        if ($this->request["album_id"] != 0) {
            $id = (int) $this->request["album_id"];
        } else if ($this->object->getId() != 0) {
            $id = (int) $this->object->getId();
        } else if ($this->request["parent_album_id"] != 0) {
            $id = (int) $this->request["parent_album_id"];
        } else {
            $id = false;
            $param = array();
            $parentParam=array();
        }

        if ($id) {
            $param = array("album_id" => $id);
            $parentParam = array("parent_album_id" => $id);
        }

        $return = new actionlink("return", "album", $param);

        if ($this->request->getAction() == "new") {
            return array(
                $return,
            );
        } else {
            return array(
                $return,
                new actionlink("new", "album/new", $parentParam),
                new actionlink("delete", "album/delete", $param)
            );
        }
    }

    /**
     * Output the view
     */
    public function view() : block {
        $user = user::getCurrent();

        $actionlinks=$this->getActionlinks();

        if ($this->request->getAction() == "new") {
            $action = "insert";

            $permissions=new block("message", array(
                "class" => "info",
                "text" => translate("After this album has been created, groups can be given access to it.")
            ));
        } else {
            $action = "update";
            $editPermissions=new editPermissions($this->object);
            $permissions=$editPermissions->view();
        }

        return new block("editAlbum", array(
            "actionlinks"   => $actionlinks,
            "action"        => $action,
            "album"         => $this->object,
            "pagesets"      => template::createSelectArray(pageset::getAll("title"), array("title"), true),
            "title"         => $this->getTitle(),
            "user"          => $user,
            "permissions"   => $permissions
        ));
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        if ($this->request->getAction() == "new") {
            return translate("new album");
        } else {
            return translate("update album");
        }
    }
}
