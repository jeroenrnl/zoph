<?php
/**
 * Database table class, to describe tables
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

use template\result;

/**
 * Database table class, to describe tables
 *
 * @package Zoph
 * @author Jeroen Roos
 */
abstract class table {

    private $result = result::UNDEFINED;

    private $error;

    /**
     * Create a new table
     */
    final public function __construct() {}

    abstract protected function structure() : create|view ;

    protected function data() : array {
        return array();
    }

    public function __toString() : string {
        return (string) $this->structure();
    }

    final public function create() : bool {
        $qry = $this->structure();
        $stmt = $qry->execute();
        if (!empty($stmt->errorInfo()[2])) {
            $this->result = result::ERROR;
            $this->error = $stmt->errorInfo()[2];
        } else {
            $this->result = result::SUCCESS;
        }

        return $this->result;

    }

    final public function addData() : bool {
        foreach ($this->data() as $qry) {
            try {
                $qry->execute();
                $this->result = result::SUCCESS;
            } catch (exception $e) {
                $this->error = $e->getMessage();
                $this->result = result::ERROR;
                return $this->result;
            }
        }
        return $this->result;
    }

    final public function getStructure() {
        return $this->structure();
    }

    final public function getResult() : result {
        $classpath = explode("\\", get_class($this));
        return new result($this->result, array_pop($classpath), $this->error);
    }
}
