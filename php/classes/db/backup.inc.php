<?php
/**
 * Database class to create backups
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace db;

use db\exception\backup as databaseBackupException;

/**
 * Create a backup of the database
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class backup {
    private $host;
    private $dbname;
    private $user;
    private $pass;

    public function __construct(string $rootpwd = null) {
        $db=db::getLoginDetails();

        $this->host     = escapeshellarg($db["host"]);
        $this->dbname   = escapeshellarg($db["dbname"]);

        if ($rootpwd) {
            $this->user = escapeshellarg("root");
            $this->pass = escapeshellarg($rootpwd);
        } else {
            $this->user = escapeshellarg($db["user"]);
            $this->pass = escapeshellarg($db["pass"]);
        }
    }

    public function execute() {
        $mysqldumpDescSpec = array(
            1 => array("pipe", "w"),
            2 => array("pipe", "w")
        );

        $mysqldump = proc_open("mysqldump" .
            " -h " . $this->host .
            " -u " . $this->user .
            " -p" . $this->pass .
            " " . $this->dbname, $mysqldumpDescSpec, $mysqlPipes);
        $backup = stream_get_contents($mysqlPipes[1]);
        $mysqlError = stream_get_contents($mysqlPipes[2]);
        if (proc_close($mysqldump) !== 0) {
            throw new databaseBackupException("MySQL: " . $mysqlError);
        }

        return gzencode($backup, 9);
    }

}
