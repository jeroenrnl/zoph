<?php
/**
 * Controller for colorScheme
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\colorScheme;

use conf\conf;
use generic\controller as genericController;
use template\colorScheme;
use user;
use web\request;


/**
 * Controller for colorScheme
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewDisplayAll = view\displayAll::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "displayAll", "edit", "insert",
        "new", "update", "copy"
    );

    public function doAction(string $action) : void {
        $user=user::getCurrent();
        if (!$user->isAdmin()) {
            $this->actionDisplay();
        } else {
            if ($action == "new") {
                $this->setObject(new colorScheme());
            } else if ($action != "displayAll") {
                $colorScheme = new colorScheme((int) $this->request["color_scheme_id"]);
                $colorScheme->lookup();
                $this->setObject($colorScheme);
            }
            parent::doAction($action);
        }
    }

   /**
    * Do action 'copy'
    */
    public function actionCopy() {
        $this->object->set("name", "copy of " . $this->object->get("name"));
        $this->object->set("color_scheme_id", 0);
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

   /**
    * Do action 'displayAll'
    */
    public function actionDisplayAll() {
        $this->view = new static::$viewDisplayAll($this->request, $this->object);
    }

   /**
    * Do action 'confirm'
    */
    public function actionConfirm() {
        parent::actionConfirm();
        $this->view->setRedirect("colourscheme/displayAll");
    }

   /**
    * Do action 'update'
    */
    public function actionUpdate() {
        parent::actionUpdate();
        user::getCurrent()->prefs->load();
    }

}
