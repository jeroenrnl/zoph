<?php
/**
 * View to display css
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace template\view;

use web\view\viewInterface;
use app;
use conf\conf;
use web\request;
use template\block;
use template\template;

/**
 * View for template (CSS) display
 */
class view implements viewInterface {
    protected string $cssfile = "css.php";
    protected string $template = "default";

    public function __construct(protected request $request) {
        $this->template = conf::get("interface.template");
    }

    final public function getHeaders() : array {
        return array("Content-Type: text/css");
    }

    /**
     * Output CSS file
     * Cannot be covered with tests because of header output
     * @codeCoverageIgnore
     */
    final public function display() : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }

        echo $this->view();
    }

    public function getComment() : string {
        return "/* This is the base CSS */";
    }

    final public function getScripts() : array {
        return array();
    }

    final public function getTitle() : string {
        return "";
    }

    final public function view() : block|string|null {
        $css="templates/" . $this->template . "/" . $this->cssfile;
        if (!file_exists($css)) {
            $css="templates/default/" . $this->cssfile;
        }

        ob_start();
        echo $this->getComment() . "\n";
        require $css;
        return ob_get_clean();
    }

}
