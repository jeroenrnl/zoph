<?php
/**
 * Class that takes care of displaying menu items
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace template\menu;

use app;
use conf\conf;
use Closure;
use template\block;
use user;

/**
 * This class takes care of displaying menu items
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class item {
    private bool $selected = false;

    public function __construct(
        private string      $linkText,
        private string      $link,
        private ?string     $displayFunction = null,
        private ?bool       $negate = false,
        private ?string     $config = null,
        private ?array      $parameter = null
    ) {

    }

    public function __toString() : string {
        return (string) $this->display();
    }
    public function display() : string {
        if ($this->shouldBeDisplayed()) {
            $link = app::getBasePath() . $this->link;

            if (isset($this->parameter)) {
                $user = user::getCurrent();
                foreach ($this->parameter as $label => $value) {
                    $obj = call_user_func(array($user, $value));
                    $this->parameter[$label] = $obj->getId();
                }

                $link .= "?" . http_build_query($this->parameter);
            }

            return new block("menuitem", array(
                "title" => translate($this->linkText),
                "link"  => $link,
                "selected"  => $this->selected
            ));
        } else {
            return "";
        }
    }

    public function setSelected(?string $page = null) : void {
        $link = $this->link;
        if (!strpos($link, "/") && strpos($page, "/")) {
            $page = substr($page, 0, strpos($page, "/"));
        } else if (strpos($this->link, "/") && !strpos($page, "/")) {
            $link = substr($link, 0, strpos($link, "/"));
        }
        if (is_null($page) || $page == $link) {
            $this->selected = true;
        }
    }

    public function shouldBeDisplayed() : bool {
        if (!empty($this->config)) {
            $conf = conf::get($this->config);
            $display = $this->negate ? !$conf : $conf;
            if (!$display) {
                return false;
            }
        }
        $user = user::getCurrent();

        if (is_null($this->displayFunction)) {
            return true;
        } else {
            $f = $this->displayFunction;
            $display = $this->negate ? fn() => !call_user_func(array($user, $f)) : fn() => call_user_func(array($user,$f));
            return (bool) $display();
        }
    }
}
