<?php
/**
 * Class that takes care of displaying the menu
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace template\menu;

use template\block;
use conf\conf;
use user;
use zoph\app;

/**
 * This class takes care of displaying the menu
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class main extends block {
    public function __construct($template = "menu") {
        parent::__construct($template, array(
            "tabs"  => app::getMenu(),
        ));

    }
}
