<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;
use user;
use userNotFoundException;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class users extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("users");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("user_id"))->int()->notNull()->autoIncrement(),
            (new column("person_id"))->int()->default("NULL"),
            (new column("user_class"))->char(1)->notNull()->default("1"),
            (new column("view_all_photos"))->char(1)->notNull()->default("0"),
            (new column("delete_photos"))->char(1)->notNull()->default("0"),
            (new column("user_name"))->varchar(16)->notNull()->default(""),
            (new column("password"))->varchar(255)->default("NULL"),
            (new column("browse_people"))->char(1)->notNull()->default("0"),
            (new column("browse_places"))->char(1)->notNull()->default("0"),
            (new column("browse_tracks"))->char(1)->notNull()->default("0"),
            (new column("edit_organizers"))->char(1)->notNull()->default("0"),
            (new column("detailed_people"))->char(1)->notNull()->default("0"),
            (new column("see_hidden_circles"))->char(1)->notNull()->default("0"),
            (new column("detailed_places"))->char(1)->notNull()->default("0"),
            (new column("import"))->char(1)->notNull()->default("0"),
            (new column("download"))->char(1)->notNull()->default("0"),
            (new column("leave_comments"))->char(1)->notNull()->default("0"),
            (new column("allow_rating"))->char(1)->notNull()->default("1"),
            (new column("allow_multirating"))->char(1)->notNull()->default("0"),
            (new column("allow_share"))->char(1)->notNull()->default("0"),
            (new column("lightbox_id"))->int()->default("NULL"),
            (new column("lastnotify"))->datetime()->default("NULL"),
            (new column("lastlogin"))->datetime()->default("NULL"),
            (new column("lastip"))->varchar(48)->default("NULL")
        ));

        return $table;
    }

    protected function data() : array {
        $return = array();
        try {
            user::getByName("admin");
        } catch (userNotFoundException $e) {
            $qry = new insert("users");
            $qry->addParam(new param(":user_id", 1, PDO::PARAM_INT));
            $qry->addParam(new param(":person_id", 1, PDO::PARAM_INT));
            $qry->addParam(new param(":user_class", 0, PDO::PARAM_INT));
            $qry->addParam(new param(":user_name", "admin", PDO::PARAM_STR));
            $qry->addParam(new param(":password", '$2y$10$cFl.NZJDITXBg6Etl3udd.JpNkgzsRIwBoJf7f5AWqi9Itv1HJY6q', PDO::PARAM_STR)); // "admin"

            $perms = array("view_all_photos", "delete_photos", "browse_people", "browse_places", "browse_tracks", "edit_organizers", "detailed_people", "see_hidden_circles", "detailed_places", "import", "download", "leave_comments");
            foreach ($perms as $perm) {
                $qry->addParam(new param(":" . $perm, 1, PDO::PARAM_INT));
            }
            $return = array($qry);
        }
        return $return;
    }
}
