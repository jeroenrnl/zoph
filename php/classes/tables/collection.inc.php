<?php
/**
 * Collection of all tables
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use generic\collection as genericCollection;
use db\create;

/**
 * This is a collection of all tables for Zoph
 *
 * @package Zoph
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class collection extends genericCollection {
    protected $items=array();


    public function __construct() {
        $this->items = array(
            new albums(),
            new categories(),
            new circles(),
            new circles_people(),
            new color_schemes(),
            new comments(),
            new conf(),
            new group_permissions(),
            new groups(),
            new groups_users(),
            new pageset(),
            new pages(),
            new pages_pageset(),
            new people(),
            new photo_albums(),
            new photo_categories(),
            new photo_comments(),
            new photo_people(),
            new photo_ratings(),
            new photo_relations(),
            new photo_sets(),
            new photos(),
            new places(),
            new point(),
            new prefs(),
            new saved_search(),
            new sets(),
            new track(),
            new users(),
            new view_photo_avg_rating(),
            new view_photo_user(),
            new view_albums_details()
        );
    }
}
