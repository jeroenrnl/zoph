<?php
/**
 * Table description
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace tables;

use PDO;
use db\clause;
use db\column;
use db\create;
use db\insert;
use db\param;
use db\select;
use db\table;

/**
 * This is a class to generate a table in Zoph's database
 *
 * @package ZophTable
 * @author Jeroen Roos
 *
 * @codeCoverageIgnore
 */
class prefs extends table { //NOSONAR  - Ignore naming convention for classes because the classes are named like the table they are describing

    protected function structure() : create {

        $table = new create("prefs");
        $table->ifNotExists();
        $table->addColumns(array(
            (new column("user_id"))->int()->notNull()->default(0),
            (new column("show_breadcrumbs"))->char(1)->notNull()->default("1"),
            (new column("num_breadcrumbs"))->smallint(5)->unsigned()->notNull()->default(8),
            (new column("num_rows"))->tinyint()->unsigned()->notNull()->default(3),
            (new column("num_cols"))->tinyint()->unsigned()->notNull()->default(4),
            (new column("max_pager_size"))->tinyint()->unsigned()->notNull()->default(10),
            (new column("random_photo_min_rating"))->tinyint()->unsigned()->notNull()->default(0),
            (new column("reports_top_n"))->smallint(5)->unsigned()->notNull()->default(5),
            (new column("color_scheme_id"))->int()->notNull()->default(1),
            (new column("slideshow_time"))->smallint(6)->notNull()->default(5),
            (new column("language"))->char(5)->default("NULL"),
            (new column("recent_photo_days"))->smallint(6)->notNull()->default(7),
            (new column("auto_edit"))->char(1)->notNull()->default("0"),
            (new column("camera_info"))->char(1)->notNull()->default("1"),
            (new column("allexif"))->char(1)->notNull()->default("0"),
            (new column("autocomp_albums"))->char(1)->default("1"),
            (new column("autocomp_places"))->char(1)->default("1"),
            (new column("autocomp_categories"))->char(1)->default("1"),
            (new column("autocomp_photographer"))->char(1)->default("1"),
            (new column("autocomp_people"))->char(1)->default("1"),
            (new column("fullsize_new_win"))->char(1)->notNull()->default("0"),
            (new column("view"))->enum("list", "tree", "thumbs")->default("list")->notNull(),
            (new column("autothumb"))->enum("oldest", "newest", "first", "last", "highest", "random")->default("highest")->notNull(),
            (new column("child_sortorder"))->enum("name", "sortname", "oldest", "newest", "first", "last", "lowest", "highest", "average", "random")->default("sortname")->notNull()
        ));

        return $table;
    }

    protected function data() : array {
        $return = array();
        $qry = new select("prefs");
        $qry->addFunction(array("count" => "COUNT(*)"));
        $qry->where(new clause("user_id=:userid"));
        $qry->addParam(new param(":userid", 1, PDO::PARAM_INT));

        if ($qry->getCount() == 0) {
            // Only inserting user_id, the rest of the fields is default

            $qry = new insert("prefs");
            $qry->addParam(new param(":user_id", 1, PDO::PARAM_INT));
            $return=array($qry);
        }
        return $return;
    }
}
