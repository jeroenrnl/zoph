<?php
/**
 * Controller for backup
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace backup;

use db\backup;
use generic\controller as genericController;
use user;
use web\request;

/**
 * Controller for backup
 */
class controller extends genericController {
    protected static $viewBackup   = view\backup::class;
    protected static $viewDisplay  = view\display::class;
    protected static $viewError    = view\error::class;
    protected static $viewRedirect = \web\view\redirect::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "backup"
    );

    public function doAction(string $action) : void {
        if (!user::getCurrent()->isAdmin()) {
            $this->view=new static::$viewRedirect($this->request);
            $this->view->setRedirect("zoph/welcome");
        } else {
            parent::doAction($action);
        }
    }


    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        $this->view=new static::$viewDisplay($this->request);
    }

    /**
     * Do action 'backup'
     * Create a backup
     */
    public function actionBackup() {
        try {
            $backup = new backup($this->request["rootpwd"] ?? null);
            $this->view=new static::$viewBackup($this->request, $backup->execute());
        } catch (\Exception $e) {
            $this->view=new static::$viewError($this->request, $e->getMessage());
        }
    }
}
