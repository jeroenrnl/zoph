<?php
/**
 * View for setting the database configuration check
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use app;
use conf\conf;
use template\block;
use template\form;
use web\request;

/**
 * Show a form to create the ini file
 */
class iniform extends view {
    /**
     * View for install
     * @return template\template
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"     => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $form=new form("form", array(
            "class"         => "dbsettings",
            "formAction"    => app::getBasePath() . "install/inifile",
            "submit"        => "next",
            "onsubmit"      => null
        ));

        $form->addInputHidden("token", $this->request["token"]);
        $form->addInputText("name", "zoph", translate("Name"),
            "Descriptive name, in case multiple Zoph installations are present on this system");
        $form->addInputText("db_host", "localhost", translate("Database Host"),
            "Server that hosts your database server");
        $form->addInputText("db_name", "zoph", translate("Database Name"));
        $form->addInputText("db_user", "zoph", translate("Database User"));
        $form->addInputPassword("db_pass", translate("Database Password"));
        $form->addInputText("db_prefix", "zoph_", translate("Database Prefix"),
            "To enable multiple Zoph installations in one database");

        $tpl->addBlock($form);

        return $tpl;
    }

}
