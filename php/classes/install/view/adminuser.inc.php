<?php
/**
 * View for setting the Zoph admin user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace install\view;

use app;
use conf\conf;
use template\block;
use template\form;
use template\template;
use web\request;

/**
 * Set Zoph admin password
 */
class adminuser extends view {
    protected array $javascripts = array(
        "window.addEventListener(\"load\", function() {
            zPass.init('admin_pass', 'admin_confirm', 'pwdmatcherror')
        })"
    );

    public array $scripts = array("js/password.js");

    /** @var string error to display instead of form */
    private $error;

    /**
     * View for install
     * @return template\template
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title" => $this->getTitle()
        ));
        $tpl->addBlock($this->getIcons());

        $tpl->addBlock(new block("text", array(
            "title"     => translate("Set admin password", false),
            "text"      => array("Please set a secure password for the user 'admin'"),
            "class"     => "install"
        )));

        $form=new form("form", array(
            "class"         => "dbuser",
            "formAction"    => app::getBasePath() . "install/config",
            "submit"        => "next",
            "onsubmit"      => null
        ));

        $form->addInputHidden("token", $this->request["token"]);
        $form->addInputPassword("admin_pass", translate("Admin Password"));
        $form->addInputPassword("admin_confirm", translate("Confirm"));
        $form->addBlock(new block("message", array(
            "id"            => "pwdmatcherror",
            "class"         => "error",
            "title"         => "Passwords do not match",
            "text"          => "Make sure password and confirm match"
        )));

        $tpl->addBlock($form);
        return $tpl;
    }

    public function setError(string $error) {
        $this->error = $error;
    }
}
