<?php
/**
 * This exception gets thrown when you try to add a page to a pageset it is already in.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package zophException
 */

namespace pageset;

/**
 * This exception gets thrown when you try to add a page to a pageset it is already in.
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class pageAlreadyInPagesetException extends Exception { }
