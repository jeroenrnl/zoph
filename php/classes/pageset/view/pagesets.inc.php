<?php
/**
 * View to display pagesets
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace pageset\view;

use web\view\viewInterface;
use pageset;
use template\actionlink;
use template\block;

/**
 * Display screen for pagesets
 */
class pagesets extends view implements viewInterface {
    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        return array(
            new actionlink("new", "pageset/new")
        );
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"         => $this->getTitle(),
        ));
        $tpl->addActionlinks($this->getActionlinks());
        $tpl->addBlock(pageset::getTable());
        return $tpl;
    }

    public function getTitle() : string {
        return translate("Pagesets", false);
    }
}
