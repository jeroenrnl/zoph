<?php
/**
 * image view - display images
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace image\view;

use web\view\viewInterface;
use Exception;
use conf\conf;
use file;
use photo;
use photo\watermarked;
use template\template;
use user;
use web\request;
use web\view\view as webView;

use photoNotFoundException;

/**
 * This is class takes care of displaying images
 */
class display extends webView implements viewInterface {
    /** @var string holds the image to display */
    protected string $file = "";
    /** @var array request variables */
    protected array $vars;

    /**
     * Create view
     * @param request web request
     * @param photo The photo to display
     */
    public function __construct(protected request $request, protected photo $object, private string $type) {
        if (empty($type)) {
            $this->type = "full";
        }
        $this->vars=$request->getRequestVars();

        $name = $this->object->get("name");
        $imagePath = conf::get("path.images") . "/" . $this->object->get("path") . "/";
        if ($this->type != "full") {
            $imagePath .= $this->type . "/" . $this->type . "_";
        }
        $imagePath .= $name;
        if (!file_exists($imagePath)) {
            throw new photoNotFoundException($name . " could not be found");
        } else {
            $this->file = $imagePath;
        }

        if ($this->object instanceof watermarked && $this->type == "full") {
            $this->object->createWatermark();
        }
    }

    public function display($template = null) : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }

        echo $this->view();
    }

    public function view() : string {
        if ($this->object instanceof watermarked) {
            $photo = $this->object->photo;
        }
        return $photo ?? file_get_contents($this->file);
    }

    /**
     * Get actionlinks
     */
    protected function getActionlinks() : ?array {
        return null;
    }

    /**
     * Get the title for this view
     * not usually displayed, because the views will output
     * an image, not an HTML page
     */
    public function getTitle() : string {
        return $this->object->getName();
    }

    public function getHeaders() : array {
        $headers = array();

        $file = $this->file;

        $mtime = filemtime($file);
        if ($this->object instanceof watermarked) {
            $filesize = $this->object->filesize;
        } else {
            $filesize = filesize($file);
        }
        $name = $this->object->get("name");

        $gmtMtime = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';


        $mod = $this->request->getServerVar("HTTP_IF_MODIFIED_SINCE") ?? "";
        if ($mod == $gmtMtime) {
            $headers[]="HTTP/1.1 304 Not Modified";
        } else {
            try {
                $f = new file($file);
                $imageType=$f->getMime();
            } catch (Exception $e) {
                throw new photoNotFoundException($name . " could not be found: " . $e->getMessage());
            }

            if ($imageType) {
                $headers[] = "Content-Length: " . $filesize;
                $headers[] = "Content-Disposition: inline; filename=" . $name;
                $headers[] = "Last-Modified: " . $gmtMtime;
                $headers[] = "Content-type: " . $imageType;
            }


        }
        return $headers;
    }
}
