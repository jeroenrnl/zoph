<?php
/**
 * Controller for circles
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace circle;

use addRemoveMembersUpdateAction;
use circle;
use generic\controller as genericController;
use organiser\actionsTrait;
use organiser\addRemoveMembersUpdateActionTrait;
use person;
use user;
use userInsufficientRightsSecurityException;
use web\request;

/**
 * Controller for circles
 */
class controller extends genericController {
    use actionsTrait;
    use addRemoveMembersUpdateActionTrait;

    protected static $memberClass   = person::class;

    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewUpdate    = view\update::class;

    /** @var array Actions that can be used in this controller */
    protected $actions = array("confirm", "delete", "display", "edit", "insert", "new", "update");

    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        $user = user::getCurrent();

        $circle = new circle($this->request["circle_id"]);
        $circle->lookup();

        /*
         * !circle->isVisible() and circle->isHidden() are not the same thing
         * see explanation in circle.php
         */
        if (!$circle->isVisible() && $action != "new") {
            $this->view = new static::$viewForbidden($this->request, new userInsufficientRightsSecurityException("user cannot see circle"));
        }

        if ($circle->isHidden() && !$user->canSeeHiddenCircles()) {
            $this->view = new static::$viewForbidden($this->request, new userInsufficientRightsSecurityException("circle is hidden"));
        }

        $this->setObject($circle);
        if (!$user->canEditOrganisers()) {
            $this->actionDisplay();
        } else {
            parent::doAction($action);
        }
    }

    protected function actionConfirm() : void {
        parent::actionConfirm();
        $this->view->setRedirect("person/people");
    }

    protected function actionInsert() : void {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

}
