<?php
/**
 * Marker to be displayed on map
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace geo;

use template\block;

/**
 * Class to display markers on a map
 * @author Jeroen Roos
 * @package Zoph
 */
class marker {

   /**
    * Create a new marker object
    */
    public function __construct(private float $lat, private float $lon, private string $icon, private string $title, private block $quicklook) {
    }

    public function __toString() {

        $find = array("\'", "\"", "\n");
        $replace = array("\\'", "\\\"", "");

        $title = str_replace($find, $replace, empty($this->title) ? "" : $this->title);
        $quicklook = str_replace($find, $replace, $this->quicklook);
        return "zMaps.createMarker(" . $this->lat . "," . $this->lon . ", icons[\"" . $this->icon . "\"], \"" . $title . "\", \"" . $quicklook . "\");\n";
    }

    /**
     * Get marker from object
     * @param photo|place Object to get marker from
     * @param string Icon to use
     * @return marker created marker.
     */
    public static function getFromObj(mapable $obj, $icon) {
        $lat=$obj->get("lat");
        $lon=$obj->get("lon");
        if ($lat && $lon) {
            $title=$obj->get("title");
            $quicklook=$obj->getQuicklook();
            return new self($lat, $lon, $icon, $title, $quicklook);
        } else {
            return null;
        }
    }
}
