<?php
/**
 * View for display track
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace geo\view;

use web\view\viewInterface;
use conf\conf;
use geo\map;
use geo\track;
use template\block;
use template\template;
use photo\collection;
use user;
use web\request;

/**
 * This view displays a track
 */
class dogeotag extends view implements viewInterface {

    public string   $url;
    public bool     $isTest;
    public int      $taggedCount;
    public int      $totalCount;
    public ?map     $map;

    public function __construct(protected request $request) {
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
            "map"               => $this->map
        ));

        $tpl->addBlock(new block("tracks_geotag_results", array(
            "actionlinks"   => $this->getActionlinks(),
            "test"          => $this->isTest,
            "tagged_count"  => $this->taggedCount,
            "total_count"   => $this->totalCount
        )));
        return $tpl;
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array(
            translate("geotag") => $this->url
        );
    }
    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("Geotag");
    }
}
