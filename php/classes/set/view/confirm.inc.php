<?php
/**
 * View for confirm delete set
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace set\view;

use photo;
use set\model as set;
use template\actionlink;
use template\block;
use user;
use web\request;

/**
 * This view displays the "confirm delete set" page
 */
class confirm extends view {


    /**
     * Get actionlinks
     */
    public function getActionlinks() : array {
        $param = array("set_id" => (int) $this->object->getId());
        return array(
            new actionlink("confirm", "set/confirm", $param),
            new actionlink("cancel", "set", $param)
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"     =>  translate("delete set"),
            "actionlinks"   => $this->getActionlinks(),
            "mainActionlinks" => null,
            "obj"           => $this->object,
        ));

    }
}
