<?php
/**
 * Class autoloader
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

class autoloader {
    private static array $paths = array(
        "."
    );

    /**
     * Autoload file
     * @param string filename to load
     */
    public function loadClass(string $class) : void {
        foreach (self::$paths as $path) {
            $file=$path . "/classes/" . str_replace("\\", "/", $class) . ".inc.php";
            if (file_exists($file)) {
                require_once $file;
            }
        }
    }

    public function register() : void {
        spl_autoload_register(array($this, "loadClass"));

    }

    public static function addPath(string $path) {
        self::$paths[] = $path;
    }

}
