<?php
/**
 * View to display groups
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use web\view\viewInterface;
use conf\conf;
use group;
use template\actionlink;
use template\block;
use web\request;

/**
 * Display screen for groups
 */
class groups extends view implements viewInterface {

    private $objects = array();

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        return array(
            new actionlink("new", "group/new")
        );
    }

    public function setObjects(array $objects) : void {
        $this->objects = $objects;
    }

    /**
     * Get view
     * @return template\block view
     */
    public function view() : block {
        return new block("displayGroups", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "groups"        => $this->objects,
        ));
    }

    public function getTitle() : string {
        return translate("Groups", false);
    }
}
