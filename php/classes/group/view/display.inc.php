<?php
/**
 * View to display group
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use web\view\viewInterface;
use conf\conf;
use group;
use template\actionlink;
use template\block;
use web\request;

/**
 * Display screen for group
 */
class display extends view implements viewInterface {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $param = array(
            "group_id" => (int) $this->object->getId()
        );
        return array(
            new actionlink("edit", "group/edit", $param),
            new actionlink("delete", "group/delete", $param),
            new actionlink("new", "group/new"),
            new actionlink("return", "group/groups")
        );
    }

    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        return new block("displayGroup", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "obj"           => $this->object,
            "view"          => "album",
            "fields"        => $this->object->getDisplayArray(),
            "watermark"     => conf::get("watermark.enable"),
            "permissions"   => $this->object->getPermissionArray()
        ));
    }
}
