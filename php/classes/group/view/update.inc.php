<?php
/**
 * View to update group
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace group\view;

use web\view\viewInterface;
use conf\conf;
use group;
use permissions\view\edit as editPermissions;
use template\actionlink;
use template\block;
use template\form;
use web\request;
use zoph\app;

/**
 * Update screen for group
 */
class update extends view implements viewInterface {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        if ($this->request->getAction() == "new") {
            return array(
                new actionlink("return", "group/groups")
            );
        } else {
            $param = array(
                "group_id" => (int) $this->object->getId()
            );
            return array(
                new actionlink("delete", "group/delete", $param),
                new actionlink("new", "group/new"),
                new actionlink("return", "group", $param)
            );
        }
    }


    /**
     * Get view
     * @return block view
     */
    public function view() : block {
        $action = $this->request->getAction() == "new" ? "insert" : "update";

        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $form=new form("form", array(
            "formAction"        => app::getBasePath() . "group/" . $action,
            "onsubmit"          => null,
            "submit"            => translate("submit")
        ));

        $form->addInputHidden("group_id", $this->object->getId());

        $form->addInputText("group_name", $this->object->getName(), translate("group name"),
            sprintf(translate("%s chars max"), 32), 32);

        $form->addInputText("description", $this->object->get("description"),
            translate("description"), sprintf(translate("%s chars max"), 128), 128, 32);

        if ($this->request->getAction() != "new") {
            $curMembers=$this->object->getMembers();
            $members=new block("formFieldsetAddRemove", array(
                "legend"    => translate("members"),
                "objects"   => $curMembers,
                "dropdown"  => $this->object->getNewMemberDropdown("_member")
            ));
            $form->addBlock($members);

            $tpl->addBlock($form);

            $view=new editPermissions($this->object);
            $tpl->addBlock($view->view());

        } else {
            $tpl->addBlock(new block("message", array(
                "class" => "info",
                "text" => translate("After this group is created it can be given access to albums.")
            )));
            $tpl->addBlock($form);
        }

        return $tpl;
    }

    public function getTitle() : string {
        if ($this->request->getAction() == "new") {
            return translate("New group");
        } else {
            return parent::getTitle();
        }

    }
}
