<?php
/**
 * View to display categories
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use web\view\viewInterface;
use conf\conf;
use category;
use organiser\viewTrait as organiserView;
use pageException;
use photoNoSelectionException;
use selection;
use template\actionlink;
use template\block;
use user;
use web\request;

/**
 * Display screen for categoriess
 */
class display extends view implements viewInterface {
    use organiserView;

    private const VIEWNAME = "Category View";
    private const PHOTOLINK = "photos?category_id=";
    private const DESCRIPTION = "category_description";

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();
        $actionlinks=array();

        $param = array("category_id" => (int) $this->object->getId());
        $parentParam = array("parent_category_id" => (int) $this->object->getId());

        if ($user->canEditOrganisers()) {
            $actionlinks=array(
                new actionlink("edit", "category/edit", $param),
                new actionlink("new", "category/new", $parentParam),
                new actionlink("delete", "category/delete", $param)
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks[]=new actionlink("unset converphoto", "category/unset coverphoto", $param);
            }
        }
        return $actionlinks;
    }

    private function getSelection() : ?selection {
        $categoryId = array(
            "category_id" => $this->object->getId()
        );

        try {
            $selection=new selection(
                array(
                    new actionlink("coverphoto", "category/coverphoto", $categoryId)
                ),
                "category?_qs=category_id=" . $this->object->getId(),
                field: "coverphoto"
            );
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }

        return $selection;
    }

    public function getTitle() : string {
        return $this->object->get("parent_category_id") ? $this->object->get("category") : translate("Categories");
    }
}
