<?php
/**
 * View for edit category page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace category\view;

use category;
use conf\conf;
use pageset;
use permissions\view\edit as editPermissions;
use template\actionlink;
use template\template;
use template\block;
use user;
use web\request;


/**
 * This view displays the category page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        if ((int) $this->request["category_id"] != 0) {
            $returnParam = array("category_id" => (int) $this->request["category_id"]);
        } else if ($this->object->getId() != 0) {
            $returnParam = array("category_id" => (int) $this->object->getId());
        } else if ($this->request["parent_category_id"] != 0) {
            $returnParam = array("category_id" => (int) $this->request["parent_category_id"]);
        } else {
            $returnParam = array();
        }

        $actionlinks = array(
            new actionlink("return", "category", $returnParam)
        );


        $param = array("category_id" => (int) $this->object->getId());
        $parentParam = array("parent_category_id" => (int) $this->object->getId());

        if ($this->request->getAction() != "new") {
            $actionlinks[] = new actionlink("new", "category/new", $parentParam);
            $actionlinks[] = new actionlink("delete", "category/delete", $param);
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        $user = user::getCurrent();

        $actionlinks=$this->getActionlinks();

        if ($this->request->getAction() == "new") {
            $action = "insert";
        } else if (in_array($this->request->getAction(), array("edit", "update"))) {
            $action = "update";
        } else {
            // Safety net. This should not happen.
            $action = "display";
        }

        return new block("editCategory", array(
            "actionlinks"   => $actionlinks,
            "action"        => $action,
            "category"      => $this->object,
            "title"         => $this->getTitle(),
            "pagesets"      => template::createSelectArray(pageset::getAll("title"), array("title"), true),
            "user"          => $user,
        ));
    }

}
