<?php
/**
 * Display and modify breadcrumbs
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author Jeroen Roos
 */

use template\block;
use web\request;
use web\session;

class breadcrumb {

    /** @var array Current breadcrumbs */
    private static $crumbs=array();

    private static $actions=array(
        "",
        "comments",
        "compose",
        "display",
        "groups",
        "new",
        "notify",
        "pages",
        "pagesets",
        "people",
        "search",
        "sets",
        "users",
        "welcome"
    );

    private static request $request;

    /**
     * Create a crumb
     * @param string title
     * @param string url
     */
    public function __construct(private string $title, private string $url) {
    }


    /**
     * Create a crumb
     * Crumbs are the path a user followed through Zoph's web GUI and can be
     * used to easily go back to an earlier visited page
     * only add a crumb if a title was set and if there is either no
     * action or a safe action ("edit", "delete", etc would be unsafe)
     * @param string title
     * @param string action (display, edit, delete, etc.)
     */
    public static function create($title, request $request) : void {
        $action = $request->getAction() ?? "";
        $url=htmlentities($request->getServerVar("REQUEST_URI"));
        $allowed = in_array($action, self::$actions, true);
        $numCrumbs = count(static::$crumbs);

        if (isset($title) && $numCrumbs < 100 && $allowed && ($numCrumbs == 0 || (!strpos($url, "_crumb=")))) {
            // if title is the same remove last and add new
            if ($numCrumbs > 0 && static::getLast()->getTitle()==$title) {
                static::eat();
            } else {
                $numCrumbs++;
            }

            self::add(new self($title, self::updateURL($url, $numCrumbs)));
        }
    }

    private static function updateURL(string $url, int $num) {
        $question = strpos($url, "?");
        if ($question > 0) {
            $url =
                substr($url, 0, $question) . "?_crumb=$num&amp;" .
                substr($url, $question + 1);
        } else {
            $url .= "?_crumb=$num";
        }
        return $url;
    }

    public static function add(breadcrumb $crumb) {
        static::$crumbs[] = $crumb;
        session::getCurrent()["crumbs"]=static::$crumbs;
    }

    /**
     * Get the title of the breadcrumb
     * @return string title
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Get the URL of the breadcrumb
     * @return string url
     */
    public function getURL() {
        return $this->url;
    }

    /**
     * This function reads the crumbs from the session, and makes sure it is updated
     */
    public static function init(request $request) {
        static::$request = $request;
        $session = session::getCurrent();
        if (isset($session["crumbs"])) {
            static::$crumbs=$session["crumbs"];
        }
        $session["crumbs"]=static::$crumbs;
    }


    /**
     * construct the link for clearing the crumbs (the 'x' on the right)
     */
    public static function getClearURL() {
        if ($_POST) {
            $clearUrl=static::$request->getServerVar("PHP_SELF") . "?" . static::$request["_qs"];
        } else {
            $clearUrl = htmlentities(static::$request->getServerVar("REQUEST_URI"));
        }

        if (strpos($clearUrl, "clear_crumbs") == 0) {
            if (strpos($clearUrl, "?") > 0) {
                $clearUrl .= "&amp;";
            } else {
                $clearUrl .= "?";
            }

            $clearUrl .= "_clear_crumbs=1";
        }
        return $clearUrl;
    }

    /**
     * Eat a crumb
     * A crumb is 'eaten' when a user clicks on the link
     * it means that the crumbs at the end are removed up to the place
     * where the user went back to
     * @param int number of crumbs up to which to eat
     */
    public static function eat($num = -1) {
        if (count(static::$crumbs) > 0) {
            if ($num < 0) {
                $num = count(static::$crumbs) - 1;
            }
            static::$crumbs = array_slice(static::$crumbs, 0, $num);
            session::getCurrent()["crumbs"]=static::$crumbs;
        }
    }

    /**
     * Get the last crumb
     */
    public static function getLast() {
        if (count(static::$crumbs) > 0) {
            return end(static::$crumbs);
        }
    }

    public static function display() {
        $user=user::getCurrent();
        $maxCrumbs=(int) ($user?->prefs->get("num_breadcrumbs") ?? 10);
        if (($numCrumbs = count(static::$crumbs)) > $maxCrumbs) {
            $crumbs=array_slice(static::$crumbs, $numCrumbs - $maxCrumbs);
            $class="firstdots";
        } else {
            $crumbs=static::$crumbs;
            $class="";
        }

        return new block("breadcrumbs", array(
            "crumbs"    =>  $crumbs,
            "class"     =>  $class,
            "clearURL"  =>  static::getClearURL()
        ));
    }

}
?>
