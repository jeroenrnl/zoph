<?php
/**
 * View for edit place page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use TimeZone;
use conf\conf;
use geo\map;
use geo\marker;
use pageset;
use place;
use template\actionlink;
use template\block;
use template\fieldset;
use template\form;
use template\template;
use user;
use web\request;
use zoph\app;

/**
 * This view displays the place page when editing
 */
class update extends view {
    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {


        if ((int) $this->request["place_id"] != 0) {
            $returnParam = array("place_id" => (int) $this->request["place_id"]);
        } else if ($this->object->getId() != 0) {
            $returnParam = array("place_id" => (int) $this->object->getId());
        } else if ($this->request["parent_place_id"] != 0) {
            $returnParam = array("place_id" => (int) $this->request["parent_place_id"]);
        } else {
            $returnParam = array();
        }
        $actionlinks = array(
            new actionlink("return", "place", $returnParam)
        );

        $param = array("place_id" => (int) $this->object->getId());
        $parentParam = array("parent_place_id" => (int) $this->object->getId());

        if ($this->request->getAction() != "new") {
            $actionlinks[] = new actionlink("new", "place/new", $parentParam);
            $actionlinks[] = new actionlink("delete", "place/delete", $param);
        }
        return $actionlinks;
    }

    /**
     * Output the view
     */
    public function view() : block {
        $action = $this->request->getAction();

        if (conf::get("maps.provider")) {
            $map = $this->getMap();
        }

        $tpl = new block("main", array(
            "title"         => $this->getTitle(),
            "map"           => $map ?? null
        ));

        $tpl->addActionlinks($this->getActionlinks());

        $tpl->addBlock(template::showJSwarning());

        $formaction = $action == "new" ? "insert" : "update";

        $form=new form("form", array(
            "formAction"    => app::getBasePath() . "place/" . $formaction,
            "onsubmit"      => null,
            "submit"        => translate("submit")
        ));

        $form->addInputHidden("place_id", $this->object->getId());
        $form->addInputText("title", $this->object->get("title"), translate("title"),
            sprintf(translate("%s chars max"), "64"), 64, 40);

        if (!$this->object->isRoot()) {
            $parentPlace=place::createDropdown("parent_place_id", $this->object->get("parent_place_id"));
            $form->addDropdown("parent_place_id", $parentPlace, translate("parent location"));
        }

        $form->addInputText("address", $this->object->get("address"), translate("address"),
            sprintf(translate("%s chars max"), "64"), 64, 40);
        $form->addInputText("address2", $this->object->get("address2"), translate("address continued"),
            sprintf(translate("%s chars max"), "64"), 64, 40);
        $form->addInputText("city", $this->object->get("city"), translate("city"),
            sprintf(translate("%s chars max"), "32"), 32);
        $form->addInputText("state", $this->object->get("state"), translate("state"),
            sprintf(translate("%s chars max"), "32"), 32, 16);
        $form->addInputText("zip", $this->object->get("zip"), translate("zip"),
            translate("zip or zip+4"), 10);
        $form->addInputText("country", $this->object->get("country"), translate("country"),
            sprintf(translate("%s chars max"), "32"), 32);
        $form->addInputText("url", $this->object->get("url"), translate("url"),
            sprintf(translate("%s chars max"), "1024"), 1024, 32);
        $form->addInputText("urldesc", $this->object->get("urldesc"), translate("url description"),
            sprintf(translate("%s chars max"), "32"), 32);

        $pageset=template::createDropdown("pageset", $this->object->get("pageset"),
            template::createSelectArray(pageset::getAll("title"), array("title"), true));
        $form->addDropdown("pageset", $pageset, translate("pageset"));

        $fieldset=new fieldset("formFieldset", array(
            "class"     => "map",
            "legend"    => translate("map")
        ));

        $fieldset->addInputText("lat", $this->object->get("lat"), translate("latitude"), null, 10);
        $fieldset->addInputText("lon", $this->object->get("lon"), translate("longitude"), null, 10);
        $mapzoom=place::createZoomDropdown($this->object->get("mapzoom"));
        $fieldset->addDropdown("mapzoom", $mapzoom, translate("zoom level"));
        $desc = translate("Paste a location in this field to lookup, supported are currently: decimal GPS coordinates (e.g. 50.5,-5.2), Open Location Codes (also known as pluscodes e.g. 7GXHX4HM+MM), Openstreetmap URLs and Zoph URLs for a photo or place.");
        $fieldset->addInputText("_locationLookup", "", translate("location lookup"), $desc, 256, 40);

        if (conf::get("maps.geocode")) {
            $fieldset->addBlock(new block("geocode"));
        }

        $form->addBlock($fieldset);

        $tzActionlinks=array();
        if (conf::get("date.guesstz")) {
            $tz=e($this->object->guessTZ());
            if (!empty($tz)) {
                $param = array(
                    "place_id" => (int) $this->object->getId(),
                    "timezone" => $tz
                );
                $tzActionlinks[] = new actionlink($tz, "place/update", $param);
            }
        }

        if ($this->object->get("timezone")) {
            $tzActionlinks[] = new actionlink(translate("set %s for children"), "place/settzchildren", array(
                "place_id" => $this->object->getId()
            ), titleParams: array($this->object->get("timezone")));
        }

        if (!empty($tzActionlinks)) {
            $form->addBlock(new block("actionlinks", array(
                "actionlinks" => $tzActionlinks
            )));
        }

        $timezone=TimeZone::createDropdown("timezone_id", $this->object->get("timezone"));
        $form->addDropdown("timezone_id", $timezone, translate("timezone"));

        $form->addTextarea("notes", $this->object->get("notes"), translate("notes"), 40, 4);

        $tpl->addBlock($form);



        return $tpl;
    }

    private function getMap() : map {
        $map=new map();
        $marker=$this->object->getMarker();
        $map->setCenterAndZoomFromObj($this->object);
        if ($marker instanceof marker) {
            $map->addMarker($marker);
        }
        $map->setEditable();
        return $map;
    }
}
