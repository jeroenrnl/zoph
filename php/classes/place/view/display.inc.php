<?php
/**
 * View to display places
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place\view;

use web\view\viewInterface;
use conf\conf;
use geo\map;
use geo\marker;
use organiser\viewTrait as organiserView;
use pageException;
use photoNoSelectionException;
use place;
use selection;
use template\actionlink;
use template\block;
use user;
use web\request;
use zoph\app;

/**
 * Display screen for placess
 */
class display extends view implements viewInterface {
    use organiserView;

    private const VIEWNAME = "Place View";
    private const PHOTOLINK = "photos?location_id=";
    private const DESCRIPTION = "place_description";

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user = user::getCurrent();
        $actionlinks=array();

        $param = array(
            "place_id"  => (int) $this->object->getId()
        );

        $parentParam = array(
            "parent_place_id"  => (int) $this->object->getId()
        );

        if ($user->canEditOrganisers()) {
            $actionlinks=array(
                new actionlink("edit", "place/edit", $param),
                new actionlink("new", "place/new", $parentParam),
                new actionlink("delete", "place/delete", $param)
            );
            if ($this->object->get("coverphoto")) {
                $actionlinks[] = new actionlink("unset coverphoto", "place/unsetcoverphoto", $param);
            }
        }
        return $actionlinks;
    }

    private function getSelection() : ?selection {
        $placeId = array(
            "place_id" => $this->object->getId()
        );

        try {
            $selection=new selection(
                array(new actionlink("coverphoto", "place/coverphoto", $placeId)),
                "place?_qs=place_id=" . $this->object->getId(),
                field: "coverphoto"
            );

        } catch (photoNoSelectionException $e) {
            $selection=null;
        }

        return $selection;
    }

    public function getTitle() : string {
        return $this->object->get("parent_place_id") ? $this->object->get("title") : translate("Places");
    }

    protected function getMap() : ?map {
        if (conf::get("maps.provider")) {
            $map=new map();
            $map->setCenterAndZoomFromObj($this->object);
            $marker=$this->object->getMarker();
            if ($marker instanceof marker) {
                $map->addMarker($marker);
            }
            $map->addMarkers($this->object->getChildren());
            return $map;
        }
        return null;
    }
}
