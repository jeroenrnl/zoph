<?php
/**
 * Controller for place
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace place;

use breadcrumb;
use conf\conf;
use generic\controller as genericController;
use log;
use place;
use rating;
use user;
use web\request;

use placeNotAccessibleSecurityException;

/**
 * Controller for place
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewNotfound  = view\notfound::class;
    protected static $viewUpdate    = view\update::class;


    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "new", "insert", "confirm", "delete", "display", "edit", "update", "coverphoto", "unsetcoverphoto", "settzchildren"
    );

    public function doAction(string $action) : void {
        if ($action == "new") {
            $place=new place();
            if (isset($this->request["parent_place_id"])) {
                $place->set("parent_place_id", $this->request["parent_place_id"]);
            }
            $this->setObject($place);
            parent::doAction($action);
        } else {
            try {
                $place=$this->getPlaceFromRequest();
            } catch (placeNotAccessibleSecurityException $e) {
                log::msg($e->getMessage(), log::WARN, log::SECURITY);
                $place=null;
            }
            if ($place instanceof place) {
                $this->setObject($place);
                parent::doAction($action);
            } else {
                $this->view = new static::$viewNotfound($this->request);
            }
        }
    }

    /**
     * Get the place based on the query in the request
     * @throws placeNotAccessibleSecurityException
     */
    private function getPlaceFromRequest() {
        $user=user::getCurrent();
        if (isset($this->request["place_id"])) {
            $place = new place($this->request["place_id"]);
        } else {
            $place=place::getRoot();
        }
        $place->lookup();

        if ($user->isAdmin() || $place->isVisible($user)) {
            return $place;
        }
        throw new placeNotAccessibleSecurityException(
            "Security Exception: place " . $place->getId() .
            " is not accessible for user " . $user->getName() . " (" . $user->getId() . ")"
        );
    }

    /**
     * Do action 'confirm'
     */
    public function actionConfirm() {
        if (user::getCurrent()->canDeletePhotos()) {
            $this->object->lookup();
            $parentId = (int) $this->object->get("parent_place_id");
            parent::actionConfirm();
            $this->view = new static::$viewRedirect($this->request, $this->object);
            $this->view->setRedirect("place?place_id=" . $parentId);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'delete'
     */
    public function actionDelete() {
        if (user::getCurrent()->canDeletePhotos()) {
            parent::actionDelete();
            $this->view = new static::$viewConfirm($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'edit'
     */
    public function actionEdit() {
        if (user::getCurrent()->canEditOrganisers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'new'
     */
    public function actionInsert() {
        parent::actionInsert();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }

    /**
     * Do action 'new'
     */
    public function actionNew() {
        $user = user::getCurrent();
        if ($user->canEditOrganisers()) {
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->setFields($this->request->getRequestVars());
            $this->object->update();
            $this->view = new static::$viewUpdate($this->request, $this->object);
        } else {
            $this->actionDisplay();
        }
    }

    public function actionCoverphoto() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user) && isset($this->request["coverphoto"])) {
            $this->object->set("coverphoto", (int) $this->request["coverphoto"]);
            $this->object->update();
        }
        if (isset($this->request["_return"])) {
            $this->view = new static::$viewRedirect($this->request, $this->object);
            $this->view->setRedirect($this->request["_return"]);
        } else {
            $this->actionDisplay();
        }
    }

    public function actionUnsetcoverphoto() {
        $user=user::getCurrent();
        if ($this->object->isWritableBy($user)) {
            $this->object->set("coverphoto", null);
            $this->object->update();
        }
        if (isset($this->request["_return"])) {
            $this->view = new static::$viewRedirect($this->request, $this->object);
            $this->view->setRedirect($this->request["_return"]);
        } else {
            $this->actionDisplay();
        }
    }

    public function actionSettzchildren() {
        $user=user::getCurrent();
        if ($user->canEditOrganisers()) {
            $this->object->lookup();
            $this->object->setTzForChildren();
        }
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }
}
