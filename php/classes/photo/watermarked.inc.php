<?php
/**
 * A class representing a watermarked photo
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo;

use file;
use photo;
use conf\conf;

/**
 * A class representing a watermarked photo
 * This is a photo with a "watermark" superimposed over it
 * usually to prevent unauthorized use the photo
 *
 * @package Zoph
 * @author Jeroen Roos
 */
class watermarked extends photo {
    public int $filesize = 0;
    public string $photo = "";

    /**
     * Display the watermarked image
     * @param string type of image to display mid, thumb or null for full-sized
     * @return array Return an array that contains:
     *               array headers: the headers
     *               string jpeg: the jpeg file
     */
    public function createWatermark() : void {
        $watermarkFile = conf::get("path.images") . "/" . conf::get("watermark.file");
        if (file_exists($watermarkFile)) {
            $name = $this->get("name");
            $imagePath = conf::get("path.images") . "/" . $this->get("path") . "/" . $name;
            $image=imagecreatefromjpeg($imagePath);
            $image=$this->watermark($image, $watermarkFile, conf::get("watermark.pos.x"),
                conf::get("watermark.pos.y"), conf::get("watermark.transparency"));
            ob_start();
                imagejpeg($image);
                imagedestroy($image);
            $this->photo=ob_get_clean();
            $this->filesize=strlen($this->photo);
        } else {
            echo "Watermark not found";
        }
    }

    /**
     * Watermark the photo
     * @param imageresource photo
     * @param string GIF image to be used as watermark
     * @param string position horizontally (center, left or right)
     * @param string position vertically (center, top or bottom)
     * @param int transparency (0 = invisible, 100 = no transparency)
     * @return imageresource watermarked photo
     */
    private function watermark($orig, $watermark, $positionX = "center", $positionY = "center", $transparency = 50) {

        $wmFile = new file($watermark);

        $wm  = match($wmFile->getMime()) {
            "image/png" => imagecreatefrompng($watermark),
            "image/gif" => imagecreatefromgif($watermark)
        };

        $widthOrig=ImageSX($orig);
        $heightOrig=ImageSY($orig);

        $widthWm=ImageSX($wm);
        $heightWm=ImageSY($wm);

        switch ($positionX) {
        case "left":
            $destX = 5;
            break;
        case "right":
            $destX = $widthOrig - $widthWm - 5;
            break;
        default:
            $destX = ($widthOrig / 2) - ($widthWm / 2);
            break;
        }

        switch ($positionY) {
        case "top":
            $destY = 5;
            break;
        case "bottom":
            $destY = $heightOrig - $heightWm - 5;
            break;
        default:
            $destY = ($heightOrig / 2) - ($heightWm / 2);
            break;
        }
        ImageCopyMerge($orig, $wm, $destX, $destY, 0, 0, $widthWm, $heightWm, $transparency);
        imagedestroy($wm);
        return $orig;
    }
}
?>
