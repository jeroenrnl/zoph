<?php
/**
 * View for 'photo not found' page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use web\request;
use template\actionlink;
use template\block;

/**
 * This view displays a "not found" error in case no photo was found
 */
class notfound extends view {

    /**
     * Create view
     * @param request web request
     * @param photo the photo that this page is dealing with
     */
    public function __construct(protected request $request) {
        $this->vars=$request->getRequestVars();
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("notFound", array(
            "title"         => translate("Photo"),
            "msg"           => translate("No photo was found"),
            "actionlinks"   => $this->getActionlinks()
        ));
    }

    public function getActionlinks() : array {
        return array(
            new actionlink("return", "photos")
        );
    }
}
