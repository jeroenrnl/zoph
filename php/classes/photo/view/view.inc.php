<?php
/**
 * Common parts for photo view
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\view;

use web\view\viewInterface;
use album;
use conf\conf;
use photo;
use selection;
use template\actionlink;
use template\block;
use user;
use web\request;
use web\url;
use web\view\view as webView;

use photoNoSelectionException;

/**
 * This is a base view for the photo page, that contains common parts for all views
 */
abstract class view extends webView implements viewInterface {

    /** * @var array request variables */
    protected $vars;

    /** @var int total number of photos return by the query */
    protected $photocount = 0;
    /** @var int offset in the query for the current photo */
    protected $offset = 0;

    /** @var string Contains url of next image */
    protected $nextURL = "";
    /** @var string Contains url of prev image */
    protected $prevURL = "";
    /** @var string Contains url for 'up' link  */
    protected $upURL = "";

    protected $title = "photo";

    /**
     * Create view
     * @param request web request
     * @param photo the photo that this page is dealing with
     */
    public function __construct(protected request $request, protected photo $photo) {
        $this->vars=$request->getRequestVars();
    }

    /**
     * Create "next" "previous" and "up" links for the photo page
     */
    public function setLinks() : void {
        if (isset($this->request["_off"])) {
            $user=user::getCurrent();
            $ignore=array("_off");

            $cols = (int) $this->request["_cols"];
            $rows = (int) $this->request["_rows"];
            $offset  = (int) $this->request["_off"];

            $cols = $cols ? $cols : $user->prefs->get("num_cols");
            $rows = $rows ? $rows : $user->prefs->get("num_rows");

            $cells = $cols * $rows;

            $upQs = $this->request->getUpdatedVars(null, null, $ignore);

            if ($cells) {
                $off = $cells * floor($offset / ($cells));
                $upQs["_off"] = $off;
            }
            $this->upURL=$this->request->getBasePath() . "photos?" . http_build_query($upQs);

            $url = $this->request->getBasePath() . "photo";

            if ($this->request->getAction() == "edit") {
                $url .= "/edit?";
            } else {
                $url .= "?";
            }

            if ($offset > 0) {
                $newoffset = $offset - 1;
                $this->prevURL= $url .
                    htmlentities(
                        str_replace("_off=$offset", "_off=$newoffset", $this->request->getReturnQueryString())
                    );
            }
            if ($offset + 1 < $this->photocount) {
                $newoffset = $offset + 1;
                $this->nextURL = $url .
                    htmlentities(
                        str_replace("_off=$offset", "_off=$newoffset", $this->request->getReturnQueryString())
                    );
            }

        }
    }


    /**
     * Get selection block for current photo
     * @return selection
     */
    protected function getSelection() : ?selection {
        try {
            $photoId = array(
                "photo_id_2" => $this->photo->getId(),
            );
            $selection=new selection(
                array(new actionlink("relate", "relation/new", $photoId)),
                "photo?_qs=" . $this->request->getEncodedQueryString(),
                $this->photo,
                field: "photo_id_1"
            );
        } catch (photoNoSelectionException $e) {
            $selection=null;
        }
        return $selection;
    }

    /**
     * Get 'share' block for current photo
     * @return block template block
     */
    protected function getShare() : ?block {
        if (conf::get("share.enable") && (user::getCurrent()->isAdmin() || user::getCurrent()->get("allow_share"))) {
            $hash=$this->photo->getHash();
            $fullHash=sha1(conf::get("share.salt.full") . $hash);
            $midHash=sha1(conf::get("share.salt.mid") . $hash);
            $fullLink=url::get() . "image?hash=" . $fullHash;
            $midLink=url::get() . "image?hash=" . $midHash;

            $share=new block("photoShare", array(
                "hash" => $hash,
                "full_link" => $fullLink,
                "mid_link" => $midLink
            ));
        }
        return $share ?? null;
    }

    public function getTitle() : string {
        return $this->title;
    }

    public function setOffset(int $offset, int $count) : void {
        $this->offset = $offset;
        $this->photocount = $count;

        if ($this->photo instanceof photo && !empty($this->photo->get("name"))) {
            $this->title = $this->photo->get("name");
        }

        if ($this->photocount > 0) {
            if (!empty($this->title)) {
                $this->title .= " - ";
            }
            $this->title .= sprintf(translate("photo %s of %s"), ($offset + 1), $this->photocount);
        }

        if (empty($this->title)) {
            $this->title = translate("Photo");
        }
    }


    /**
     * Create the actionlinks for this page
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();
        $actionlinks=array();

        $photoId = array("photo_id" => $this->photo->getId());

        if (conf::get("feature.mail")) {
            $actionlinks[] = new actionlink("email", "mail/compose", $photoId);
        }

        $writable = $user->getPhotoPermissions($this->photo)->get("writable");
        if ($writable) {
            $url = "photo/edit";
            $qs = $this->request->getReturnQueryString();
            if (!empty($qs)) {
                $url .= "?" . $qs;
            }
            $actionlinks["edit"] = new actionlink("edit", $url);
        }

        if ($user->canDeletePhotos() && $writable) {
            $actionlinks[] = new actionlink("delete", "photo/delete", array_merge($photoId, array(
                "_qs" => $this->request->getEncodedQueryString())));
        }

        if ($user->get("lightbox_id")) {
            $lightbox = new album($user->get("lightbox_id"));
            $lbPhotos = array_map(
                function ($ph) {
                    return $ph->getId();
                }, $lightbox->getPhotos()
            );

            if (array_search($this->photo->getId(), $lbPhotos) === false) {
                $action = "lightbox";
            } else {
                $action = "unlightbox";
            }

            $actionlinks[] = new actionlink($action, "photo/" . $action, array(
                "photo_id"  => $this->photo->getId(),
                "_return"   => "photo",
                "_qs"       => $this->request->getEncodedQueryString()
            ));
        }

        if (conf::get("feature.comments") && $user->canLeaveComments()) {
            $actionlinks[] = new actionlink("add comment", "comment/new", $photoId);
        }

        if ($user->isAdmin()) {
            $url = "photo/select";
            $qs = $this->request->getReturnQueryString();
            if (!empty($qs)) {
                $url .= "?" . $qs;
            }
            $actionlinks[] = new actionlink("select", $url);
        }
        return $actionlinks;
    }

    public function getScripts() : array {
        $scripts = parent::getScripts();
        $scripts[]="js/photoPeople.js";
        $scripts[]="js/locationLookup.js";
        $scripts[]="js/rating.js";
        $scripts[]="js/json.js";

        return $scripts;
    }

}
