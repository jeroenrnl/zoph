<?php
/**
 * Controller for photo\relation
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace photo\relation;

use conf\conf;
use generic\controller as genericController;
use web\request;
use photo;
use user;

use securityException;
use userInsufficientRightsSecurityException;

/**
 * Controller for photo\relation
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewUpdate    = view\update::class;

    /** @var photo First photo in this relation */
    private $photo1;
    /** @var photo Second photo in this relation */
    private $photo2;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "confirm", "delete", "display", "edit", "insert",
        "new", "update"
    );

   /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        try {
            $user=user::getCurrent();
            if (!$user->isAdmin()) {
                throw new userInsufficientRightsSecurityException("Admin user required");
            }

            $photoId1 = (int) $this->request["photo_id_1"];
            $photoId2 = (int) $this->request["photo_id_2"];
            $this->photo1 = new photo($photoId1);
            $this->photo2 = new photo($photoId2);
            $this->object = new model($this->photo1, $this->photo2);
            if ($photoId1 !== 0 && $photoId2) {
                $this->object->lookup();
                $this->photo1->lookup();
                $this->photo2->lookup();
            }

            parent::doAction($action);
        } catch (securityException $e) {
            $this->actionForbidden($e);
        }
    }

    /**
     * Do action 'confirm'
     */
    protected function actionConfirm() {
        parent::actionConfirm();
        $this->view->setRedirect("photo?photo_id=" . $this->photo1->getId());
    }

}
