<?php
/**
 * View for confirm delete page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace page\view;

use web\view\viewInterface;
use template\actionlink;
use template\block;
use web\request;

/**
 * This view displays the "confirm page" page
 */
class confirm extends view implements viewInterface {

   /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        $param = array("page_id" => (int) $this->object->getId());
        return array(
            new actionlink("confirm", "page/confirm", $param),
            new actionlink("cancel", "page", $param)
        );
    }

    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             => $this->getTitle(),
            "actionlinks"       => null,
            "mainActionlinks"   => $this->getActionlinks(),
            "obj"               => $this->object,
        ));

    }

    public function getTitle() : string {
        return sprintf(translate("Confirm deletion of '%s'"), $this->object->getName());
    }
}
