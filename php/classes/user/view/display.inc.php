<?php
/**
 * View to display user
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace user\view;

use conf\conf;
use user;
use template\actionlink;
use template\block;
use template\form;
use template\template;
use web\request;
use zoph\app;

/**
 * Display screen for user
 */
class display extends view {

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : ?array {
        $param = array(
            "user_id" => (int) $this->object->getId()
        );
        return array(
            new actionlink("edit", "user/edit", $param),
            new actionlink("delete", "user/delete", $param),
            new actionlink("change password", "user/password", $param),
            new actionlink("new", "user/password"),
            new actionlink("user", "user/users")
        );
    }


    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $notifyForm=new form("form", array(
            "formAction"        => app::getBasePath() . "mail/notifyuser",
            "onsubmit"          => null,
            "submit"            => translate("Notify User")
        ));

        $notifyForm->addInputHidden("user_id", $this->object->getId());

        $comments=$this->object->getComments();

        $ratingGraph = new block("graph_bar", array(
            "title"         => translate("photo ratings"),
            "class"         => "ratings",
            "value_label"   => translate("rating", 0),
            "count_label"   => translate("count", 0),
            "rows"          => $this->object->getRatingGraph()
        ));

        return new block("displayUser", array(
            "title"         => $this->getTitle(),
            "actionlinks"   => $this->getActionlinks(),
            "obj"           => $this->object,
            "fields"        => $this->object->getDisplayArray(),
            "notifyForm"    => $notifyForm,
            "hasComments"   => (bool) (sizeof($comments) > 0),
            "comments"      => $comments,
            "ratingGraph"   => $ratingGraph
        ));
    }
}
