<?php
/**
 * View for 'not allowed' page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace generic\view;

use web\view\viewInterface;
use exception;
use web\request;
use web\view\view;
use template\block;

/**
 * This view displays a "not allowed" error in case access was denied
 */
class forbidden extends view implements viewInterface {
    /**
     * Create view
     * @param request web request
     */
    public function __construct(protected request $request, private exception $exception) {
    }

    /**
     * Output view
     */
    public function view() : block {
        $tpl = new block("message", array(
            "title" => translate("Access denied"),
            "text"   => $this->exception->getMessage(),
            "class" => "error"
        ));
        $tpl->addActionlinks(array("return" => "zoph.php"));
        return $tpl;
    }

    public function display($template = "error") : void {
        parent::display($template);
    }

}
