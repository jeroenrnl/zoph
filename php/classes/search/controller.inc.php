<?php
/**
 * Controller for searches
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace search;

use generic\controller as genericController;
use photo\collection;
use photos\params;
use search;
use web\request;
use user;

/**
 * Controller for searches
 */
class controller extends genericController {
    protected static $viewConfirm   = view\confirm::class;
    protected static $viewDisplay   = view\display::class;
    protected static $viewNew       = view\update::class;
    protected static $viewUpdate    = view\update::class;
    protected static $viewPhotos    = \photos\view\display::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array("confirm", "delete", "display", "edit", "insert", "new", "update", "search");

    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        if (isset($this->request["search_id"])) {
            $search = new search($this->request["search_id"]);
            $search->lookup();
        } else if ($action=="new") {
            $search=new search();
            $search->setSearchURL($this->request);
            $search->set("owner", user::getCurrent()->getId());
        } else {
            $search=new search();
        }
        $this->setObject($search);
        parent::doAction($action);
    }

    /**
     * Do action 'search'
     */
    public function actionSearch() {
        $photos = collection::createFromRequest($this->request);
        $params = new params($this->request);
        $display = $photos->subset($params->offset, $params->cells);
        $lightbox = $params->getLightBox();

        $this->view=new static::$viewPhotos($this->request, $params);
        $this->view->setPhotos($photos);
        $this->view->setDisplay($display);
        $this->view->setLightBox($lightbox);
        $style = $params->getStyle();
        if ($style) {
            $this->view->setStyle($style);
        }
    }
}
