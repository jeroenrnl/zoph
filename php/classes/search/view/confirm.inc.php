<?php
/**
 * View for confirm delete search
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace search\view;

use template\actionlink;
use template\block;
use web\request;
use photo;
use user;
use search;

/**
 * This view displays the "confirm delete search" page
 */
class confirm extends view {

    /**
     * Get actionlinks
     */
    public function getActionlinks() : ?array {
        $key = array("search_id" => $this->object->getId());
        return array(
            new actionlink("confirm", "search/confirm", $key),
            new actionlink("cancel", "search"),
        );

    }
    /**
     * Output view
     */
    public function view() : block {
        return new block("confirm", array(
            "title"             =>  translate("delete search"),
            "actionlinks"       => $this->getActionlinks(),
            "mainActionlinks"   => null,
            "obj"               => $this->object,
        ));

    }
}
