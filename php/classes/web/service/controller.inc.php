<?php
/**
 * Controller for web services
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\service;

use album;
use category;
use conf\conf;
use feature\base as feature;
use generic\controller as genericController;
use geo\locationLookup;
use import\web as webImport;
use language;
use person;
use photo;
use photographer;
use photo\collection;
use photo\data as photoData;
use photo\people as photoPeople;
use place;
use user;
use web\request;

/**
 * Controller for web services
 */
class controller extends genericController {
    protected static $viewJson = view\json::class;
    protected static $viewXML = view\xml::class;

    /** @var array Actions that can be used in this controller */
    protected $actions = array(
        "album",
        "category",
        "details",
        "feature",
        "importThumbs",
        "location",
        "locationLookup",
        "person",
        "photographer",
        "photoData",
        "photoPeople",
        "place",
        "search",
        "translation"
    );

    /** @var array Objects that can be used to show details */
    protected $detailsObjects = array(
        "album",
        "category",
        "circle",
        "person",
        "place"
    );

    /**
     * Do action 'Details'
     */
    public function actionDetails() {
        $object = $this->request["search"];
        $id = (int) $this->request["id"];

        if (in_array($object, $this->detailsObjects)) {
            $obj = new $object($id);
            $this->view = new static::$viewXML($this->request, $obj->getDetailsXML());
        }
    }

    /**
     * Do action 'locationLookup'
     */
    public function actionLocationlookup() {
        $search = $this->request["search"];
        $server = $this->request->getServerVar("SERVER_NAME");
        $location = new locationLookup($search, $server);
        $data = [ "search" => $search, "lat" => $location->getLat(), "lon" => $location->getLong(), "zoom" => $location->getZoom() ];
        $this->view = new static::$viewJson($this->request, $data);
    }

    /**
     * Do action 'photoData'
     */
    public function actionPhotoData() {
        $this->view = "json";
        $photoId = $this->request["photoId"];
        $photo = new photo($photoId);
        if ($photo->lookup()) {
            $photodata = new photoData($photo);
            $data = $photodata->getData();
        }
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }

    /**
     * Do action 'photoPeople'
     */
    public function actionPhotoPeople() {
        $photoId = (int) $this->request["photoId"];
        $personId = (int) $this->request["personId"];
        $photo = new photo($photoId);
        $person = new person($personId);
        $photoPeople = new photoPeople($photo);
        $user=user::getCurrent();

        if (!$photo->isWritableBy($user)) {
            $this->view = new static::$viewJson($this->request, array(
                "error" => "insufficient rights"
            ));
            return;
        }

        $action = $this->request["action"];
        if (in_array($action, [ "left", "right", "up", "down" ])) {
            list($row, $pos) = $photoPeople->getPosition($person);
        }

        switch ($action) {
            case "left":
                $photoPeople->setPosition($person, $pos - 1);
                break;
            case "right":
                $photoPeople->setPosition($person, $pos + 1);
                break;
            case "up":
                $photoPeople->setRow($person, $row - 1);
                break;
            case "down":
                $photoPeople->setRow($person, $row + 1);
                break;
            case "add":
                $row = (int) $this->request["row"];
                $person->addPhoto($photo);
                $photoPeople->setRow($person, $row);
                break;
            case "remove":
                $person->removePhoto($photo);
                break;
            default:
                // Not making any changes, just outputting status quo
                break;
        }
        if ($photo->lookup()) {
            $photoPeople = new photoPeople($photo);
            $data = $photoPeople->getData();
        }
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }


    /**
     * Do action 'search'
     * This is used in the slideshow to gather the list of photos to show,
     * but could in the future also be used to give a dynamic preview of
     * search actions.
     */
    public function actionSearch() {
        $photoCollection = collection::createFromRequest($this->request);

        $data=$photoCollection->getIds();
        $this->view = new static::$viewJson($this->request, $data ?? null);
    }


    /**
     * Do action 'translate'
     * Retrieves translations so they can be used in javascript-generated elements
     */
    public function actionTranslation() {
        $this->view = new static::$viewJson($this->request, language::getCurrent()->getAllTranslations());
    }

    /**
     * Do action 'feature'
     * Retrieve 'featured' photos
     */
    public function actionFeature() {
        $type = $this->request["type"];
        $count = $this->request["count"] ?? 10;
        $features = feature::getAll();
        $feature = new $features[$type];
        $this->view = new static::$viewJson($this->request, $feature->getData($count));
    }

    /**
     * Do action 'importThumbs'
     * Get thumbnails of photos that are in the import queue
     */
    public function actionImportThumbs() {
        $this->view = new static::$viewXML($this->request, webImport::getThumbsXML());
    }

    /**
     * Do action 'album'
     * Get XML of albums, used for autocomplete
     */
    public function actionAlbum() {
        $search = $this->request["search"];
        $this->view = new static::$viewXML($this->request, album::getXML($search));
    }

    /**
     * Do action 'categories'
     * Get XML of categories, used for autocomplete, as well as assigning categories based on XMP-profiles during web import
     */
    public function actionCategory() {
        $search = $this->request["search"];
        $this->view = new static::$viewXML($this->request, category::getXML($search));
    }

    /**
     * Do action 'place'
     * Get XML of places, used for autocomplete
     */
    public function actionPlace() {
        $search = $this->request["search"];
        $this->view = new static::$viewXML($this->request, place::getXML($search));
    }

    /**
     * Do action 'location'
     * Get XML of locations, used for autocomplete
     */
    public function actionLocation() {
        $this->actionPlace();
    }

    /**
     * Do action 'person'
     * Get XML of people, used for autocomplete
     */
    public function actionPerson() {
        $search = $this->request["search"];
        $this->view = new static::$viewXML($this->request, person::getXML($search));
    }

    /**
     * Do action 'photographer'
     * Get XML of photographers, used for autocomplete
     */
    public function actionPhotographer() {
        $search = $this->request["search"];
        $this->view = new static::$viewXML($this->request, photographer::getXML($search));
    }
}
