<?php
/**
 * 404 not found error
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\view;

use web\view\viewInterface;
use language;
use template\actionlink;
use template\block;
use template\page;
use web\request;

/**
 * Display a not found message
 */
class notfound extends view implements viewInterface {
    /** @var string message to include */
    private $msg = "Not found";

    /**
     * Output view
     */
    public function view() : ?block {
        $tpl = new block("message", array(
            "class" => "error",
            "title" => "Not found",
            "text"  => $this->msg
        ));

        $tpl->addActionLinks(array(
            new actionlink("return", "zoph/welcome")
        ));
        return $tpl;
    }

    /**
     * Output page
     */
    public function display($template = "error") : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }

        $page = new page($template, array(
            "lang"    => language::getCurrentISO(),
            "title"   => $this->getTitle(),
        ));

        $page->addBlock($this->view());
        echo $page;
    }

    public function setMessage(string $msg)  : void {
        $this->msg = $msg;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return "404 not found";
    }

    /**
     * get Headers
     */
    public function getHeaders() : array {
        return array($this->request->getServerVar("SERVER_PROTOCOL") . " 404 Not Found");
    }

    /**
     * Get Response Code
     */
    public function getResponseCode() {
        return 404;
    }
}
