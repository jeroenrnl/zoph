<?php
/**
 * View for redirect page
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace web\view;

use app;
use template\block;
use web\request;

/**
 * This view redirects the user to another page
 */
class redirect extends view {

    /** @var string where to redirect to */
    private $redirect = "zoph.php";

    /**
     * Output view
     */
    public function view() : ?block {
        return null;
    }

   /**
    * Output page
    * @codeCoverageIgnore
    */
    public function display($template = "html") : void {
        foreach ($this->getHeaders() as $header) {
            header($header);
        }
    }

    /**
     * Set the page to redirect to
     * @param string redirect target
     */
    public function setRedirect(string $redirect) : void {
        $this->redirect=app::getBasePath() . $redirect;
    }

    /**
     * Get headers
     */
    public function getHeaders() : array {
        return array("Location: " . $this->redirect);

    }

    public function getActionlinks() : array {
        return array();
    }

    public function getTitle() : string {
        return "";
    }

    public function getScripts() : array {
        return array();
    }

}
