<?php
/**
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * This code is heavily based on:
 * PHP Calendar Class Version 1.4 (5th March 2001)
 *
 * Copyright David Wilkinson 2000 - 2001. All Rights reserved.
 * This software may be used, modified and distributed freely
 * providing this copyright notice remains intact at the head
 * of the file.
 *
 * This software is freeware. The author accepts no liability for
 * any loss or damages whatsoever incurred directly or indirectly
 * from the use of this script. The author of this software makes
 * no claims as to its fitness for any purpose whatsoever. If you
 * wish to use this software you should first satisfy yourself that
 * it meets your requirements.
 *
 * URL:   http://www.cascade.org.uk/software/php/calendar/
 * Email: davidw@cascade.org.uk
 *
 * @author David Wilkinson
 * @author Jeroen Roos
 * @url http://www.cascade.org.uk/software/php/calendar
 * @copyright David Wilkinson 2000 - 2001
 * @package Zoph
 */

namespace calendar;

use DateInterval;
use photo\collection as photos;
use template\block;
use Time;
use zoph\app;

class model {
    /**
     * @var string the field to search on when linking back to photos
     * (date or timestamp)
     */
    private $searchField ="";

    /**
     * Constructor for the Calendar class
     */
    public function __construct() {
    }

    public function setSearchField($search) {
        $this->searchField=$search;
    }

    /**
     * Return the URL to link to in order to display a calendar for a given month/year.
     */
    public static function getCalendarLink(Time $date, $searchField="date") {
        $month=$date->format("m");
        $year=$date->format("Y");
        return app::getBasePath() . "calendar?month=" . $month . "&amp;year=" . $year . "&amp;search_field=" . $searchField;
    }

    /**
     * Return the URL to link to  for a given date.
     */
    public function getDateLink(Time $date) {
        if ($date > new Time) { return; }

        if ($this->searchField == "timestamp") {

            // since timestamps have hms, we have to do
            // timestamp >= today and timestamp < tomorrow
            // Or we could trim the date within Mysql:
            // substring(timestamp, 0, 8) = today

            $today = $date->format("Ymd000000");
            $dateTomorrow = clone $date;
            $dateTomorrow->add(new DateInterval("P1D"));
            $tomorrow = $dateTomorrow->format("Ymd000000");


            $qs =
                rawurlencode("timestamp[0]") . "=" . "$today&" .
                rawurlencode("_timestamp_op[0]") . "=" . rawurlencode(">=") . "&" .
                rawurlencode("timestamp[1]") . "=" . "$tomorrow&" .
                rawurlencode("_timestamp_op[1]") . "=" . rawurlencode("<");
        } else {
            $today=$date->format("Y-m-d");
            $qs = "date=$today";
        }

        return app::getBasePath() . "photos?$qs";
    }

    /**
     * Return the HTML for a specified month
     */
    public function getMonthView(Time $date) {
        $date->setTime(0, 0, 0);
        $prevDate=clone $date;
        $prev = $prevDate->sub(new DateInterval("P1M"));
        $nextDate=clone $date;
        $next = $nextDate->add(new DateInterval("P1M"));

        $daysInMonth=$date->format("t");
        $firstDay=$date->format("w");
        $today=new Time();
        $today->setTime(0, 0, 0);
        $header=$date->format("F Y");

        $days=array();

        for ($i=0; $i < $firstDay; $i++) {
            $days[]=array(
                "date"  => "",
                "link"  => "",
                "class" => "calendar",
                "photo" => null,
                "photocount" => 0
            );
        }

        for ($day=1; $day<=$daysInMonth; $day++) {
            $classes="calendar day";
            $photos=photos::createFromVars(array("date" => $date->format("Y-m-d")));
            if ($photos instanceof photos && sizeof($photos) > 0) {
                $classes .= " photos";
                $link = $this->getDateLink($date);
                $cover = $photos->random()->pop()->getImagetag(THUMB_PREFIX);
                $count = sizeof($photos);
            } else {
                $link = "";
                $cover = null;
                $count = 0;
            }
            if ($date == $today) {
                $classes .= " today";
            }
            $days[]=array(
                "date"  => $day,
                "link"  => $link,
                "class" => $classes,
                "photo" => $cover,
                "photocount" => $count
            );
            $date->add(new dateInterval("P1D"));
        }



        return new block("calendar", array(
            "prev"      => $this->getCalendarLink($prev, $this->searchField ?? "date"),
            "next"      => $this->getCalendarLink($next, $this->searchField ?? "date"),
            "header"    => $header,
            "days"      => $days
        ));
    }
}

?>
