<?php
/**
 * common parts of view for comment
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace comment\view;

use web\view\viewInterface;
use comment;
use conf\conf;
use photo;
use template\actionlink;
use template\block;
use template\template;
use user;
use web\request;
use web\view\view as webView;

/**
 * Common parts for comment views
 */
abstract class view extends webView implements viewInterface {
    protected $photo;

    public function __construct(protected request $request, protected array|comment $object) {
        if (!is_array($this->object)) {
            $photo = $this->object->getPhoto();
            if (!$photo instanceof photo && $this->object->getId() == 0) {
                $photo = new photo((int) $request["photo_id"]);
                $photo->lookup();
            }
            $this->photo=$photo;
        }
    }

    /**
     * Get actionlinks
     * @return array actionlinks
     */
    protected function getActionlinks() : array {
        $user=user::getCurrent();
        $photoId = array("photo_id" => $this->photo->getId());
        $commentId = array("comment_id" => $this->object->getId());
        $actionlinks=array(
            new actionlink("return", "photo", $photoId)
        );

        if ($this->request->getAction() != "new" && $user->isAdmin() || $this->object->isOwner($user)) {
            $actionlinks[] = new actionlink("edit", "comment/edit", $commentId);
            $actionlinks[] = new actionlink("delete", "comment/delete", $commentId);
        }
        return $actionlinks;
    }

    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return $this->object->get("subject");
    }
}
