<?php
/**
 * View for editting permissions
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace permissions\view;

use album;
use conf\conf;
use group;
use template\block;
use template\template;
use zoph\app;

/**
 * View for editting permissions
 */
class edit {

    /**
     * Create view from object
     * @param group|album Object to operate on
     */
    public function __construct(private album|group $object) {
    }

    /**
     * Output view
     */
    public function view() : block {
        $accessLevelAll=template::createInputNumber("access_level_all", 5, null, 1, 10);
        $wmLevelAll=template::createInputNumber("watermark_level_all", 5, null, 1, 10);
        $accessLevelNew=template::createInputNumber("access_level_new", 5, null, 1, 10);
        $wmLevelNew=template::createInputNumber("watermark_level_new", 5, null, 1, 10);

        $edit = $this->object instanceof album ? "group" : "album";
        return new block("editPermissions", array(
            "action"            => app::getBasePath() . "permissions/update" . $edit . "s",
            "watermark"         => conf::get("watermark.enable"),
            "edit"              => $edit,
            "fixed"             => get_class($this->object),
            "id"                => $this->object->getId(),
            "edit_id"           => $edit . "_id",
            "accessLevelAll"    => $accessLevelAll,
            "wmLevelAll"        => $wmLevelAll,
            "accessLevelNew"    => $accessLevelNew,
            "wmLevelNew"        => $wmLevelNew,
            "permissions"       => $this->object->getPermissionArray()
        ));
    }
}
