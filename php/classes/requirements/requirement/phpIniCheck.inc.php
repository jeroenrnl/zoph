<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
abstract class phpIniCheck extends requirement {

    abstract protected function check();

    protected function toBytes($size) {
        if (substr($size, -1) == "K") {
            $size = (int) $size * 1024;
        } else if (substr($size, -1) == "M") {
            $size = (int) $size * 1024 * 1024;
        } else if (substr($size, -1) == "G") {
            $size = (int) $size * 1024 * 1024 * 1024;
        } else {
            $size = (int) $size;
        }
        return $size;
    }
}
?>
