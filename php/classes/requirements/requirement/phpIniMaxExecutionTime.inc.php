<?php
/**
 * Requirement class
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace requirements\requirement;

use conf\conf;

/**
 * Requirements class
 *
 * @author Jeroen Roos
 * @package Zoph
 */
class phpIniMaxExecutionTime extends requirement {

    protected const NAME = "php.ini: max_execution_time";
    protected const DESCRIPTION = "Maximum time Zoph can spend executing a script (relevant for resizing large photos)";
    protected const MSG_PASS = "max_execution_time sufficient.";
    protected const MSG_FAIL = "max_execution_time should be 30 seconds or more (60 or more recommended).";
    protected const MSG_WARNING = "max_execution_time is set to less than 60 seconds, this may not be sufficient for resizing large photos.";

    protected function check() {
        $time = (int) ini_get("max_execution_time");
        if ($time === 0) {
            // 0 (unlimited) is ok
            return static::PASS;
        } else if ($time < 30) {
            return static::FAIL;
        } else if ($time < 60) {
            return static::WARNING;
        } else {
            return static::PASS;
        }
    }

}
?>
