<?php
/**
 * View to show the main (welcome) page of Zoph
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace zoph\view;

use web\view\viewInterface;
use conf\conf;
use feature\base as feature;
use template\block;
use template\page;
use template\template;
use user;
use web\request;
use web\view\view;

/**
 * Display welcome screen
 */
class display extends view implements viewInterface {

    /**
     * Get view
     * @return template view
     */
    public function view() : block {
        $tpl = new block("main", array(
            "title"         => $this->getTitle()
        ));

        foreach (feature::getAll() as $type => $class) {
            $feature = new $class;
            $tpl->addBlock(new block("feature", array(
                "class" => $type,
                "type"  => $type,
                "title" => translate($feature->getTitle()),
                "thumb" => $feature->getWidth(),
                "more"  => $feature->getMore()
            )));
        }

        $tpl->addBlock(new block("version", array(
            "version"   => VERSION
        )));

        return $tpl;
    }

    public function getScripts() : array {
        $scripts = parent::getScripts();
        $scripts[] = "js/feature.js";
        return $scripts;
    }

}
