<?php
/**
 * View for display mail result
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use template\form;
use template\template;
use user;

/**
 * This view displays the result of a mail action
 */
class result extends view implements viewInterface {

    /**
     * Output view
     */
    public function view() : block {
        $tpl=new block("main", array(
            "title"             => $this->getTitle(),
        ));

        foreach (array("error", "warning", "success") as $msgType) {
            if (isset($this->$msgType)) {
                $tpl->addBlock(new block("message", array(
                    "class"     => $msgType,
                    "title"     => "mail: " . $msgType,
                    "text"      => $this->$msgType
                )));
            }
        }

        $tpl->addActionlinks($this->getActionlinks());

        return $tpl;
    }
}
