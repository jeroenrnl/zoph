<?php
/**
 * common parts of view mail
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace mail\view;

use web\view\viewInterface;
use conf\conf;
use template\block;
use template\form;
use photo;
use user;
use web\request;
use web\view\view as webView;
use zoph\app;

/**
 * Common parts for mail views
 */
abstract class view extends webView implements viewInterface {
    /** @var string error message */
    protected $error;

    /** @var string warning message */
    protected $warning;

    /** @var string success message */
    protected $success;

    public function __construct(protected request $request, protected ?photo $object = null) {
    }

    /**
     * Get action links
     * @return array action links
     */
    protected function getActionlinks() : array {
        return array();
    }
    /**
     * Get the title for this view
     */
    public function getTitle() : string {
        return translate("email photo");
    }

    public function setMessage(string $error = null, string $warning = null, string $success = null) : void {
        $this->error = $error ?? null;
        $this->warning = $warning ?? null;
        $this->success = $success ?? null;
    }

    protected function getMailForm(string $action, string $msg) : form {
        $user = user::getCurrent();

        if (isset($this->user) && $this->user instanceof user) {
            $toName = $this->user->person ? $this->user->person->getName() : "";
            $toEmail = $this->user->person ? $this->user->person->getEmail() : "";
        } else {
            $toName = $toEmail = "";
        }

        $fromName = $user->person->getName();
        $fromEmail = $user->person->getEmail();

        $form = new form("form", array(
            "class"         => "email",
            "formAction"    => app::getBasePath() . "mail/" . $action,
            "action"        => $action,
            "submit"        => translate("email"),
            "onsubmit"      => null
        ));

        $msg = translate("Hi", 0) . " " . $toName .  ",\n\n" . $msg . "\n\n";
        $msg .= translate("Regards,", 0) . "\n" .  $fromName;

        $form->addInputText("to_name", $toName, translate("to (name)"), sprintf(translate("%s chars max"), "32"), 32, 24);
        $form->addInputEmail("to_email", $toEmail, translate("to (email)"));
        $form->addInputText("from_name", $fromName, translate("from (your name)"), sprintf(translate("%s chars max"), "32"), 32, 24);
        $form->addInputEmail("from_email", $fromEmail, translate("from (your email)"));
        $form->addTextarea("message", $msg, translate("message"), 70, 5);

        return $form;
    }



}
