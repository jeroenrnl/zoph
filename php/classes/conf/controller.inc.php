<?php
/**
 * Controller for config
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace conf;

use generic\controller as genericController;
use web\request;

use user;

/**
 * Controller for config
 */
class controller extends genericController {
    protected static $viewDisplay   = view\display::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "update"
    );

    /**
     * Do action
     * @param string action
     */
    public function doAction(string $action) : void {
        if (user::getCurrent()->isAdmin()) {
            parent::doAction($action);
        } else {
            $this->view=new static::$viewRedirect($this->request);
            $this->view->setRedirect("zoph/welcome");
        }
    }


    /**
     * Do action 'update'
     */
    public function actionUpdate() {
        $user=user::getCurrent();
        if ($user->isAdmin()) {
            conf::loadFromRequestVars($this->request->getRequestVars());
        }
        $this->actionDisplay();
    }
}
