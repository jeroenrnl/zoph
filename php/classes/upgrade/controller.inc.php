<?php
/**
 * Controller for migrations
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use db\db;
use db\exception\connectionException as dbConnectionException;
use generic\controller as genericController;
use template\block;
use template\result;
use user;
use web\request;


/**
 * Controller for migrations
 */
class controller extends genericController {
    protected static $viewDisplay   = view\display::class;
    protected static $viewUpgrade   = view\upgrade::class;
    protected static $viewNotFound  = view\notfound::class;
    protected static $viewResult    = view\result::class;

    /** @var array Actions that can be used in this controller */
    protected   $actions    = array(
        "display", "upgrade", "rollback"
    );

    /** @var array migrations to be done */
    private $migrations;

    /** @var string Where to redirect after actions */
    public $redirect="zoph";

    public function setMigrations(migrations $migrations) {
        $this->migrations = $migrations;
    }

    /**
     * Do action 'display'
     */
    public function actionDisplay() {
        if (empty($this->migrations)) {
            $this->setMigrations(new migrations());
        }
        $user = user::getCurrent();
        if (!$user->isAdmin()) {
            $this->view=new static::$viewRedirect($this->request);
            $this->view->setRedirect("zoph/welcome");
            return;
        }

        if ($this->migrations->check(conf::get("zoph.version"))) {
            $this->view=new static::$viewDisplay($this->request, $this->migrations);
        } else {
            $this->view=new static::$viewNotFound($this->request);
        }
    }

    /**
     * Do action 'upgrade'
     */
    public function actionUpgrade() {
        if (empty($this->migrations)) {
            $this->setMigrations(new migrations());
        }
        $doneMigrations=array();
        if ($this->migrations->check(conf::get("zoph.version"))) {
            $dbLogin = db::getLoginDetails();

            $dbuser = $this->request["user"];
            $dbpass = $this->request["pass"];

            if (empty($dbuser) || empty($dbpass)) {
                $dbuser = $dbLogin["user"];
                $dbpass = $dbLogin["pass"];
            }

            db::setLoginDetails($dbLogin["host"], $dbLogin["dbname"], $dbuser, $dbpass, $dbLogin["prefix"]);
            db::disconnect();

            try {
                db::testConnection();
            } catch (dbConnectionException $e) {
                $this->view=new static::$viewResult($this->request);
                $this->view->setMessage(error: $e->getMessage());

                // This will not give a very nice output, but if we
                // run Zoph the normal way, the template will require a database
                // connection and will error out with different, less obvious
                // error.
                echo $this->view->view();
                exit(99);
            }


            foreach ($this->migrations->get() as $migration) {
                $results = $migration->up();
                $doneMigrations[] = new block("migration", array(
                    "desc"      => $migration->getDesc(),
                    "results"   => $results
                ));
                $error = false;
                foreach ($results as $result) {
                    if ($result->getResult() == result::ERROR) {
                        $error = true;
                    }
                }
                if ($error) {
                    $doneMigrations[] = new block("migration", array(
                        "desc" => "Migration failed",
                        "results" => array(new result(result::ERROR, "One or more migrations has failed, the rest of the migrations was skipped. The upgrade not completed"))
                    ));
                    break;
                } else {
                    conf::set("zoph.version", $migration->getTo())->update();
                }
            }
            $this->view=new static::$viewUpgrade($this->request);
            $this->view->setComplete($doneMigrations);
        } else {
            $this->view=new static::$viewNotFound($this->request);
        }
    }

    /**
     * Do action 'rollback'
     */
    public function actionRollback() {
        // Not implemented
    }
}
