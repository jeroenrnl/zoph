<?php
/**
 * This is a trait to add the add and remove members to both groups and cirlces
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package Zoph
 */

namespace organiser;

use template\block;

/**
 * Actions add/remove members for groups/circles
 *
 * @author Jeroen Roos
 * @package Zoph
 */
trait addRemoveMembersUpdateActionTrait {
    /**
     * Action: update
     * The update action processes a form as generated after the "edit" action.
     * The subsequently called view displays the object.
     * takes care of adding and removing members of the circle or group
     */
    protected function actionUpdate() : void {
        $this->object->setFields($this->request->getRequestVars());
        if (isset($this->request["_member"]) && ((int) $this->request["_member"] > 0)) {
            $this->object->addMember(new static::$memberClass((int) $this->request["_member"]));
        }

        if (is_array($this->request["_remove"])) {
            foreach ($this->request["_remove"] as $id) {
                $this->object->removeMember(new static::$memberClass((int) $id));
            }
        }
        $this->object->update();
        $this->view = new static::$viewUpdate($this->request, $this->object);
    }
}
