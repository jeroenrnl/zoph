// This file is part of Zoph.
//
// Zoph is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// 
// Zoph is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with Zoph; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


var zImport=function() {
    var categories = new Array();
    var people = new Array();
    var categoriesLoaded = false;
    var peopleLoaded = false;

    function httpResponse(object, xml) {
        var root=xml.getElementsByTagName('importprogress');
        var importnode=root[0].firstChild;
        var id=importnode.firstChild.firstChild.nodeValue;
        var currentnode=importnode.childNodes[1];
        var current, total, filename, percent;
        if(currentnode.childNodes.length===0) {
            current=0;
            total=0;
            filename="unknown";
        } else {
            current=currentnode.firstChild.nodeValue;
            total=importnode.childNodes[2].firstChild.nodeValue;
            filename=importnode.childNodes[3].firstChild.nodeValue;
        }
        
        var fn=document.getElementById("fn_" + id);
        fn.innerHTML=filename;

        var MB = parseInt(parseInt(total, 10) / 1024 / 102.4, 10) / 10;

        var size=document.getElementById("sz_" + id);
        size.innerHTML=MB.toString() + " MiB";

        var progressdiv=document.getElementById("pb_" + id + "_inner");
        if(total > 0) {
            percent = parseInt(
                parseInt(current, 10) / parseInt(total,10) * 100,10);
        } else {
            percent=0;
        }
        progressdiv.innerHTML=percent + "%";
        progressdiv.style.width=percent.toString() + "%";
    }

    function httpResponseImport(object, xml) {
        if (xml.firstChild.tagName == "categories") {
            categories = makeList(xml);
            categoriesLoaded = true;
        } else if (xml.firstChild.tagName == "people") {
            people = makeList(xml);
            peopleLoaded = true;
        } else {
            console.log("Error with XML");
        }
        init();
    }

    function makeList(nodes, list = Array()) {
        var xmlTags = Array( 
            "categories",
            "category",
            "people", 
            "person"
        );

        var c = nodes.childNodes;
        var key;
        var name;

        for (var i=0; i < c.length; i++) {
            if (xmlTags.includes(c[i].nodeName)) {
                list=makeList(c[i], list);
            } else if (c[i].nodeName=="key") {
                key=c[i].textContent;
            } else if (c[i].nodeName=="title") {
                name=c[i].textContent;
            }

            if (typeof name != "undefined" && typeof key != "undefined") {
                list.push({ name: name, key: parseInt(key,10)});
            }
        }
        return list;
    }

    function init() {
        if (!categoriesLoaded) {
            XML.getData("categories", "");
        } else if (!peopleLoaded) {
            XML.getData("people", "");
        } else {
            zImport.getThumbs(false);
            var button=document.getElementById("import_button");
            button.addEventListener("click", function() { zImport.importPhotos();}, false);
        }
    }

    function getThumbs(notimer) {
        var http=new XMLHttpRequest();
        http.open("GET", basePath + "service/importThumbs", true);
        http.onreadystatechange=function() { 
            zImport.showThumbs(http); 
        };
        http.send(null);
        if(!notimer) {
            setTimeout(function() { zImport.getThumbs(false); }, 15000);
        }
    }

    function showThumbs(http) {
        var content;
        var status;
        var imgsrc;
        var importli;
        var xmlimport;

        if (http.readyState == 4) {
            if(http.status == 200) {
                var response=http.responseXML;
                var thumbswindow=document.getElementById("import_thumbs");
                var thumbs=document.getElementById("import_thumbnails");
                
                var files=response.getElementsByTagName("file");
                var ids=[];
                if(files.length>0) {
                    thumbswindow.style.display="block";
                    for(var i=0; i<files.length; i++) {
                        var md5 = null;
                        status = null;
                        var icon = null;
                        var subjects = Array();
                        var ppl = Array();
                        var rating = null;
                        var sidecar = null;
                        for(var c=0; c<files[i].childNodes.length; c++) {
                            var tag=files[i].childNodes[c];
                            content=null;
                            content=tag.textContent;
                            switch(tag.nodeName) {
                            case "md5": 
                                md5=content;
                                ids.push(md5);
                                break;
                            case "status":
                                status=content;
                                break;
                            case "icon":
                                icon=content;
                                break;
                            case "subjects":
                                showXMPCheckbox("xmpcategories");
                                for (var s=0; s<files[i].childNodes[c].childNodes.length; s++) {
                                    var subject = files[i].childNodes[c].childNodes[s].textContent;
                                    subjects.push(subject);
                                }
                                break;
                            case "people":
                                showXMPCheckbox("xmppeople");
                                for (var s=0; s<files[i].childNodes[c].childNodes.length; s++) {
                                    var person = files[i].childNodes[c].childNodes[s].textContent;
                                    ppl.push(person);
                                }
                                break;
                            case "rating":
                                showXMPCheckbox("xmprating");
                                rating = parseInt(files[i].childNodes[c].textContent, 10)
                                break;
                            case "sidecar":
                                sidecar = files[i].childNodes[c].textContent;
                                break;
                            }
                        }
                        var deleteli=document.createElement("li");
                        var actionlinks=document.createElement("ul");
                        
                        actionlinks.className="actionlink";
                        
                        var del=createNode("a", translate['delete']);
                        del.href="#";
                        del.setAttribute("onClick", "zImport.doAction('delete', '" + md5 + "'); return false");
                        deleteli.appendChild(del);

                        var retryli=document.createElement("li");
                        var retry=createNode("a", translate['retry']);
                        retry.href="#";
                        retry.setAttribute("onClick", "zImport.doAction('retry', '" + md5 + "'); return false");
                        retryli.appendChild(retry);
                        
                        actionlinks.appendChild(deleteli);

                        var checkbox=document.createElement("input");
                        checkbox.setAttribute("type", "checkbox");
                        checkbox.setAttribute("name", "cb_" + md5);
                        checkbox.className="thumb_checkbox";
                        
                        var existing=document.getElementById(md5);
                        if(existing) {
                            for(var e=0; e<existing.childNodes.length; e++) {
                                tag=existing.childNodes[e];
                                if(tag.nodeName=="IMG") {
                                    if(((tag.className=="waiting" || tag.className=="busy") &&
                                          (status=="done" || status=="ignore")) 
                                          || tag.className=="" && status!="done"){
                                        // so this thumb is out of sync with
                                        // the status on disk. Could be due to
                                        // clicking 'back' or 'reload'
                                        // We delete it, so it will be recreated 
                                        // correctly the next round.
                                        deleteNode(tag.parentNode);
                                        setTimeout(function() { zImport.getThumbs(true); }, 500);
                                    }
                                }
                            }

                        } else {
                            var div=document.createElement("div");
                            div.id=md5;
                            div.className="thumbnail";
                            var name=files[i].getAttribute("name");
                            var type=files[i].getAttribute("type");
                            
                            var filename=createNode("span", name);
                            filename.className="filename";

                            var img=document.createElement("img");
                            switch(status) {
                            case "done":
                                if(type=="gpx") {
                                    imgsrc=icon;
                                    importli=document.createElement("li");
                                    xmlimport=createNode("a", translate['import']);
                                    xmlimport.href="#";
                                    xmlimport.setAttribute("onClick", "zImport.doAction('process', '" + md5 + "'); return false");
                                    importli.appendChild(xmlimport);
                                    actionlinks.appendChild(importli);
                                } else {
                                    actionlinks.appendChild(retryli);
                                    div.appendChild(checkbox);
                                    imgsrc="image/importThumb?file=" + md5;
                                    img.setAttribute("onmouseover", "zImport.createPreviewDiv('" + md5 +"');");
                                    img.setAttribute("onmouseout", "zImport.destroyPreviewDiv('" + md5 +"');");
                                }
                                break;
                            case "waiting":
                                img.className="waiting";
                                imgsrc=icon;    
                                break;
                            case "ignore":
                                img.className="ignore";
                                actionlinks.appendChild(retryli);
                                imgsrc=icon;    
                                break;
                            default:
                                imgsrc="";
                                break;
                            }
                            var categoryul=document.createElement("ul");
                            categoryul.className="categories";
                            var cat = 0;
                            for (var subject in subjects) {
                                var categoryli=createNode("li", subjects[subject]);
                                var category = categories.filter(ctg => ctg.name.toLowerCase() == subjects[subject].toLowerCase() );
                                if (category.length == 0) {
                                    categoryli.className="unknown";
                                } else {
                                    var input=document.createElement("input");
                                    input.setAttribute("type", "hidden");
                                    input.setAttribute("id", "category_" + md5 + "[" + cat + "]");
                                    input.value = category[0].key;
                                    div.appendChild(input);
                                    cat++;
                                }
                                categoryul.appendChild(categoryli);
                            }

                            var peopleul=document.createElement("ul");
                            peopleul.className="people";
                            var pplcount = 0;
                            for (var person in ppl) {
                                var personli=createNode("li", ppl[person]);
                                var person = people.filter(psn => psn.name.toLowerCase() == ppl[person].toLowerCase() );
                                if (person.length == 0) {
                                    personli.className="unknown";
                                } else {
                                    var input=document.createElement("input");
                                    input.setAttribute("type", "hidden");
                                    input.setAttribute("id", "person_" + md5 + "[" + pplcount + "]");
                                    input.value = person[0].key;
                                    div.appendChild(input);
                                    pplcount++;
                                }
                                peopleul.appendChild(personli);
                            }

                            img.setAttribute("src", imgsrc);
                            div.appendChild(actionlinks);
                            div.appendChild(img);
                            div.appendChild(filename);

                            if (rating) {
                                div.appendChild(zRating.rating(rating, "rating_" + md5));
                            }
                            div.appendChild(categoryul);
                            div.appendChild(peopleul);

                            if (sidecar) {
                                // For now we only show one badge, but let's be prepared
                                // for multiple badges...
                                ul = document.createElement("ul");
                                ul.className = "badges";
                                li = document.createElement("li");
                                img = document.createElement("img");
                                img.src=icons["sidecar"];
                                img.setAttribute("alt", "sidecar");
                                img.setAttribute("title", sidecar);
                                li.appendChild(img);
                                ul.appendChild(li);

                                var input=document.createElement("input");
                                input.setAttribute("type", "hidden");
                                input.name = "_sidecar_" + md5;
                                input.setAttribute("id", "_sidecar_" + md5);
                                input.value = sidecar;
                                div.appendChild(input);
                                div.appendChild(ul);

                            }
                            thumbs.appendChild(div);
                        }
                    }
                    
                } else {
                    thumbswindow.style.display="none";
                }
                // Remove all thumbs for which the file no longer
                // exists
                for(var thumb of thumbs.childNodes) {
                    if(thumb.className=="thumbnail") {
                        if(findInArray(ids,thumb.id)==-1) {
                            thumbs.removeChild(thumb);
                            setTimeout(function() { zImport.getThumbs(true); }, 500);
                        }
                    }
                }

                // Sort the nodes by Filename
                var names=getElementsByClass("filename");
                var oldfile="";
                for(var f=0; f<names.length; f++) {
                    var file=names[f].innerHTML;
                    if(file<oldfile) {
                        for(var n=0; n<names.length; n++) {
                            if(file<names[n].innerHTML) {
                                   names[f].parentNode.parentNode.insertBefore(names[f].parentNode, names[n].parentNode);
                                   break;
                            }
                        }
                    } else {
                        oldfile=file;
                    }
                }
                // Re-enable the submit button
                var button=document.getElementById("import_button");
                button.disabled=false;
                
                processFiles();
                    
            }
        }
    }

    function showXMPCheckbox(id) {
        document.getElementById("xmpimport").style.display="block";
        document.getElementById(id).style.display="block";
        document.getElementById(id).labels[0].style.display="block";
    }

    function processFiles() {
        var waiting=getElementsByClass("waiting");
        var busy=getElementsByClass("busy");

        if(parallel < 1) {
            parallel = 1;
        }
        if(waiting.length > 0 && busy.length < parallel) {
            busy=waiting[0];
            busy.className="busy";
            var thumbs=top.document.getElementById("import_thumbs");
            thumbs.style.display="block";
            var md5=busy.parentNode.id;
            var filename=busy.nextSibling.innerHTML;

            switch(getFileType(filename)) {
            case "image":
                busy.src=icons["resize"];
                break;
            case "archive":
                busy.src=icons["unpack"];
                break;
            }
            doAction("process", md5);
        } else {
        }
    }

    function processDone(html) {
        if(html) {
            var output=top.document.getElementById("import_details_text");
            var p=document.createElement("p");
            var t=document.createElement("p");
            t.innerHTML=html;
            output.appendChild(p);
            p.innerHTML=t.innerHTML;
            output.parentNode.style.display="block";
        }
    }

    function doAction(action,md5) {
        var http=new XMLHttpRequest();
        http.open("GET", "import/" + action + "?file=" + md5, true);
        var thumb=document.getElementById(md5);
        if(action=="delete" || action=="retry") {
            deleteNode(thumb);
        }
        http.onreadystatechange=function() {
            XML.httpResponse(http,'action');
        };
        http.send(null);
        setTimeout(function() { zImport.getThumbs(true); }, 500);
    }

    function deleteSelected() {
        var images=getElementsByClass("thumb_checkbox");
        var toDelete=[];
        for(var i=0; i<images.length; i++) {
            if(images[i].checked) {
                var cb=images[i].name.split("_");
                var id=cb[1];
                toDelete.push(id);
             }
         }
        for(var i=0; i<toDelete.length; i++) {
            doAction("delete", toDelete[i]);
         }
    }

    function toggleSelection() {
        var images=getElementsByClass("thumb_checkbox");
        for(var i=0; i<images.length; i++) {
            if(images[i].checked) {
                images[i].checked=false;
            } else {
                images[i].checked=true;
            }
         }
    }

    function selectAll() {
        var images=getElementsByClass("thumb_checkbox");
        for(var i=0; i<images.length; i++) {
            images[i].checked=true;
         }
    }

    function importPhotos() {
        var toImport=0;

        var form=document.getElementById("import_form");
        // Disable the submit button to prevent submitting twice
        // it will be reactivated after refreshing the thumbnails

        var button=document.getElementById("import_button");
        button.disabled=true;

        // Delete the old checkboxes, if any.
        var fieldset=document.getElementById("import_checkboxes");
        if(fieldset) {
            removeChildren(fieldset);
        } else {
            fieldset=document.createElement("fieldset");
            fieldset.id="import_checkboxes";
            form.appendChild(fieldset);
        }
        // Now copy the checkboxes from the form above into this form
        var images=getElementsByClass("thumb_checkbox");
        for (var i=0; i<images.length; i++) {
            if (images[i].checked) {
                var input=document.createElement("input");
                input.name="_import_image[" + toImport + "]";
                input.className="import_image";

                var cb=images[i].name.split("_");
                input.value=cb[1];
                fieldset.appendChild(input);

                if (document.getElementById("xmpcategories").checked) {
                    copyXMPfields("category", fieldset, cb[1]);
                }
                if (document.getElementById("xmpcategories").checked) {
                    copyXMPfields("person", fieldset, cb[1]);
                }

                var sidecar = document.getElementById("_sidecar_" + cb[1]);
                if (sidecar) {
                    fieldset.appendChild(sidecar);
                }

                var rating = document.getElementById("rating_" + cb[1]);
                if (rating && document.getElementById("xmprating").checked) {
                    var ratinginput=document.createElement("input");

                    ratinginput.name="_rating_" + cb[1];
                    ratinginput.value = rating.dataset.rating;
                    fieldset.appendChild(ratinginput);
                }

                toImport++;
            }
        }
        if(toImport>0) {
            XML.submitForm(form, basePath + "import/import");
        } else {
            alert("You need to select at least one photo");
        }
    }

    function copyXMPfields(field, fieldset, md5) {
        var counter = 0
        while ( counter != -1) {
            var input = document.getElementById(field + "_" + md5 + "[" + counter + "]");
            if (input && input.className != "unknown" ) {
                var newinput=document.createElement("input");

                newinput.name="_" + field + "_" + md5 + "["  + counter + "]";
                newinput.value = input.value;
                fieldset.appendChild(newinput);
                counter++;
            } else {
                counter = -1;
            }
        }
    }
    function createPreviewDiv(md5) {
        var div=document.createElement("div");
        var img=document.createElement("img");
        var body=document.getElementsByTagName("body")[0];
        
        div.className="preview";
        div.id="preview"+md5;

        img.setAttribute("src", basePath + "image/importMid?file=" + md5);
        div.appendChild(img);

        body.appendChild(div);
    }
    
    function destroyPreviewDiv(md5) {
        var div=document.getElementById("preview" + md5);
        deleteNode(div);
    }

    return {
        init:init,
        getThumbs:getThumbs,
        showThumbs:showThumbs,
        deleteSelected:deleteSelected,
        selectAll:selectAll,
        toggleSelection:toggleSelection,
        doAction:doAction,
        httpResponse:httpResponse,
        httpResponseImport:httpResponseImport,
        processDone:processDone,
        importPhotos:importPhotos,
        createPreviewDiv:createPreviewDiv,
        destroyPreviewDiv:destroyPreviewDiv
    };
}();

if(window == top) {
    window.addEventListener("load", function(){ zImport.init(); },false);
}
