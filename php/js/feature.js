let feature=function() {
    function init(e=false) {
        const features = document.getElementsByClassName("featured");
        const width = document.getElementById("main").clientWidth
        
        for (let feature of features) {
            let type = feature.dataset["type"];
            let count = Math.floor((width - 25)/(parseInt(feature.dataset["thumb"])))
            getFeaturePhotos(type, count);
        }
    }

    function getFeaturePhotos(type, count) {
        const url = basePath + "service/feature?type=" + type + "&count=" + count
        let http=new XMLHttpRequest()
        http.responseType = 'json'

        http.open("GET", url, true)
        http.send()
        http.onreadystatechange=function() {
            if (http.readyState == XMLHttpRequest.DONE) {
                processFeatures(http)
            }
        }
    }

    function processFeatures(http) {
        let response = http.response;

        let type = response["type"]
        let objects = response["objects"]
        let ul = null

        const feature = document.querySelector('[data-type="' + type + '"]')
        for (const child of feature.children) {
            if (child.className == "featured-images") {
                ul = child
            }
        }
        switch(type) {
            case "albums":
                featureOrganisers(ul, objects)
                break
            default:
                featurePhotos(ul, objects)
                break
        }
    }

    function featurePhotos(ul, objects) {

        for (let photoId of objects) {
            let a = document.createElement("a");
            a.href = basePath + "photo?photo_id=" + photoId

            let img = document.createElement("img");
            img.className = "thumb";
            img.src = basePath + "image/thumb?photo_id=" + photoId
            let li = document.createElement("li")
            a.appendChild(img)
            li.appendChild(a)
            ul.appendChild(li)
        }

    }
    
    function featureOrganisers(ul, objects) {
        for (let object of objects) {
            let a = document.createElement("a");
            a.href = basePath + "album?album_id=" + object["id"]
            
            let div = document.createElement("div")
            div.className = "coverphoto"

            if (object["cover"] != 0) {
                let img = document.createElement("img");
                img.className = "thumb"
                img.src = basePath + "image/thumb?photo_id=" + object["cover"]
                div.appendChild(img)
                div.appendChild(document.createElement("br"))
            }
            //covera = a.cloneNode()
            a.appendChild(div)
            
            let titlediv = document.createElement("div")
            titlediv.className = "name"

            titlediv.appendChild(document.createTextNode(object["title"]))
            a.appendChild(titlediv)

            let li = document.createElement("li")
            li.className = "thumb_album"
            //li.appendChild(covera)
            li.appendChild(a)
            ul.appendChild(li)
        }
        ul.classList.add("thumbs")
    }

    return {
        init:init,
        getFeaturePhotos:getFeaturePhotos,
    };
}();

window.addEventListener("load",feature.init,false);

