<?php
/**
 * Database migrations for v0.9.22
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use tables\sets;
use tables\photo_sets;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

/**
 * Make database changes for v0.9.22
 *
 * @codeCoverageIgnore
 */
class zoph0922 extends migration {
    protected const DESC="Upgrade to v0.9.22";
    protected const FROM="v00.09.21.000";
    protected const TO="v00.09.22.000";

    public function __construct() {
        $sets = new sets();
        $photoSets = new photo_sets();

        $this->addStep(self::UP, $sets->getStructure(), "Add table for sets");
        $this->addStep(self::UP, $photoSets->getStructure(), "Add link table for photos and sets");
    }
}

return new zoph0922();
