<?php
/**
 * Database migrations for v0.9.18
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jeroen Roos
 */

namespace upgrade;

use conf\conf;
use db\alter;
use db\db;

if (!defined("MIGRATIONS")) {
    throw new \exception("error");
}

/**
 * Make database changes for v0.9.18
 *
 * @codeCoverageIgnore
 */
class zoph0918 extends migration {
    private $tables=array("albums", "categories", "circles", "circles_people", "color_schemes",
        "comments", "conf", "group_permissions", "groups", "groups_users", "pages",
        "pages_pageset", "pageset", "people", "photo_albums", "photo_categories", "photo_comments",
        "photo_people", "photo_ratings", "photo_relations", "photos", "places", "point", "prefs",
        "saved_search", "track", "users");

    protected const DESC="Upgrade from v0.9.17 to v0.9.18";
    protected const FROM="v00.09.17.000";
    protected const TO="v00.09.18.000";

    public function __construct() {
        foreach ($this->tables as $table) {
            $this->addStep(
                self::UP,
                (new alter($table))->setEngine("InnoDB"),
                "Convert " . db::getPrefix() . $table . " to InnoDB"
            );

            $this->addStep(
                self::DOWN,
                (new alter($table))->setEngine("MyISAM"),
                "Convert " . db::getPrefix() . $table . " to MyISAM"
            );
        }
    }
}

return new zoph0918();
