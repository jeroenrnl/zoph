<?php
/* This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

use template\colorScheme;

?>

:root {
    --breadCrumbBgColour:       <?= colorScheme::getColor("breadcrumb_bg_color") ?>;
    --linkColour:               <?= colorScheme::getColor("link_color") ?>;
    --pageBgColour:             <?= colorScheme::getColor("page_bg_color") ?>;
    --selectedTabBgColour:      <?= colorScheme::getColor("selected_tab_bg_color") ?>;
    --selectedTabFontColour:    <?= colorScheme::getColor("selected_tab_font_color") ?>;
    --tabBgColour:              <?= colorScheme::getColor("tab_bg_color") ?>;
    --tabFontColour:            <?= colorScheme::getColor("tab_font_color") ?>;
    --tableBgColour:            <?= colorScheme::getColor("table_bg_color") ?>;
    --tableBorderColour:        <?= colorScheme::getColor("table_border_color") ?>;
    --textColour:               <?= colorScheme::getColor("text_color") ?>;
    --titleBgColour:            <?= colorScheme::getColor("title_bg_color") ?>;
    --titleFontColour:          <?= colorScheme::getColor("title_font_color") ?>;
}

/* vim: set syntax=css: */
