<?php
/* This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

use conf\conf;
use app\zoph;
use template\template;

?>

:root {
    --thumbSize:            <?= THUMB_SIZE ?>px;
    --midSize:              <?= MID_SIZE ?>px;
    --interfaceWidth:       <?= conf::get("interface.width");  ?>;
    --basePath:             "<?= app::getBasePath() ?>";


    --icon-add:             url(<?= template::getImage("icons/add.png") ?>);
    --icon-album:           url(<?= template::getImage("icons/album.png") ?>);
    --icon-arrow-down:      url(<?= template::getImage("icons/1downarrow.png") ?>);
    --icon-arrow-right:     url(<?= template::getImage("icons/1rightarrow.png") ?>);
    --icon-category:        url(<?= template::getImage("icons/category.png") ?>);
    --icon-datetime:        url(<?= template::getImage("icons/datetime.png") ?>);
    --icon-down:            url(<?= template::getImage("icons/down.png") ?>);
    --icon-error:           url(<?= template::getImage("icons/error.png") ?>);
    --icon-fullscreen:      url(<?= template::getImage("icons/fullscreen.png") ?>);
    --icon-html:            url(<?= template::getImage("icons/html.png") ?>);
    --icon-left:            url(<?= template::getImage("icons/left.png") ?>);
    --icon-link:            url(<?= template::getImage("icons/link.png") ?>);
    --icon-location:        url(<?= template::getImage("icons/location.png") ?>);
    --icon-next:            url(<?= template::getImage("icons/next.png") ?>);
    --icon-no-fullscreen:   url(<?= template::getImage("icons/nofullscreen.png") ?>);
    --icon-ok:              url(<?= template::getImage("icons/ok.png") ?>);
    --icon-pause:           url(<?= template::getImage("icons/pause.png") ?>);
    --icon-people:          url(<?= template::getImage("icons/people.png") ?>);
    --icon-photo-big:       url(<?= template::getImage("icons/photobig.png") ?>);
    --icon-photographer:    url(<?= template::getImage("icons/photographer.png") ?>);
    --icon-pref:            url(<?= template::getImage("icons/prev.png") ?>);
    --icon-remove:          url(<?= template::getImage("icons/remove.png") ?>);
    --icon-right:           url(<?= template::getImage("icons/right.png") ?>);
    --icon-title:           url(<?= template::getImage("icons/title.png") ?>);
    --icon-up:              url(<?= template::getImage("icons/up.png") ?>);
    --icon-warning:         url(<?= template::getImage("icons/warning.png") ?>);


    --img-bg-star:          url(<?= template::getImage("backgroundStar.png") ?>);
    --img-down:             url(<?= template::getImage("down2.gif") ?>);
    --img-star:             url(<?= template::getImage("star.png") ?>);
}

/* vim: set syntax=css expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
