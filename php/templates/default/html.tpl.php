<?php
/**
 * Template for full page;
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

if (!defined("ZOPH")) {
    die("Illegal call");
}
?>

<!DOCTYPE html>
<html lang="<?= $tpl_lang ?>" class="<?= $tpl_html_class ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link type="image/png" rel="icon" href="<?= $tpl_favicon ?>">

        <?php foreach ($tpl_scripts as $script): ?>
            <script type='text/javascript' src="<?= $tpl_basepath ?><?= $script ?>"></script>
        <?php endforeach ?>
        <?php foreach ($tpl_javascripts as $javascript): ?>
            <script type='text/javascript'>
                <?= $javascript ?>
            </script>
        <?php endforeach ?>
        <?php foreach ($tpl_css as $css): ?>
            <link type='text/css' rel="stylesheet" href="<?= $tpl_basepath ?><?= $css ?>">
        <?php endforeach ?>
        <?php if (!empty($tpl_style)):  ?>
            <style type="text/css">
                <?= $tpl_style ?>
            </style>
        <?php endif ?>
        <?php if (isset($tpl_prev)): ?>
            <link rel="prev" href="<?= $tpl_prev ?>">
        <?php endif ?>
        <?php if (isset($tpl_next)): ?>
            <link rel="next" href="<?= $tpl_next ?>">
        <?php endif ?>
        <script type="text/javascript">
            var template = "<?= $tpl_template ?>";
            var icons={
                <?php foreach ($tpl_icons as $icon=>$file): ?>
                    "<?= $icon ?>": "<?= $file ?>",
                <?php endforeach ?>
            };
        </script>

        <title><?= $tpl_title ?></title>
    </head>
    <body>
        <?= $tpl_menu ?? "" ?>
        <?= $tpl_breadcrumbs ?? "" ?>
        <?= $this->displayBlocks() ?>
    </body>
</html>
