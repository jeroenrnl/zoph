<?php
/**
 * Template for displaying a list of colour schemes
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author Jeroen Roos
 * @package ZophTemplates
 */
if (!defined("ZOPH")) {
    die("Illegal call");
}
?>
<h1>
    <?= $this->getActionlinks() ?>
    <?= $tpl_title ?>
</h1>
<div class="main">
    <table class="colorschemes">
        <caption><?= $tpl_title ?></caption>
        <tr>
            <th scope="col"><?= translate("name") ?></th>
            <th scope="col"><?= translate("preview") ?></th>
            <th scope="col"><?= translate("options") ?></th>
        </tr>
        <?php foreach ($tpl_cs as list($cs, $actionlinks)): ?>
            <tr>
                <td><?= $cs->get("name") ?></td>
                <td>
                    <?php foreach ($cs->getColors() as $name=>$color): ?>
                        <div class="cs_example" style="background: #<?= $color ?>"></div>
                    <?php endforeach ?>
                </td>
                <td>
                    <ul class="actionlink">
                        <?php foreach ($actionlinks as $actionlink): ?>
                            <li><?= $actionlink ?></li>
                        <?php endforeach ?>
                    </ul>
                </td>
            </tr>
        <?php endforeach ?>
</div>
