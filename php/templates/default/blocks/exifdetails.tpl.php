<?php
/**
 * Template for exif details
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

use template\template;

if (!defined("ZOPH")) {
    die("Illegal call");
}
?>

<table class="allexif">
    <?php foreach ($tpl_exif as $key => $value): ?>
        <tr>
            <?php if (is_array($value)): ?>
                <td colspan=2>
                    <details>
                        <summary><?= $key ?></summary>
                        <p>
                            <table class="exif-subvalues">
                                <?php foreach ($value as $subkey => $subvalue): ?>
                                    <tr>
                                        <th><?= $subkey ?></th>
                                        <td><?= $subvalue ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </table>
                        </p>
                    </details>
                </td>
            <?php else: ?>
                <th><?= $key ?></th>
                <td><?= $value ?></td>
            <?php endif ?>
        </tr>
    <?php endforeach ?>
</table>
