<div class="install">
    <ul>
        <?php foreach ($tpl_icons as $page => $icon): ?>
            <li <?= ($tpl_selected == $page ? "class='selected'" : "") ?>>
                <img alt="<?= $page ?>" src="<?= $icon ?>">
            </li>
        <?php endforeach ?>
    </ul>
</div>
