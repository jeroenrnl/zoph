<?php
/**
 * Template for 'all exif' tab
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package ZophTemplates
 * @author Jeroen Roos
 */

use template\template;

if (!defined("ZOPH")) {
    die("Illegal call");
}
?>
<li class="exif">
    <div class="tab">
      <img alt="exif" src="<?php echo template::getImage("icons/exif.png") ?>">
    </div>
    <div class="contents">
        <h1><?= translate("Full EXIF details", 0) ?></h1>
        <?= $tpl_allexif ?>
    </div>
</li>
