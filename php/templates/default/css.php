<?php
/* This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
?>

@import "css/reset.css";
@import "css/leaflet.css";
@import "css/style.css";
@import "css/alert.css";
@import "css/colours.css";
@import "css/variables.css";

/** temporarily added here until the constants are phased out */

img.<?= THUMB_PREFIX ?> {
    box-shadow: 5px 5px 5px rgba(0,0,0,0.4);
}

div.thumbnail img.<?= THUMB_PREFIX ?>:hover {
    margin-top: -2px;
    margin-left: -2px;
    box-shadow: 7px 7px 7px rgba(0,0,0,0.4);
}

img.<?= MID_PREFIX ?> {
    box-shadow: 5px 5px 5px rgba(0,0,0,0.4);
    margin: 10px auto;
    clear: both;
    text-align: center;
    display: block;
}

?>
/* vim: set syntax=css: */
