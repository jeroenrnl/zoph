<?php
/**
 * Global helper functions
 *
 * This file is part of Zoph.
 *
 * Zoph is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Zoph is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Zoph; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @package Zoph
 * @author Jason Geiger
 * @author David Baldwin
 * @author Jeroen Roos
 */

use generic\variable;

function i($value) {
    $var=new variable($value);
    return $var->input();
}

function e($value) {
    $var=new variable($value);
    return $var->escape();
}

/**
 * Translate the given string
 * @param string The string to be translated
 * @param bool If true add [tr] before any string that cannot be
 *   translated.
 * @return string The translated string
 */
function translate(string|array $str, $error=true) : string|array {
    $lang = language::getCurrent();
    if ($lang instanceof language) {
        return $lang->translate($str, $error);
    } else {
        return $str;
    }
}
?>
