# UPGRADE INSTRUCTIONS #
As of Zoph 0.9.18, database upgrades are included in the release. Simply copy the contents of the `php` directory into the webroot. Then log in to Zoph with an admin user and the GUI will guide you through the upgrade process. If you are running an older version of Zoph, please make sure you follow the upgrade instructions to v0.9.17 (below) prior to logging in to Zoph and performning the upgrade from there.
```
    cp -a php/* /var/www/html/zoph
```
Since files may have been moved or deleted, it's a good idea to delete the old files first.

If you use the CLI client, you should copy it to a path that's in your `$PATH`.
```
    cp cli/zoph /usr/bin
```

## Zoph 0.9.21 ##
A small change was made to the CLI client, if you use it, you should update the version in your `$PATH`.

PHP 8.1 is now supported.

## Zoph 0.9.20 ##
A small change was made to the CLI client, if you use it, you should update the version in your `$PATH`.

As of this release, Zoph requires PHP 8.0 or later. 8.1 should work, but is not yet tested. PHP 7.4 or older are no longer supported.

## 0.9.17 and older versions ##
If you want to upgrade from an older version, download [v0.9.18](https://gitlab.com/zoph-project/zoph/-/releases/v0.9.18) and upgrade to that version, from there upgrade to the newest version. 
